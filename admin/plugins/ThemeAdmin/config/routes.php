<?php
use Cake\Routing\Router;

Router::plugin(
    'ThemeFdAdmin',
    ['path' => '/theme-fd-admin'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
