<%
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.1.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'];
$associationFields = collection($fields)
->map(function($field) use ($immediateAssociations) {
foreach ($immediateAssociations as $alias => $details) {
if ($field === $details['foreignKey']) {
return [$field => $details];
}
}
})
->filter()
->reduce(function($fields, $value) {
return $fields + $value;
}, []);

$groupedFields = collection($fields)
->filter(function($field) use ($schema) {
return $schema->columnType($field) !== 'binary';
})
->groupBy(function($field) use ($schema, $associationFields) {
$type = $schema->columnType($field);
if (isset($associationFields[$field])) {
return 'string';
}
if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
return 'number';
}
if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
return 'date';
}
return in_array($type, ['text', 'boolean']) ? $type : 'string';
})
->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>
<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('<%=  $pluralHumanName  %>'), ['controller' => '<%= $pluralVar %>']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit <%= $singularHumanName %>'), ['action' => 'edit', <%= $pk %> ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( <%= '$' . $singularVar . '->ativo' %>) ? 'close' : 'check'; ?>
                <?php $label_active =  ( <%= '$' . $singularVar . '->ativo' %>) ? __('Desativar') : __('Ativar'); %>
                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', <%= $pk %>], ['confirm' => __('Deseja (des)Ativar # {0}?', <%= $pk %>), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('<%= $singularHumanName %>') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($<%= $singularVar %>-><%= $displayField %>) ?></h4>
                        <table class="table">
                            <% if ($groupedFields['string']) : %>
                            <% foreach ($groupedFields['string'] as $field) : %>
                            <% if (isset($associationFields[$field])) :
                            $details = $associationFields[$field];
                            %>
                            <tr>
                                <th><?= __('<%= Inflector::humanize($details['property']) %>') ?></th>
                                <td><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
                            </tr>
                            <% else : %>
                            <tr>
                                <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
                                <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
                            </tr>
                            <% endif; %>
                            <% endforeach; %>
                            <% endif; %>
                            <% if ($associations['HasOne']) : %>
                            <%- foreach ($associations['HasOne'] as $alias => $details) : %>
                            <tr>
                                <th><?= __('<%= Inflector::humanize(Inflector::singularize(Inflector::underscore($alias))) %>') ?></th>
                                <td><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
                            </tr>
                            <%- endforeach; %>
                            <% endif; %>
                            <% if ($groupedFields['number']) : %>
                            <% foreach ($groupedFields['number'] as $field) : %>
                            <tr>
                                <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
                                <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
                            </tr>
                            <% endforeach; %>
                            <% endif; %>
                            <% if ($groupedFields['date']) : %>
                            <% foreach ($groupedFields['date'] as $field) : %>
                            <tr>
                                <th><%= "<%= __('" . Inflector::humanize($field) . "') %>" %></th>
                                <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
                            </tr>
                            <% endforeach; %>
                            <% endif; %>
                            <% if ($groupedFields['boolean']) : %>
                            <% foreach ($groupedFields['boolean'] as $field) : %>
                            <tr>
                                <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
                                <td><?= $<%= $singularVar %>-><%= $field %> ? __('Yes') : __('No'); ?></td>
                            </tr>
                            <% endforeach; %>
                            <% endif; %>
                        </table>

                    </div>
                </div>
            </div>


            <% if ($groupedFields['text']) : %>
                        <% foreach ($groupedFields['text'] as $field) : %>
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('<%= Inflector::humanize($field) %>') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                            <?= $this->Text->autoParagraph(h($<%= $singularVar %>-><%= $field %>)); ?>
                        </div>
                </div>
            </div>

                        <% endforeach; %>
                        <% endif; %>
                        <%
                        $relations = $associations['HasMany'] + $associations['BelongsToMany'];
                        foreach ($relations as $alias => $details):
                        $otherSingularVar = Inflector::variable($alias);
                        $otherPluralHumanName = Inflector::humanize(Inflector::underscore($details['controller'])); %>


            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('<%= $otherPluralHumanName %>') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('<%= $otherPluralHumanName %>') ?></h4>
                        <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>
                        <table class="table">
                            <tr>
                                <% foreach ($details['fields'] as $field): %>
                                <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
                                <% endforeach; %>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($<%= $singularVar %>-><%= $details['property'] %> as $<%= $otherSingularVar %>): ?>
                            <tr>
                                <%- foreach ($details['fields'] as $field): %>
                                <td><?= h($<%= $otherSingularVar %>-><%= $field %>) ?></td>
                                <%- endforeach; %>
                                <%- $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => '<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $otherPk %>)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    <% endforeach; %>

        </div>
    </div>
</div>
