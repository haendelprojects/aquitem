/*$Id$*/
/**
 * this js file containing reports related functions
 * @author Murugesan K
 * @date 18-01-2008
 */


var criteriaID = 1;

var selectedChartType = -1;

function multipleSelectAllOptions(dic){
    debugger;
    for(i = 0 ; i <  dic.length ; i++){
        selectAllOptions(dic[i]);
    }
}

function selectAllOptions(id) {
    obj = document.getElementById(id);

    for (var i = 0; i < obj.options.length; i++) {
        obj.options[i].selected = true;
    }
}

function moveUp(index, selectTag) {
    if (( index > 0) && (index < selectTag.length)) {
        var tmp = selectTag.options[index - 1].text;
        var val = selectTag.options[index - 1].value;
        selectTag[index - 1].text = selectTag[index].text;
        selectTag[index - 1].value = selectTag[index].value;
        selectTag[index].text = tmp;
        selectTag[index].value = val;
        selectTag[index].selected = false;
        selectTag[index - 1].selected = true;
    }
}

function multiMoveUp(from) {
    var selectTag = document.getElementById(from);
    var selectedList = new Array();
    var j = 0;
    for (var i = 0; i < selectTag.length; i++) {
        if (selectTag.options[i].selected == true) {
            if (i == 0) break;
            if (i > 0) {
                selectedList[j] = i;
                j++;
            }
        }
    }
    for (j = 0; j < selectedList.length; j++) {
        moveUp(selectedList[j], selectTag);
    }
}

function moveDown(index, selectTag) {
    var max = selectTag.length;
    var min = 0;
    if (index < max - 1 && index >= min) {
        var tmp = selectTag.options[index + 1].text;
        var val = selectTag.options[index + 1].value;
        selectTag.options[index + 1].text = selectTag.options[index].text;
        selectTag.options[index + 1].value = selectTag.options[index].value;
        selectTag.options[index].text = tmp;
        selectTag.options[index].value = val;
        selectTag.options[index].selected = false;
        selectTag.options[index + 1].selected = true;
    }
}

function multiMoveDown(from) {
    var selectTag = document.getElementById(from);
    var max = selectTag.length;
    var min = 0;
    var selectedList = new Array();
    var j = 0;
    for (var i = 0; i < max; i++) {
        if (selectTag.options[i].selected == true) {
            selectedList[j] = i;
            j++;
        }
    }
    if (selectedList[j - 1] < max && selectedList[j - 1] >= min) {
        for (j = selectedList.length; j >= 0; j--) {
            moveDown(selectedList[j], selectTag);
        }
    }
}
function copyListValues(from, to, selectTagObj) {
    if (selectTagObj != null) {
        selectTagObj.selected = true;
    }
    var sel = false;
    try {
        var fromList = document.getElementById(from);
        var toList = document.getElementById(to);

        if (fromList.options.length > 0) {
            for (var i = 0; i < fromList.options.length; i++) {
                if (fromList.options[i].selected) {
                    toList.appendChild(fromList.options[i]);
                    i--;
                    sel = true;
                }
            }
        }
    }
    catch (e) {
        alert(e.message);
    }
    if (!sel) alert('Item Não Selecionado');
}
function setDateFilterStatus(selectTag) {
    if (selectTag.value == -1) {
        document.reportfilter.preDateFilterType[0].disabled = true;
        document.reportfilter.preDateFilterType[1].disabled = true;
        document.reportfilter.dateFilterType.disabled = true;
        document.reportfilter.preFromDate.disabled = true;
        document.reportfilter.imgFromDate.disabled = true;
        document.reportfilter.preToDate.disabled = true;
        document.reportfilter.imgToDate.disabled = true;
    }
    else {
        document.reportfilter.preDateFilterType[0].disabled = false;
        document.reportfilter.preDateFilterType[1].disabled = false;
        document.reportfilter.dateFilterType.disabled = false;
    }
}

var selecteUniqueId = null;
//var sortId = null;

function getSelectedRowId() {
    return this.selecteUniqueId;
}

function getSelectedSortOrder() {
    return this.sortId;
}

function getCriteriaOperators(selectTag) {
    this.selecteUniqueId = selectTag.id.split("_")[2];
    this.sortId = selectTag.id.split("_")[0];
    if (selectTag.value == -1) {
        return false;
    }
    callReportsAjaxRequest("/CustomReportHandler.do", "module=get_criteria_operator&column_id=" + selectTag.value, 'get_criteria_operator'); //No I18N
}

function getAbsDate() {
    callReportsAjaxRequest("/CustomReportHandler.do", "module=get_abs_date&filter_id=" + document.reportfilter.dateFilterType.value, 'getabsdate', 'GET'); //No I18N
}


function enableFilterDisDate() {
    document.reportfilter.dateFilterType.disabled = false;
    document.reportfilter.preFromDate.disabled = true;
    document.reportfilter.imgFromDate.disabled = true;
    document.reportfilter.preToDate.disabled = true;
    document.reportfilter.imgToDate.disabled = true;
}
function disableFilterEnableDate() {
    document.reportfilter.dateFilterType.disabled = true;
    document.reportfilter.preFromDate.disabled = false;
    document.reportfilter.imgFromDate.disabled = false;
    document.reportfilter.preToDate.disabled = false;
    document.reportfilter.imgToDate.disabled = false;
}

function ajaxRequestOnSuccess(requestObj, action) {
    sdpHideIndicator();
    if (action == 'getabsdate') {
        var dates = requestObj.split("#");
        document.reportfilter.preFromDate_tmp.value = dates[0];
        document.reportfilter.preToDate_tmp.value = dates[1];

    }
    else if (action == 'get_criteria_operator') {
        var criObj = requestObj.getElementsByTagName("ReportCriteria");
        var length = criObj.length;
        /* Remove all previous criteria(s) */
        var selectTag = document.getElementById('select_criteria_' + getSelectedRowId());
        removeAllOptionTags(selectTag);
        var optionSize = selectTag.options.length;
        for (var i = 0; i < length; i++) {
            selectTag.options[optionSize + i] = new Option(criObj[i].getAttribute("criteria_name"), criObj[i].getAttribute("criteria_id"));
        }
        var criTypeObj = requestObj.getElementsByTagName("ReportCriteriaType");
        setColumnDataType(criTypeObj[0].getAttribute("criteria_type"));

        /* Reset the previous column value */
        document.getElementById('criteriaValue_' + getSelectedRowId()).value = "";
    }
    else if (action == 'show_filter_page') {
        var rcstep2 = document.getElementById('rcstep2');
        jQuery('#rcstep2').html(requestObj);


        constructFilterPage();
        if (document.reportfilter.preDateFilterType && document.reportfilter.preDateFilterType[0].checked == true) {
            getAbsDate();
        }
        if (document.reportfilter.dateFilterColumn && document.reportfilter.dateFilterColumn.value == -1) {
            document.reportfilter.preDateFilterType[0].disabled = true;
            document.reportfilter.preDateFilterType[1].disabled = true;
            document.reportfilter.dateFilterType.disabled = true;
            document.reportfilter.preFromDate.disabled = true;
            document.reportfilter.imgFromDate.disabled = true;
            document.reportfilter.preToDate.disabled = true;
            document.reportfilter.imgToDate.disabled = true;
        }
        //new Effect.BlindDown(rcstep2);
        rcstep2.style.display = 'block';
    }
    else if (action == 'show_grouping_page') {
        jQuery('#rcstep3').html(requestObj).show();
        constructGroupingPage();
    }
    else if (action == 'show_summary_page') {
        jQuery('#rcstep4').html(requestObj).show();
        setSummaryDataInPage();
    }
    else if (action == 'show_report_page') {
        if (document.getElementById('report_input_pages') != undefined) {
            //This element will not be available if the user run the report from listview page
            //new Effect.BlindUp('report_input_pages');
            jQuery('#report_input_pages').hide();
            jQuery('#report_result').html(requestObj);
        }
        else {
            //This code will be called if the user run the report from list view
            jQuery('#reportlistviewpage').html(requestObj);
        }
        if (document.getElementById("centerstatus") != undefined) {
            hideDiv("centerstatus");
        }
        parent.closeDialog();
    }
    else if (action == 'show_matrix_report_page') {
        hideDiv("centerstatus");
        new Effect.BlindUp('report_input_pages');
        jQuery('#report_result').html(requestObj);
        //constructMatrixPage();
    }
    else if (action == 'show_graph_page') {
        jQuery('#rcstep5').html(requestObj).show();
        rcstep5.style.display = 'block';
        restoreChartProperties();
    }
    else if (action == 'create_new_folder') {
        if (requestObj != "NA") {
            var folder_name = document.getElementById("folder_name").value; // No I18N
            jQuery('#folderId optgroup[key*=OWN_FOLDERS]').prepend("<option ispublic='true' value='" + requestObj + "' title='" + folder_name + "'>" + folder_name + "</option>"); // No I18N
            jQuery('#folderId').val(requestObj).trigger('change'); // No I18N
        }
        jQuery('#folder_data').show(); // No I18N
        jQuery("#addfolder").hide(); // No I18N
    }
    else if (action == 'create_duplicate_report') {
        sdpShowIndicator({message: getMessageForKey('sdp.reports.customreport.duplicate.success'), success: true});
        sdpClosePopup();
    }
    else if (action == 'save_report_as') {
        var response = requestObj;

        if (response.indexOf('success') == 0) {
            var reportId = response.split("_")[1];
            updateReportPageLinks(reportId, '/CustomReportHandler.do?module=export');
        }
        else {
            sdpShowIndicator({message: getMessageForKey('sdp.failure') + ' ' + requestObj, success: false});
        }
    }
    else if (action == 'get_folder_desc') {
        jQuery("#desc").val(requestObj.MESSAGE);
    }
    else if (action == 'update_custom_settings') {
        sdpClosePopup();
        sdpShowIndicator({success: true, message: getMessageForKey('sdp.reports.customReport.update.success')}); //No I18N
        //document.getElementById('updatereportsettings').className=''; Not needed in new UI id commented by push
    }
    else if (action == 'delete_schedule_report') {
        sdpShowIndicator({success: true, message: getMessageForKey('sdp.admin.common.deletedsuccessfully')}); //No I18N
        changeReportsTab('scheduleReports');//No I18N
    }
    else if (action == 'send_mail') {
        var response = requestObj;

        if (response.indexOf('success') == 0) {
            //document.getElementById('mailsentStatus').innerHTML = '<font color="#00CC00"><b>'+getMessageForKey('sdp.reports.customreport.mailsent.success')+'</b></font>';
            //setTimeout('sdpClosePopup();',1000);
            sdpClosePopup();
            sdpShowIndicator({success: true, message: getMessageForKey('sdp.reports.customreport.mailsent.success')});//No I18N
        }
        else {
            sdpShowIndicator({message: getMessageForKey('sdp.failure') + requestObj, success: false});
        }
    }
    else if (action == 'run_from_listview') {
        hideDiv("centerstatus");
        jQuery('#reportlistviewpage').html(requestObj);
        parent.closeDialog();
    }
    else if (action == 'edit_report_page') {
        jQuery('#reportlistviewpage').html(requestObj);
    }
    else if (action == 'add_folder' || action == 'update_folder' || action == 'delete_folder') {
        var msgkey = 'sdp.reports.customReport.folder.delete.success'; // No I18N
        if (action == 'add_folder') {
            msgkey = 'sdp.reports.customReport.foldersuccessmsg'; // No I18N
        } else if (action == 'update_folder') // No I18N
        {
            msgkey = 'sdp.reports.customReport.folder.update.success'; // No I18N
        }
        sdpClosePopup(jQuery('[name*=editFolder]')); // No I18N
        sdpShowIndicator({success: true, message: getMessageForKey(msgkey)}); // No I18N
        jQuery('#reportlistviewpage').html(requestObj);
    }
    else if (action == 'show_summary_filter_page') {
        hideDiv("centerstatus");
        jQuery('#reportlistviewpage').html(requestObj);

        if (document.reportfilter.preDateFilterType && document.reportfilter.preDateFilterType[0].checked == true) {
            getAbsDate();
        }
        if (document.reportfilter.dateFilterColumn && document.reportfilter.dateFilterColumn.value == -1) {
            document.reportfilter.preDateFilterType[0].disabled = true;
            document.reportfilter.preDateFilterType[1].disabled = true;
            document.reportfilter.dateFilterType.disabled = true;
        }
        constructFilterPage();
    }
    else if (action == 'show_audit_main_page') {
        try {
            hideDiv("centerstatus");
        } catch (e) {
        }
        jQuery('#reportlistviewpage').html(requestObj);
    }
    else if (action == 'save_schedule') {
        sdpShowIndicator({message: getMessageForKey('sdp.header.personalize.language.save.success'), success: true});
        //for diverting the page to schedule home after saving the details...
        showScheduledReportListView();
    }
    else if (action == 'input_page' || action == 'start_page' || action == 'show_available_subreports') {
        try {
            hideDiv("centerstatus");
        } catch (e) {
        }
        jQuery('#reportlistviewpage').html(requestObj);


        //Updating date columns - java.util.Date
        var reportTypeObj = document.getElementsByName('reportType');
        var reportTypeId = -1;
        for (var i = 0; i < reportTypeObj.length; i++) {
            if (reportTypeObj[i].checked) {
                reportTypeId = reportTypeObj[i].id;
            }
        }
        if (reportTypeId != -1) {
            getAvailableModules(reportTypeId);
        }
    }
    else if (action == 'show_advmatrix_page') {
        jQuery('#advanced').html(requestObj);
        constructMatrixPage();
    }
    else if (action == 'show_simplematrix_page') {
        jQuery('#simple').html(requestObj);
        constructMatrixPage();
    }
    /*else if ( action == 'show_query_editor' )
     {
     jQuery('#reportlistviewpage').html(requestObj);
     }*/
    else if (action == 'get_sql_columns') {
        var status = requestObj.getElementsByTagName("status")[0].childNodes[0].nodeValue;
        if (status == 'success') {
            var sqlCols = requestObj.getElementsByTagName("columns");
            var length = sqlCols.length;
            removeAllOptionTags(document.getElementById('QGroupBy'));
            //removeAllOptionTags(document.getElementById('qsum'));
            //removeAllOptionTags(document.getElementById('qmin'));
            //removeAllOptionTags(document.getElementById('qmax'));
            //removeAllOptionTags(document.getElementById('qavg'));
            //removeAllOptionTags(document.getElementById('qcount'));
            //removeAllOptionTags(document.CustomReportHandlerForm.chartType);
            //var optionSize = document.getElementById('QGroupBy').options.length;

            for (i = 0; i < length; i++) {
                document.getElementById('QGroupBy').options[i + 1] = new Option(sqlCols[i].getAttribute('name'), sqlCols[i].getAttribute('index'));
                //var data_type = sqlCols[i].getAttribute('type');
                /*
                 if( data_type == 'java.lang.Long' || data_type == 'java.lang.Integer' || data_type ==
                 'java.lang.Double' || data_type == 'javax.lang.Memory' || data_type == 'java.sql.Time'  )
                 {
                 document.getElementById('qsum').options[optionSize] = new Option(sqlCols[i].getAttribute('name'),sqlCols[i].getAttribute('index'));
                 document.getElementById('qmin').options[optionSize] = new Option(sqlCols[i].getAttribute('name'),sqlCols[i].getAttribute('index'));
                 document.getElementById('qmax').options[optionSize] = new Option(sqlCols[i].getAttribute('name'),sqlCols[i].getAttribute('index'));
                 document.getElementById('qavg').options[optionSize] = new Option(sqlCols[i].getAttribute('name'),sqlCols[i].getAttribute('index'));
                 document.getElementById('qcount').options[optionSize] = new Option(sqlCols[i].getAttribute('name'),sqlCols[i].getAttribute('index'));
                 optionSize++;
                 }
                 */
            }
            document.getElementById('errorlog').value = '';
        }
        else {
            document.getElementById('errorlog').value = status;
        }
    }
    else if (action == 'run_query_editor') {
        //Clear the loading icon in SQL query editor page
        document.getElementById('queryreportgenmsg').innerHTML = "";

        new Effect.BlindUp('report_input_pages');
        jQuery('#report_result').html(requestObj);
    }
    else if (action == 'can_delete') {
        var report_id = requestObj;
        if (report_id != "-1") {
            sdpodconfirm({
                title: getMessageForKey('alert.title'), message: jQuery('#reportdelconfirm').html(), okFunc: function ()	 //No I18N
                {
                    callReportsAjaxRequest("/CustomReportHandler.do", "module=delete_report&report_id=" + report_id, 'delete_report'); //No I18N
                }
            });
        }
        else {
            //alert(geti18nString( "sdp.reports.customreport.defreportdelmsg" ));
            sdpodalert({title: getMessageForKey('alert.title'), message: jQuery("#defreportdelmsg").html()});	 //No I18N
        }

    }
    else if (action == 'delete_report') {
        if (requestObj != 'false') {
            deleteRow(document.getElementById("report_" + requestObj));
        }
        else {
            sdpodalert({
                title: getMessageForKey('alert.title'),
                message: getMessageForKey('sdp.reports.customreport.deletefalse')
            }); //No I18N
        }
    }
    else if (action == 'get_available_module') {
        var moduleObj = document.getElementsByName('moduleID'); // No I18N
        var checked = false;

        var hasModule = false;

        jQuery(moduleObj).each(
            function () {
                if (requestObj.indexOf(jQuery(this).val()) > -1) {
                    if (!checked) {
                        jQuery(this).prop('checked', true); // No I18N
                        checked = true;
                    }
                    jQuery(this).parents('tr').first().show(); // No I18N
                    hasModule = true;
                } else {
                    jQuery(this).prop('checked', false).parents('tr').first().hide(); // No I18N
                }
            });
        if (!hasModule) {
            sdpodalert({
                title: getMessageForKey('alert.title'),
                message: getMessageForKey('sdp.reports.customreport.module.previlage')
            }); //No I18N
            jQuery("#addnew223225").prop("disabled", true);//No I18N
        } else {
            jQuery("#addnew223225").prop("disabled", false);//No I18N
        }
    }
}
function removeInnerHTML(elementId) {
    document.getElementById(elementId).innerHTML = "";
}

function updateReportPageLinks(reportId, exportParam) {
    parent.previousConfig.reportId = reportId;
    var browser = navigator.appName;
    sdpClosePopup();
    sdpShowIndicator({success: true, message: getMessageForKey('sdp.reports.customreport.save.success')});//No I18N
    try {
        //Error ignored here, while running report from list view page the property name
        //'navReportName' will not be there
        if (jQuery('#navReportName').length > 0) {
            jQuery('#navReportName').html(ashtmlString(jQuery('#reportName').val()));
        }
    }
    catch (e) {
    }
    jQuery('#headerReportName').html(ashtmlString(jQuery('#reportName').val()));
    try {
        jQuery('#savereportaslink').hide();//No I18N
        //jQuery('#savereportaslink').html(getMessageForKey('sdp.reports.customReport.savereportas'));
        //jQuery('#savereportaslink').attr('onclick',null).off('click').click(function(event) { openSaveReportAsWindow(event,reportId); return false;}); //No I18N
    }
    catch (e) {/*Error ignored for audit report*/
    }
    try {
        var typeId = parent.previousConfig.reportTypeId;
        if (typeId === undefined || typeId === "") {
            typeId = '1';//No I18N
        }
        jQuery('#editreportlink').attr('onclick', null).off('click').click(function () {//No I18N
            editReport(reportId, typeId, false);
            return false;
        });
    }
    catch (e) {
    }

    try {
        jQuery('#sendmaillink').attr('onclick', null).off('click').click(function (event) {
            sendMail(reportId, event);
            return false;
        });//No I18N
    }
    catch (e) {/*Error ignored for summary and audit report*/
    }
    jQuery('#printpreviewlink').attr('onclick', null).off('click').click(function () {
        print_view(reportId, null);
        return false;
    });//No I18N
    jQuery("#rep_id").val(reportId); //No I18N

//	document.getElementById('exporthtml').href = exportParam + "&file_type=HTML&report_id=" + reportId;
//	document.getElementById('exportpdf').href = exportParam + "&file_type=PDF&report_id=" + reportId;
//	document.getElementById('exportxls').href = exportParam + "&file_type=XLS&report_id=" + reportId;
//	document.getElementById('exportcsv').href = exportParam + "&file_type=CSV&report_id=" + reportId;
}

function changeImage(reportTypeId) {
    if (reportTypeId == 'Summary Reports') {
        showDiv('summary');
        hideDiv('tabular');
        hideDiv('matrix');
    }
    else if (reportTypeId == "Tabular Reports") {
        hideDiv('summary');
        showDiv('tabular');
        hideDiv('matrix');
    }
    else if (reportTypeId == "Matrix Reports") {
        hideDiv('summary');
        hideDiv('tabular');
        showDiv('matrix');
    }
}

function showDiv(id) {
    var idele = document.getElementById(id);
    if (idele) {
        idele.style.display = 'block';
    }
}
function hideDiv(id) {
    var idele = document.getElementById(id);
    if (idele) {
        idele.style.display = 'none';
    }
}

function setSelectOption(selectboxname, optvalue) {
    jQuery('select[name="' + selectboxname + '"]').find('option[value="' + optvalue + '"]').prop("selected", true);//No I18N
}

function restoreChartProperties() {
    if (parent['previousConfig'] != undefined && parent['previousConfig'].chartId != undefined) {
        var repObj = parent['previousConfig'];
        var chartType = repObj.chartId;
        var chartProp = repObj.chartProperties;
        document.chart_types.chartType.value = chartType;
        changeChartType(document.chart_types.chartType);
        var url = "/servlet/SDODAuthServlet?ACTION=getChartName&CHART_ID=" + chartType;
        var ch_name = getAjaxResponse(url);

        if (ch_name == 'PieChart 2D' || ch_name == 'PieChart 3D' || ch_name == 'Ring Chart') {
            setSelectOption('pieChartGroupColumn', chartProp.group_column);
            setSelectOption('pieChartDisplayFormat', chartProp.display_format);
        }
        else if (ch_name == 'BarChart 2D' || ch_name == 'BarChart 3D' || ch_name == 'BarChart Stacked 2D' || ch_name == 'BarChart Stacked 3D' || ch_name == 'Layered BarChart' || ch_name == 'Overlaid BarChart') {
            setSelectOption('barXGroup1', chartProp.barXGroup1);
            setSelectOption('barXGroup2', chartProp.barXGroup2);
            setSelectOption('barChartBy', chartProp.barChartBy);
        }
        else if (ch_name == 'Line Chart') {
            setSelectOption('lineXDate', chartProp.lineXDate);
            setSelectOption('lineXDateFormat', chartProp.lineXDateFormat);
            setSelectOption('lineXGroup', chartProp.lineXGroup);
        }
        else if (ch_name == 'TimeSeries Chart') {
            setSelectOption('timeXDate', chartProp.timeXDate);
            setSelectOption('timeXDateFormat', chartProp.timeXDateFormat);
            setSelectOption('timeXGroup', chartProp.timeXGroup);
            setSelectOption('timeYCountColumn', chartProp.timeYCountColumn);
        }
        else if (ch_name == 'Area Chart') {
            setSelectOption('areaXDate', chartProp.areaXDate);
            setSelectOption('areaXDateFormat', chartProp.areaXDateFormat);
            setSelectOption('areaXGroup', chartProp.areaXGroup);
        }
        else if (ch_name == 'Step Chart') {
            setSelectOption('stepXGroup1', chartProp.stepXGroup1);
            setSelectOption('stepYCountColumn', chartProp.stepYCountColumn);
            setSelectOption('stepXGroup2', chartProp.stepXGroup2);
        }
    }
    else {
        //Report chart all default column should be group column
        var group_column = -1;
        if (document.group_columns != undefined && document.group_columns.groupTabularColumn != null) {
            group_column = document.group_columns.groupTabularColumn.value;
        }
        if (group_column != -1) {
            setSelectOption('stepXGroup1', group_column);
            setSelectOption('areaXGroup', group_column);
            setSelectOption('timeXGroup', group_column);
            setSelectOption('lineXGroup', group_column);
            setSelectOption('barXGroup1', group_column);
            setSelectOption('pieChartGroupColumn', group_column);
        }
    }
}

function changeChartType(obj) {
    var url = "/servlet/SDODAuthServlet?ACTION=getChartName&CHART_ID=" + obj.value;
    var ch_name = obj.value;

    for (var i = 1; i <= 6; i++) {
        chart = document.getElementById("chart" + i);
        if (chart != null) {
            chart.style.display = 'none';
        }
    }

    chart = document.getElementById("chart21");
    if (chart != null) {
        chart.style.display = 'none';
    }

    var graph_image = document.getElementById("graph_image");
    var i = 0;
    if (ch_name == 'chart_1') {
        graph_image.className = "piechart2d";
        i = 1;
    }
    else if (ch_name == 'chart_2') {
        i = 2;
        graph_image.className = "piechart3d";
    }
    else if (ch_name == 'chart_3') {
        i = 3;
        graph_image.className = "ringchart";
    }
    else {
        graph_image.className = "chart_none_big";
        return;
    }

    chart = document.getElementById("chart" + i);
    if (chart != null) {
        chart.style.display = 'block';
    }
    if (ch_name == 'BarChart Stacked 2D' || ch_name == 'BarChart Stacked 3D') {
        chart = document.getElementById("chart" + i + "1");
        if (chart != null) {
            chart.style.display = 'block';
        }
    }
}

var dataType = null;

function setColumnDataType(dataType) {
    this.dataType = dataType;
}

function getColumnDataType() {
    return this.dataType;
}

function showDataPicker(inputObj, e, sortOrder) {
    var uniqueRowId = -1;
    if (inputObj.id.indexOf("criteriaValue") >= 0) {
        uniqueRowId = inputObj.id.split("_")[1];
    }
    else if (inputObj.id.indexOf("button") >= 0) {
        uniqueRowId = inputObj.id.split("_")[1];
    }

    var col_id = document.getElementById(sortOrder + '_filterColumn_' + uniqueRowId).value;
    var cri_value = document.getElementById('select_criteria_' + uniqueRowId).value;
    if (col_id != -1 && cri_value != -1) {
        sdpShowURLInPopup('/CustomReportHandler.do?module=datapicker&column_id=' + col_id + "&row_index=" + uniqueRowId, 'title= ' + jQuery('#' + sortOrder + '_filterColumn_' + uniqueRowId + ' :selected').text() + ',closeButton=no,position=absolute,width=350,left=' + (e.screenX - 370) + ',top=' + (e.screenY - 350)); //No I18N
    }
    else {
        sdpodalert({
            title: getMessageForKey('alert.title'),
            message: getMessageForKey("sdp.reports.errmsg.selectcolumncriteria")
        });	//No I18N
        return false;
    }
}

function removeAllOptionTags(selectTagObj) {
    var length = selectTagObj.options.length;
    for (var i = 1; i < length; i++) {
        selectTagObj.options[i] = null;
    }
    selectTagObj.options.length = 1;
}


function ajaxRequestOnFailure(requestObj, action) {
    if (action == 'run_query_editor') {
        document.getElementById("errorlog").value = requestObj.responseText;
        hideDiv("queryreportgenmsg");
    }
    if (action == 'show_report_page') {
        hideDiv("centerstatus");
        document.getElementById('report_result').innerHTML = "";
    }
    if (document.getElementById("centerstatus") != undefined) {
        hideDiv("centerstatus");
    }

}

function validateDateFormat(obj) {
    var objRegExp = /(^[^GymMwWDdFEakKhHsSzZ]*$|[()*^%$#@!`~_=+}{|;?\]"[])/;
    if (trim(obj.value) == "") {
        sdpodalert({
            message: getMessageForKey("sdp.reports.empty.dateformat"), action: function () {
                obj.focus();
            }
        });
        return false;
    }
    if (objRegExp.test(obj.value)) {
        sdpodalert({
            message: getMessageForKey("sdp.reports.invalid.dateformat"), action: function () {
                obj.focus();
            }
        });
        return false;
    }
    return true;
}

function updateReportSettings() {
    if (!validateDateFormat(document.getElementsByName('dateFormat')[0])) return false;
    if (!validate(document.getElementsByName('smallText')[0])) return false;
    if (!validate(document.getElementsByName('largeText')[0])) return false;
    if (!validate(document.getElementsByName('number')[0])) return false;
    if (!validate(document.getElementsByName('date')[0])) return false;
    if (!validate(document.getElementsByName('time')[0])) return false;
    if (!validate(document.getElementsByName('cellWidth')[0])) return false;
    if (!validate(document.getElementsByName('cellHeight')[0])) return false;

    var args = "&smallText=" + document.getElementsByName('smallText')[0].value;
    args = args + "&largeText=" + document.getElementsByName('largeText')[0].value;
    args = args + "&number=" + document.getElementsByName('number')[0].value;
    args = args + "&date=" + document.getElementsByName('date')[0].value;
    args = args + "&time=" + document.getElementsByName('time')[0].value;
    args = args + "&cellWidth=" + document.getElementsByName('cellWidth')[0].value;
    args = args + "&cellHeight=" + document.getElementsByName('cellHeight')[0].value;
    args = args + "&dateFormat=" + encodeURIComponent(jQuery('input[name="dateFormat"]').val()); //No I18N

    if (document.getElementsByName('disablelink')[0].checked) {
        args = args + "&disablelink=true";
    }
    /*
     removinng for now...Will be Included later
     if( document.getElementsByName('removetitle')[0].checked )
     {
     args = args + "&removetitle=true";
     }*/
    if (document.getElementsByName('groupperpage')[0].checked) {
        args = args + "&groupperpage=true";
    }
    if (trim(document.getElementsByName('replaceNullValue')[0].value) != '') {
        args = args + "&replaceNullValue=" + encodeURIComponent(jQuery('input[name="replaceNullValue"]').val());//No I18N
    }
    else {
        sdpodalert({message: "Empty value not allowed..."});//No I18N
        document.getElementsByName('replaceNullValue')[0].focus();
        return false;
    }

    sdpShowIndicator();
    callReportsAjaxRequest("/CustomReportHandler.do", "module=update_custom_settings" + args, 'update_custom_settings'); //No I18N
}

function validate(obj) {
    if (!(isPositiveInteger(obj.value) && Number(obj.value) > 0)) {
        sdpodalert({
            message: getMessageForKey("sdp.common.invalidnumber"), action: function () {
                obj.focus();
            }
        });
        return false;
    }
    return true;
}


function showSettingsPage(e) {
    //sdpShowURLInPopup('CustomReportHandler.do?module=duplicate_report','title='+geti18nString('sdp.reports.saveReport.saveSubmit')+',position=absolute,width=580,left=' + (e.screenX - 370) +',top=' + (e.screenY -  150)); //No I18N

    sdpShowURLInPopup('CustomReportHandler.do?module=custom_settings', 'position=absolute,width=600,left=' + (e.screenX - 450) + ',top=' + (e.screenY - 230) + ',title=' + getMessageForKey("sdp.report.reportsettings")); //No I18N
}

function addNewRow(thisRow) {
    if (document.createElement && document.childNodes) {
        var gUniqueRowID = Math.round((999 - 100) * Math.random() + 1);
        //var thisRow = document.getElementById(rowId);
        var newElement = thisRow.cloneNode(true);
        newElement.id = "report_" + gUniqueRowID;
        //thisRow.parentNode.insertBefore(newElement,thisRow.nextSibling);
        thisRow.parentNode.insertBefore(newElement, thisRow.nextSibling);

        updateElementName(newElement, gUniqueRowID);
    }
}

function updateElementName(rowObj, newId) {

    var uvhId = null;
    for (var i = 0; i < rowObj.childNodes.length; i++) {
        if (rowObj.childNodes[i].nodeName == 'TD') {
            for (var j = 0; j < rowObj.childNodes[i].childNodes.length; j++) {
                var tags = rowObj.childNodes[i].childNodes[j];
                if (tags.nodeName == 'SELECT' || tags.nodeName == 'INPUT' || tags.nodeName == 'IMG') {
                    if (tags.name != undefined && tags.name.indexOf("filterColumn") >= 0) {
                        uvhId = 1100 + this.criteriaID;
                        tags.name = "filters[" + newId + "][filterColumn]";
                        tags.id = "filters[" + newId + "][filterColumn]";
                        this.criteriaID = this.criteriaID + 1;
                    }
                    else if (tags.name != undefined && tags.name.indexOf("selectCriteria") >= 0) {
                        tags.name = "filters[" + newId + "][selectCriteria]";
                        tags.id = "filters[" + newId + "][selectCriteria]";
                    }
                    else if (tags.name != undefined && tags.name.indexOf("matchType") >= 0) {
                        tags.name = "filters[" + newId + "][matchType]";
                        tags.id = "filters[" + newId + "][matchType]";
                    }
                    else if (tags.name != undefined && tags.name.indexOf("value") >= 0) {
                        tags.name = "filters[" + newId + "][value]";
                        tags.id = "filters[" + newId + "][value]";
                    }
                    else if (tags.name != undefined && tags.name.indexOf("button") >= 0) {
                        tags.name = "button_" + newId;
                        tags.id = "button_" + newId;
                        var browser = navigator.appName;
                        if (browser == "Netscape") {
                            tags.onclick = function (event) {
                                showDataPicker(this, event, uvhId);
                            };
                        }
                        else {
                            tags.onclick = function () {
                                showDataPicker(this, event, uvhId);
                            };
                        }
                    }
                }
            }
        }
    }
}

function removeRowWithID(theRow) {
    if (document.createElement && document.childNodes) {
        var thisRow = theRow.parentNode.parentNode;
        this.selectedRowId = thisRow.id;
        var idStr = "" + this.selectedRowId;   //No I18N
        if (idStr.indexOf("report_") === 0 && jQuery("tr[id^=report_]").length === 1)  //No I18N
        {
            var rowId = this.selectedRowId.split("_")[1];
            //Reset all selected data
            var filterId = getFilterColumnId(document.reportfilter).split("&")[1];
            document.getElementById(filterId + '_filterColumn_' + rowId).value = -1;
            document.getElementById('select_criteria_' + rowId).value = -1;
            document.getElementById('criteriaValue_' + rowId).value = "";
        }
        else {
            clearRowData(thisRow);
            //Removed the Effect script error occurs due to the delay
            //deleteRow(thisRow);
            //setTimeout('clearRowData();',1500);
        }
    }
}

function getFilterColumnId(formName) {
    var params = "";
    try {
        var elements_list = formName.elements;
        var length = elements_list.length;
        var element_type;
        for (var i = 0; i < length; i++) {
            element_type = elements_list[i].type;
            if (element_type == 'select-one' && elements_list[i].name.indexOf("filterColumn") >= 0) {
                params += "&" + elements_list[i].name.split("_")[0]; //No I18N
            }
        }
    }
    catch (e) {
    }
    return params;
}


function editScheduleReport(instanceId) {
    displayFadeMsg();
    callReportsAjaxRequest("/ReportSchedule.do", "mode=edit&instance_id=" + instanceId, 'input_page'); //No I18N
}

function deleteScheduleReport(instanceId) {
    sdpodconfirm({
        message: geti18nString("sdp.reports.customReport.scheduledeletemsg"), okFunc: function () {//No I18N
            callReportsAjaxRequest("/ReportSchedule.do", "mode=delete&instance_id=" + instanceId, 'delete_schedule_report'); //No I18N
            //deleteRow(document.getElementById('schedule_' + instanceId));
        }
    });
}

function scheduleReport(reportId) {
    //document.location = "/ReportSchedule.do?mode=new&reportName=" + reportId;
    displayFadeMsg();
    params = "mode=new&report_id=" + reportId;
    callReportsAjaxRequest("/ReportSchedule.do", params, 'start_page'); //No I18N
}

function clearRowData(thisRow) {
    //var thisRow = document.getElementById(this.selectedRowId);
    if (thisRow)
        thisRow.parentNode.removeChild(thisRow);
}

function deleteRow(rowObj) {
    for (var j = 0; j < rowObj.childNodes.length; j++) {
        if (rowObj.childNodes.item(j).nodeName == 'TD') {
            rowObj.childNodes.item(j).setAttribute('bgcolor', '#FAF8CC');
            new Effect.Fade(rowObj.childNodes.item(j), {duration: 1.15});
        }
    }
    new Effect.Fade(rowObj, {duration: 1.15});
}

function openMatrixReportWizard(tdTagId) {
    var divObj = document.getElementById(tdTagId);
    if (divObj.style.display == 'none') {
        divObj.style.display = 'block';
        if (tdTagId == 'rcstep2') {
            //document.getElementById('rcstep1').style.display = 'none';
            if (document.getElementById('rcstep2').innerHTML == '') {
                var param = "moduleID=" + parent['previousConfig'].moduleId + "&module=show_filter_page";	//No I18N
                callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_filter_page'); //No I18N
            }
            plusMinus(tdTagId);
        }
        else {
            //document.getElementById('rcstep2').style.display = 'none';
            //toggleSwipe1(tdTagId);
            plusMinus(tdTagId);
        }
    }
    else {
        toggleSwipe1(tdTagId);
        plusMinus(tdTagId);
    }
}

function setSummaryDataInPage() {
    if (parent['previousConfig']) {
        var repObj = parent['previousConfig'];
        if (repObj.cntCols) {
            jQuery.each(repObj.cntCols, function (idx, col) {
                jQuery('input[name="count"][value="' + col + '"]').prop('checked', true);//No I18N
            });
        }
        if (repObj.sumCols) {
            jQuery.each(repObj.sumCols, function (idx, col) {
                jQuery('input[name="sum"][value="' + col + '"]').prop('checked', true);//No I18N
            });
        }
        if (repObj.minCols) {
            jQuery.each(repObj.minCols, function (idx, col) {
                jQuery('input[name="min"][value="' + col + '"]').prop('checked', true);//No I18N
            });
        }
        if (repObj.maxCols) {
            jQuery.each(repObj.maxCols, function (idx, col) {
                jQuery('input[name="max"][value="' + col + '"]').prop('checked', true);//No I18N
            });
        }
        if (repObj.avgCols) {
            jQuery.each(repObj.avgCols, function (idx, col) {
                jQuery('input[name="average"][value="' + col + '"]').prop('checked', true);//No I18N
            });
        }
    }
}
function constructGroupingPage() {
    if (parent['previousConfig']) {
        var repObj = parent['previousConfig'];
        if (repObj.tabularGroupByColumn) {
            jQuery('#groupTabularColumn option[value="' + repObj.tabularGroupByColumn + '"]').prop('selected', true);//No I18N
        }
        if (repObj.orderBy != undefined && repObj.orderBy.length > 0) {
            jQuery.each(repObj.orderBy, function (arridx, orderbycol) {
                var pos = arridx + 1;
                jQuery('#orderBy' + pos + ' option[value="' + orderbycol + '"]').prop('selected', true);//No I18N
            });
        }
    }
}
function constructMatrixPage() {
    if (parent['previousConfig']) {
        var repObj = parent['previousConfig'];
        if (repObj.matrixReportType) {
            jQuery('#matrixReportType').val(repObj.matrixReportType);
            if ("simple" == repObj.matrixReportType) {
                jQuery("#simple-r").prop("checked", true);//No I18N
                swapLayer('simple', 'advanced');		//No I18N
                changeMatrixType('simple');	//No I18N
                jQuery('#simpleTopColumn option[value="' + repObj.simpleTopColumn + '"]').prop('selected', true);//No I18N
                jQuery('#simpleLeftColumn option[value="' + repObj.simpleLeftColumn + '"]').prop('selected', true);//No I18N
                jQuery('#simpleMatrixSummaryType option[value="' + repObj.simpleMatrixSummaryType + '"]').prop('selected', true);//No I18N
                jQuery('#simpleMatrixCountColumn option[value="' + repObj.simpleMatrixCountColumn + '"]').prop('selected', true);//No I18N
            }
            else if ("advanced" == repObj.matrixReportType) {
                jQuery("#advanced-r").prop("checked", true);//No I18N
                swapLayer('advanced', 'simple');	//No I18N
                changeMatrixType('advanced');	//No I18N
                jQuery('#matrixDateColumn option[value="' + repObj.matrixDateColumn + '"]').prop('selected', true);//No I18N
                jQuery('#matrixColumnGroupBy option[value="' + repObj.matrixColumnGroupBy + '"]').prop('selected', true); // No I18N
                jQuery('#matrixGroup1 option[value="' + repObj.matrixGroup1 + '"]').prop('selected', true);//No I18N
                jQuery('#matrixGroup2 option[value="' + repObj.matrixGroup2 + '"]').prop('selected', true);//No I18N
                jQuery('#matrixGroup3 option[value="' + repObj.matrixGroup3 + '"]').prop('selected', true);//No I18N
                jQuery('#matrixSummaryType option[value="' + repObj.matrixSummaryType + '"]').prop('selected', true);//No I18N
                jQuery('#matrixCountColumn option[value="' + repObj.matrixCountColumn + '"]').prop('selected', true);//No I18N

            }
        }
    }
}

function constructFilterPage() {
    if (parent['previousConfig']) {
        var repObj = parent['previousConfig'];
        if (repObj.datefilter) {
            jQuery("#dateFilterColumn option[value='" + repObj.datefilter.dateFilterColumn + "']").prop("selected", true);//No I18N
            repObj.datefilter.preDateFilterType;
            if ("predefined" == repObj.datefilter.preDateFilterType) {
                //predefined types
                jQuery('input[name="preDateFilterType"][value="customized"]').prop("checked", false); //No I18N
                jQuery('input[name="preDateFilterType"][value="predefined"]').prop("checked", true);  //No I18N
                jQuery("#dateFilterType option[value='" + repObj.datefilter.dateFilterType + "']").prop("selected", "selected");//No I18N
                getAbsDate();
                //TODO need to select this type of value.
            } else {//customized date filter
                jQuery('input[name="preDateFilterType"][value="predefined"]').prop("checked", false);   //No I18N
                jQuery('input[name="preDateFilterType"][value="customized"]').prop("checked", true);    //No I18N
                var fd = (repObj.datefilter.preFromDateDisp) ? repObj.datefilter.preFromDateDisp : repObj.datefilter.preFromDate;
                var td = (repObj.datefilter.preToDateDisp) ? repObj.datefilter.preToDateDisp : repObj.datefilter.preToDate;

                jQuery('#preFromDate_tmp').val(fd); //No I18N
                jQuery('#preToDate_tmp').val(td); //No I18N
            }
        } else {
            getAbsDate();
        }
        if (repObj.criterialist != undefined && repObj.criterialist.length > 0) {
            var c = 0;
            c = c - repObj.criterialist.length;
            for (var k = 0; k < repObj.criterialist.length; k++) {
                var cri = repObj.criterialist[k];
                var colName = cri.columnName;
                var criName = cri.criteriaName;
                var rowHtml = '<tr id="oldreport_' + c + '">';	//No I18N

                rowHtml += '<td></td><td style="vertical-align:middle;border:0px;"><input style="width: 0px;border: 0px;BACKGROUND-COLOR: transparent" name="' + c + '_filterColumn_' + c + '" readonly type="text" value="' + cri.columnID + '" id="' + c + '_filterColumn_' + c + '">' + colName + '</td>';	//No I18N
                rowHtml += '<td style="vertical-align:middle;border:0px;"><input title="' + getMessageForKey("sdp.report.common.edit.message") + '" style="width: 0px; BACKGROUND-COLOR: transparent;border: 0px solid #000000;" name="selectCriteria_' + c + '" readonly type="text" value="' + cri.criteriaID + '" id="select_criteria_' + c + '">' + criName + '</td>';		//No I18N
                rowHtml += '<td nowrap="nowrap" style="vertical-align:middle;border:0px;"><input name="criteriaValue_' + c + '" id="criteriaValue_' + c + '" readonly="readonly" value="" class="formStyle" style="width: 100%; cursor: pointer;" title="Click to edit." onclick="javascript:showDataPicker(this, event, \'' + c + '\');return false;" type="text">&nbsp;&nbsp;</td>';	//No I18N
                rowHtml += '<td style="vertical-align:middle;border:0px;">';	//No I18N
                rowHtml += '<select name="matchType_' + c + '" class="formStyle" id="matchType_' + c + '">';	//No I18N
                rowHtml += '<option value="and">AND</option>';	//No I18N
                rowHtml += '<option value="or">OR</option>';		//No I18N
                rowHtml += '</select></td><td width="20"></td><td width="20" style="vertical-align:middle;border:0px;"><span class="delete-ico" onclick="removeRowWithID(this);" style="vertical-align:middle;"></span></td></tr>';	//No I18N
                jQuery('#criteriaListTable tr:last').before(rowHtml);
                jQuery('select[name="matchType_' + c + '"] option[value="' + cri.matchType + '"]').prop('selected', true);//No I18N
                jQuery('#criteriaValue_' + c).val(cri.value.replace(/\\\"/g, '"'));
                c++;
            }
        }
    } else {
        getAbsDate();
    }
}
function openReportWizard(tdTagId) {
    var len = document.CustomReportHandlerForm.displayColumnList.options.length;
    var showstep1 = false;
    if ((tdTagId == 'rcstep5' || tdTagId == 'rcstep4') && len == 0) {
        sdpodalert({message: getMessageForKey("sdp.reports.customreport.selectcolumn")});
        //openReportWizard('rcstep1');
        tdTagId = 'rcstep1';
        showstep1 = true;
        //return;
    }

    for (var i = 1; i <= 5; i++) {
        tdObj = document.getElementById("rcstep" + i);

        if (("rcstep" + i) == tdTagId) {
            if (tdObj.style.display == 'none') {
                tdObj.style.display = 'block';
                plusMinus(tdTagId);
                if (trim(tdObj.innerHTML) == '') {
                    var param = "moduleID=" + parent['previousConfig'].moduleId;	//No I18N
                    if (tdTagId == 'rcstep2') {
                        sdpShowIndicator();
                        param += "&module=show_filter_page";	//No I18N
                        callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_filter_page'); //No I18N
                        return;
                    }
                    else if (tdTagId == 'rcstep3') {
                        sdpShowIndicator();
                        param += "&module=show_grouping_page";	//No I18N
                        callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_grouping_page'); //No I18N
                        return;
                    }
                    else if (tdTagId == 'rcstep4') {
                        var dispCols = document.getElementById("displayColumnList");
                        var length = dispCols.options.length;
                        if (length > 0) {
                            var addParams = "&" + constructParameters(document.summary_columns);
                            sdpShowIndicator();
                            param += "&module=show_summary_page&loadSummaryColumn=true";	//No I18N
                            for (var i = 0; i < length; i++) {
                                param += "&displayColumnList=" + dispCols.options[i].value;	//No I18N
                            }
                            param += addParams;
                            //param = appendPreviousReportConfig(param);
                            callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_summary_page'); //No I18N
                        }
                        return;
                    }
                    else if (tdTagId == 'rcstep5') {
                        updateGraphDetails(tdTagId, len, true);
                        return;
                    }
                }
                else if (tdTagId == 'rcstep4') {
                    var dispCols = document.getElementById("displayColumnList");
                    var length = dispCols.options.length;
                    if (length > 0) {
                        var addParams = "&" + constructParameters(document.summary_columns);
                        sdpShowIndicator();
                        var param = "moduleID=" + parent['previousConfig'].moduleId;	//No I18N
                        param += "&module=show_summary_page";	//No I18N
                        for (var i = 0; i < length; i++) {
                            param += "&displayColumnList=" + dispCols.options[i].value;
                        }
                        param += addParams;
                        //param = appendPreviousReportConfig(param);
                        callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_summary_page'); //No I18N
                    }
                    return;
                }
                else if (tdTagId == 'rcstep5') {
                    backupChartDetails(document.chart_types.chartType.value);
                    updateGraphDetails(tdTagId, len, false);
                    return;
                }
                /*
                 if( tdObj.style.display == 'none' )
                 {
                 //tdObj.style.display = 'block';
                 new Effect.BlindDown("rcstep" + i);
                 }*/
            }
            else {
                if (!showstep1) {
                    toggleSwipe1(tdTagId);
                    plusMinus(tdTagId);
                }
            }
        }
    }
}

function backupChartDetails(chartType) {
    //Backing up chart details
    if (chartType != -1) {
        this.selectedChartType = chartType;
        var url = "/servlet/SDODAuthServlet?ACTION=getChartName&CHART_ID=" + chartType;
        var ch_name = getAjaxResponse(url);
        if (ch_name == 'PieChart 2D' || ch_name == 'PieChart 3D' || ch_name == 'Ring Chart') {
            this.pieChartGroupColumn = document.chart_types.pieChartGroupColumn.value;
            this.pieChartDisplayFormat = document.chart_types.pieChartDisplayFormat.value;
        }
        else if (ch_name == 'BarChart 2D' || ch_name == 'BarChart 3D' || ch_name == 'BarChart Stacked 2D' || ch_name == 'BarChart Stacked 3D' || ch_name == 'Layered BarChart' || ch_name == 'Overlaid BarChart') {
            this.barXGroup1 = document.chart_types.barXGroup1.value;
            this.barXGroup2 = document.chart_types.barXGroup2.value;
            this.barChartBy = document.chart_types.barChartBy.value;
        }
        else if (ch_name == 'Line Chart') {
            this.lineXDate = document.chart_types.lineXDate.value;
            this.lineXDateFormat = document.chart_types.lineXDateFormat.value;
            this.lineXGroup = document.chart_types.lineXGroup.value;
        }
        else if (ch_name == 'TimeSeries Chart') {
            this.timeXDate = document.chart_types.timeXDate.value;
            this.timeXGroup = document.chart_types.timeXGroup.value;
            this.timeYCountColumn = document.chart_types.timeYCountColumn.value;
            this.timeXDateFormat = document.chart_types.timeXDateFormat.value;
        }
        else if (ch_name == 'Area Chart') {
            this.areaXDate = document.chart_types.areaXDate.value;
            this.areaXGroup = document.chart_types.areaXGroup.value;
            this.areaXDateFormat = document.chart_types.areaXDateFormat.value;
        }
        else if (ch_name == 'Step Chart') {
            this.stepXGroup1 = document.chart_types.stepXGroup1.value;
            this.stepYCountColumn = document.chart_types.stepYCountColumn.value;
            this.stepXGroup2 = document.chart_types.stepXGroup2.value;
        }
    }
}

function updateGraphDetails(tdTagId, len, loadingFirstTime) {
    sdpShowIndicator();
    var param = "moduleID=" + parent['previousConfig'].moduleId;	//No I18N
    param += "&module=show_graph_page";	//No I18N
    if (loadingFirstTime == true) {
        param += "&loadGraphDetails=true";
    }
    for (var i = 0; i < len; i++) {
        param += "&displayColumnList=" + document.CustomReportHandlerForm.displayColumnList.options[i].value;
    }
    callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_graph_page'); //No I18N
}


function updateItemListIntoText(selectTagObj, updateTo) {
    var len = selectTagObj.options.length;
    if (len > 0) {
        var textData = null;
        for (var i = 0; i < len; i++) {
            if (selectTagObj.options[i].selected) {
                if (textData == null) {
                    textData = '"' + selectTagObj.options[i].value + '"';
                }
                else {
                    textData += ',"' + selectTagObj.options[i].value + '"';
                }
            }
        }
        if (textData != null) {
            updateTo.value = textData;
        }
    }
}

function geti18nString(key) {
    return callSjaxRequest("/CustomReportHandler.do", "module=geti18nkey&key=" + key).responseText;
}

function openMatrixReportPage() {
    var matrixReportType = document.getElementById('matrixReportType').value;

    var param = "module=generate_matrix_report&matrixReportType=" + matrixReportType;
    try {
        param = checkAndAppendReportTitle(param, true);
    } catch (e) {
        return;
    }

    param = appendPreviousReportConfig(param);
    if (matrixReportType == "advanced") {
        var g1 = document.getElementById('matrixGroup1').value;
        var g2 = document.getElementById('matrixGroup2').value;
        var g3 = document.getElementById('matrixGroup3').value;

        var d1 = document.getElementById('matrixDateColumn').value;

        if (d1 == -1) {
            sdpodalert({message: document.getElementById("selectdatecolumn").innerHTML});
            return;
        }
        if (g1 == -1) {
            sdpodalert({message: document.getElementById("selectrowgroupcolumn").innerHTML});
            return;
        } else if ((g2 != -1 && g1 == g2) || (g3 != -1 && g1 == g3) || (g3 != -1 && g2 != -1 && g2 == g3)) {
            sdpodalert({message: document.getElementById("differentgroupby").innerHTML});
            return;
        } else if ((g1 == d1) || (g2 != -1 && g2 == d1) || (g3 != -1 && g3 == d1)) {
            sdpodalert({message: document.getElementById("samegroubyanddatecoloum").innerHTML});
            return;
        }

        //Updating matrix grouping column details
        if (document.advancedmatrix != undefined) {
            param += constructParameters(document.advancedmatrix);
        }
    }
    else {
        var LeftCol = document.getElementById('simpleLeftColumn').value;
        var TopCol = document.getElementById('simpleTopColumn').value;

        if (LeftCol == -1) {
            sdpodalert({message: document.getElementById("selectleftcol").innerHTML});
            return;
        }
        if (TopCol == -1) {
            sdpodalert({message: document.getElementById("selecttopcol").innerHTML});
            return;
        }
        if (LeftCol == TopCol) {
            sdpodalert({message: document.getElementById("samecolumns").innerHTML});
            return;
        }

        //Updating matrix grouping column details
        if (document.simplematrix != undefined) {
            param += constructParameters(document.simplematrix);
        }
    }

    //Updating filter column details
    if (document.reportfilter != undefined) {
        if (document.reportfilter.elements.length > 0) {
            param += "&filterEnabled=true";
        }
        var filter_param = constructFilterParameters(document.reportfilter);
        if (!filter_param) {
            sdpodalert({message: document.getElementById("entercriteriavalue").innerHTML});
            return false;
        }
        if (filter_param == 'true') {
            filter_param = '';
        }
        param += filter_param;

        //param += constructParameters(document.reportfilter);
    }

    displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
    callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_matrix_report_page'); //No I18N
}

function runReport(param) {
    parent.closeDialog();
    displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
    callReportsAjaxRequest("/CustomReportHandler.do", param, 'run_from_listview'); //No I18N
}

function openReportPage() {
    //this.criteriaID = 1;
    var param = "module=show_report_page";

    //Validating display column details
    var len = document.CustomReportHandlerForm.displayColumnList.length;
    if (len == 0) {
        //alert(geti18nString("sdp.reports.customReport.selectcolserr"));
        sdpodalert({message: document.getElementById("selectcolserr").innerHTML});
        return false;
    }

    var summaryCol = jQuery("form[name='summary_columns'] [columnid]").map(function (i, element) {
        return jQuery(element).attr("columnid");
    }).get();
    //Cleanup for summary columns.
    if (summaryCol.length > 0) {
        var displayColumn = jQuery(document.CustomReportHandlerForm.displayColumnList);
        jQuery.each(summaryCol, function (i, e) {
            if (displayColumn.find("option[value='" + e + "']").length == 0) {
                jQuery("form[name='summary_columns'] [columnid='" + e + "']").remove();
            }
        });
    }

    //Validating chart details
    var chartType = '-1'; // No I18N
    if (document.chart_types != undefined && document.chart_types.chartType != undefined) {
        chartType = document.chart_types.chartType.value;
        if (chartType === '-1') { // No I18N
            parent.previousConfig.chartId = undefined;
            parent.previousConfig.chartProperties = undefined;
        }
        param += "&chartEnabled=true"; // No I18N
    }

    if (chartType !== '-1') // No I18N
    {
        var url = "/servlet/SDODAuthServlet?ACTION=getChartName&CHART_ID=" + chartType;
        var ch_name = getAjaxResponse(url);
        if (ch_name == 'PieChart 2D' || ch_name == 'PieChart 3D' || ch_name == 'Ring Chart') {
            if (document.chart_types.pieChartGroupColumn.value == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
        else if (ch_name == 'Multiple PieChart') {
            if (document.chart_types.pieChartGroupColumn.value == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
            if (document.chart_types.pieChartDataColumn.value == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
        else if (ch_name == 'BarChart 2D' || ch_name == 'BarChart 3D' || ch_name == 'BarChart Stacked 2D' || ch_name == 'BarChart Stacked 3D' || ch_name == 'Layered BarChart' || ch_name == 'Overlaid BarChart') {
            if (document.chart_types.barXGroup1.value == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
        else if (ch_name == 'Line Chart') {
            var date_col = document.chart_types.lineXDate.value;
            var group_col = document.chart_types.lineXGroup.value;
            //var count_col = document.CustomReportHandlerForm.lineYCountColumn.value;
            if (date_col == "-1" || group_col == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
        else if (ch_name == 'TimeSeries Chart') {
            var date_col = document.chart_types.timeXDate.value;
            var group_col = document.chart_types.timeXGroup.value;
            var count_col = document.chart_types.timeYCountColumn.value;
            if (date_col == "-1" || group_col == "-1" || count_col == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
        else if (ch_name == 'Area Chart') {
            var date_col = document.chart_types.areaXDate.value;
            var group_col = document.chart_types.areaXGroup.value;
            //var count_col = document.CustomReportHandlerForm.areaYCountColumn.value;
            if (date_col == "-1" || group_col == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
        else if (ch_name == 'Step Chart') {
            var date_col = document.chart_types.stepXGroup1.value;
            //var group_col = document.CustomReportHandlerForm.stepXGroup.value;
            var count_col = document.chart_types.stepYCountColumn.value;
            if (date_col == "-1" || count_col == "-1") {
                //alert(geti18nString( "sdp.reports.customReport.mandatorymsg" ));
                sdpodalert({message: document.getElementById("mandatorymsg").innerHTML});
                return;
            }
        }
    }
    //Updating display column list
    for (var i = 0; i < len; i++) {
        param += "&displayColumnList=" + document.CustomReportHandlerForm.displayColumnList[i].value;
    }
    //Updating filter column details
    if (document.reportfilter != undefined) {
        if (document.reportfilter.elements.length > 0) {
            param += "&filterEnabled=true";
        }
        var filter_param = constructFilterParameters(document.reportfilter);
        if (!filter_param) {
            sdpodalert({message: document.getElementById("entercriteriavalue").innerHTML});
            return false;
        }
        if (filter_param == 'true') {
            filter_param = '';
        }
        param += filter_param;
    }

    //Updating group column details
    if (document.group_columns != undefined) {
        if (document.group_columns.elements.length > 0) {
            param += "&groupEnabled=true";
        }
        param += constructParameters(document.group_columns);
    }
    //Updating summary column list

    if (document.summary_columns != undefined) {
        if (document.summary_columns.elements.length > 0) {
            param += "&summaryEnabled=true";
        }
        param += constructParameters(document.summary_columns);
    }
    //Updating chart details
    if (chartType !== '-1') // No I18N
    {
        if (document.chart_types != undefined) {
            param += constructParameters(document.chart_types);
        }
    }


    //Passing addition parameter if the report is newly generated one
    if (document.CustomReportHandlerForm.newReport != null) {
        param += "&newReport=true&reportType=" + document.CustomReportHandlerForm.reportType.value;
        param += "&moduleID=" + document.CustomReportHandlerForm.moduleID.value;
    }
    try {
        param = checkAndAppendReportTitle(param, true);
    } catch (e) {
        return;
    }

    param = appendPreviousReportConfig(param);
    displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
    callReportsAjaxRequest("/CustomReportHandler.do", param, 'show_report_page'); //No I18N
    return false;
}

function go_back() {
    document.location = "/CustomReportHandler.do?module=start_page";
}

function updateAllSelectTag(col_details) {
    //Updating display column list
    var selectTag = document.getElementById('fromList');
    addSelectTagData(selectTag, null);
}

function addSelectTagData(selectTag, filter) {
    var len = col_details.length;
    var optionSize = selectTag.options.length;
    var count = 0;
    var filter_data_types = null;
    if (filter != null) {
        filter_data_types = filter.split(",");
    }
    for (var i = 0; i < len; i++) {
        if ((filter != null && isDataTypeMaching(col_details[i].data_type, filter_data_types) ) || filter == null) {
            selectTag.options[optionSize + count] = new Option(col_details[i].display_name, col_details[i].column_id);
            selectTag.options[optionSize + count].id = col_details[i].column_id;
            count++;
        }
    }
}

function isDataTypeMaching(data_type, filter_type) {
    var len = filter_type.length;
    for (var i = 0; i < len; i++) {
        if (data_type == filter_type[i]) {
            return true;
        }
    }
    return false;
}

function isDateColumn(column_id) {
    var len = col_details.length;
    for (var i = 0; i < len; i++) {
        if (col_details[i].column_id == column_id && col_details[i].data_type == 6) {
            return true;
        }
    }
    return false;
}
/** TODO NEED to check and remove this method */
function summaryReportPrintPreview(report_id) {
    print_view(report_id);
}

function print_view(report_id, isAuditReport) {
    var report_file = "/common/PrintConf.jsp?printModule=OtherReport";//No I18N
    if (report_id != null) {
        report_file += "&report_id=" + report_id;  //No I18N
    }
    sdpShowURLInPopup(report_file, 'width=1000,modal=true,resizable=true,title=' + jQuery('#printpreviewlink').html());//No I18N
}

function graph_view(module, report_id) {
    var url = "CustomReportHandler.do"; //No I18N
    var params = "module=graph_view&module_id=" + module;	//No I18N

    if (report_id != null) {
        params += "&report_id=" + report_id;	//No I18N
    }
    params = appendPreviousReportConfig(params);
    jQuery.ajax({
        url: url,
        context: document.body,
        type: 'POST',        //No I18N
        data: params,
        headers: {"If-Modified-Since": 'Thu, 1 Jan 1970 00:00:00 GMT'}, //No I18N
        success: function (data) {
            sdpShowPopup(data, 'title=Graph View,width=900,height=600,modal=true');//No I18N
        }
    }).fail(function (jqXHR, textStatus) {
        sdpodalert({title: getMessageForKey('alert.title'), message: 'Failed to load the graph'});	//No I18N
    });
}

function editFreshSummaryReport() {
    var param = "module=show_available_subreports&reportType=Summary Reports";	//No I18N
    param = appendPreviousReportConfig(param);
    callReportsAjaxRequest("/SummaryReportHandler.do", param, 'show_available_subreports'); //No I18N
}

function editFreshReport(reportType) {
    document.getElementById('report_input_pages').style.display = "block"; // No I18N
    //new Effect.BlindDown('report_input_pages');
    document.getElementById('report_result').innerHTML = "";
    sdpHideIndicator();
}

function editReport(reportId, reportTypeId, isFreshReport) {
    parent.closeDialog();
    sdpShowIndicator();
    if (reportTypeId == 6) {
        editAuditReport(reportId, isFreshReport);
    }
    else if (isFreshReport == "true") {
        if (reportTypeId == 5) {
            editFreshSummaryReport();

        } else {
            editFreshReport();
        }
    }
    else if (reportTypeId == 5) {
        var param = "module=edit_report&report_id=" + reportId;
        callReportsAjaxRequest('/SummaryReportHandler.do', param, 'edit_report_page'); //No I18N
    }
    else if (reportTypeId != 2) {
        var param = "module=edit_report&report_id=" + reportId;
        callReportsAjaxRequest('/CustomReportHandler.do', param, 'edit_report_page'); //No I18N
    }
    else if (reportTypeId == 2) {
        //var param = "module=edit_qreport&report_id=" + reportId;
        //callReportsAjaxRequest('/CustomReportHandler.do', param, 'edit_qreport');
        sdpodalert({message: getMessageForKey('sdp.reports.query.cannot.edit')});
        sdpHideIndicator();
    }

}

function nextPage(page_index, total_pages, module, report_id, url) {
    if (page_index != total_pages) {
        var param = "module=generate_next_report_page&PAGE_INDEX=" + page_index + "&TOTAL_PAGES=" + total_pages + "&module_id=" + module + "&report_id=" + report_id; //No I18N

        sdpShowIndicator();
        param = appendPreviousReportConfig(param);
        callReportsAjaxRequest(url, param, 'show_report_page'); //No I18N
    }
}

function prevPage(page_index, total_pages, module, report_id, url) {
    if (page_index != '-1') {
        var param = "module=generate_next_report_page&PAGE_INDEX=" + page_index + "&TOTAL_PAGES=" + total_pages + "&module_id=" + module + "&report_id=" + report_id; //No I18N
        sdpShowIndicator();
        param = appendPreviousReportConfig(param);
        callReportsAjaxRequest(url, param, 'show_report_page'); //No I18N
    }
}

/*function showQuery( status, report_type, e )
 {
 if( report_type == "QTabular Reports" )
 {
 showURLInDialog('CustomReportHandler.do?module=show_query&queryreport=true','closeButton=yes,position=absolute,width=500,left=' + (e.screenX - 370) +',top=' + (e.screenY -  150));
 }
 else if( status == 'fresh' )
 {
 showURLInDialog('CustomReportHandler.do?module=show_query&freshreport=true','closeButton=yes,position=absolute,width=500,left=' + (e.screenX - 370) +',top=' + (e.screenY -  150));
 }
 else
 {
 showURLInDialog('CustomReportHandler.do?module=show_query','closeButton=yes,position=absolute,width=500,left=' + (e.screenX - 370) +',top=' + (e.screenY -  150));
 }
 }*/

function setFocus() {
    document.getElementById('folder_name').focus();
}

function checkAvailabilityFolder() {
    var folder_name = document.getElementById("folder_name").value;
    if (trim(folder_name) !== "") {
        if (!nameCheck(folder_name, "reportFolder", '')) {
            document.getElementById('folder_name').focus();
            return false;
        }
        folder_name = encodeURIComponent(folder_name);
        callReportsAjaxRequest("/CustomReportHandler.do", "module=create_new_folder&folder_name=" + folder_name, 'create_new_folder'); //No I18N
    }
}

function getFolderDesc(selectTagObj) {
    callReportsAjaxRequest("/CustomReportHandler.do", "module=get_folder_desc&folder_id=" + selectTagObj.value, 'get_folder_desc', 'GET'); //No I18N
}

function openSaveReportAsWindow(e, reportId, title) {
    sdpShowURLInPopup('CustomReportHandler.do?module=duplicate_report', 'title=' + title + ',width=580,resizable=true,modal=true'); //No I18N
}

function openSaveReportWindow(e, title) {
    sdpShowURLInPopup('CustomReportHandler.do?module=save_report', 'title=' + title + ',width=580,resizable=true,modal=true'); //No I18N
}

function saveReport(reportType) {
    var selectedReportFolder = document.getElementById('folderId').value;
    var reportName = document.getElementById('reportName').value;

    if (trim(reportName) == '') {
        sdpodalert({message: 'Report name missing!!!'});//No I18N
        return;
    }
    var ispublic = "private";
    if (document.getElementById('public').checked) {
        ispublic = 'public';
    }
    var desc = document.getElementById('desc').value;

    sdpShowIndicator();

    //For matrix report
    var matrixReportType = "";
    if (document.reportfilter != undefined && document.getElementById('matrixReportType') != undefined) {
        matrixReportType = "&matrixReportType=" + document.getElementById('matrixReportType').value;
    }

    //var ajaxIdentifier = 'save_report_as';
    //var url = "/CustomReportHandler.do";

    var param = "module=save_report_as&folderId=" + selectedReportFolder + "&reportName=" + encodeURIComponent(reportName) + "&ispublic=" + encodeURIComponent(ispublic) + "&desc=" + encodeURIComponent(desc) + matrixReportType; //No I18N
    param = appendPreviousReportConfig(param);
    loadAjaxURL({
        url: '/CustomReportHandler.do',	//No I18N
        data: param,
        success: function (data) {
            var response = data;
            if (response.indexOf('success') == 0) {
                var reportId = response.split("_")[1];
                updateReportPageLinks(reportId, '/CustomReportHandler.do?module=export'); //No I18N
                if (parent.previousConfig) {
                    parent.previousConfig.folderId = selectedReportFolder;//No I18N
                    parent.previousConfig.reportName = reportName;
                    parent.previousConfig.reportDesc = desc;
                    parent.previousConfig.isPublic = (ispublic === 'public'); //No I18N
                    parent.previousConfig.freshReport = false;
                    parent.previousConfig.displayReportName = reportName;
                }
            }
            else {
                sdpShowIndicator({message: getMessageForKey('sdp.failure') + ' ' + response, success: false});
            }
        }
    });
    //callReportsAjaxRequest(url, param, ajaxIdentifier);
}

function isValidEmailId(toAddress) {
    var validto = validateEMailIDs(toAddress);
    if (!validto) {
        return false;
    }
    return true;
}


function send() {

    if (document.getElementById('report_id') != null) {
        if (isValidEmailId(document.getElementById('toEmailSearch'))) {
            var param = "module=emailthisreport&to=" + encodeURIComponent(document.getElementById('toEmailSearch').value) + "&Message=" + encodeURIComponent(document.getElementById('Message').value) + "&emailsubject=" + encodeURIComponent(document.getElementById('emailsubject').value) + "&file_type=" + encodeURIComponent(document.getElementById('file_type').value) + "&report_id=" + encodeURIComponent(document.getElementById('report_id').value); //No I18N
            sdpShowIndicator({message: getMessageForKey('sdp.purchase.newpo.sendmail.sending')});
            param = appendPreviousReportConfig(param);
            callReportsAjaxRequest("/CustomReportHandler.do", param, 'send_mail'); //No I18N
        }
    }
    else if (isValidEmailId(document.getElementById('toEmailSearch'))) {
        var param = "module=emailthisreport&to=" + encodeURIComponent(document.getElementById('toEmailSearch').value) + "&Message=" + encodeURIComponent(document.getElementById('Message').value) + "&emailsubject=" + encodeURIComponent(document.getElementById('emailsubject').value) + "&file_type=" + encodeURIComponent(document.getElementById('file_type').value);
        sdpShowIndicator({message: getMessageForKey('sdp.purchase.newpo.sendmail.sending')});
        param = appendPreviousReportConfig(param);
        callReportsAjaxRequest("/CustomReportHandler.do", param, 'send_mail'); //No I18N
    }
}

function sendMail(reportId, e, title) {
    if (reportId != null) {
        sdpShowURLInPopup("/reports/sendmail.jsp?report_id=" + reportId, 'width=auto,modal=true,title=' + title); //No I18N
    }
    else {
        sdpShowURLInPopup("/reports/sendmail.jsp", 'width=auto,modal=true,title=' + title); //No I18N
    }
}

function saveReportAs(reportId) {
    var selectedReportFolder = document.getElementById('folderId').value;
    var reportName = document.getElementById('reportName').value;

    if (trim(reportName) == '') {
        sdpodalert({message: 'Report name missing!!!'});//No I18N
        return;
    }
    var ispublic = "private";
    if (document.getElementById('public').checked) {
        ispublic = 'public';
    }
    var desc = document.getElementById('desc').value;

    sdpShowIndicator();
    var params = "module=create_duplicate_report&folderId=" + selectedReportFolder + "&reportName=" + encodeURIComponent(reportName) + "&ispublic=" + encodeURIComponent(ispublic) + "&desc=" + encodeURIComponent(desc);	//No I18N
    params = appendPreviousReportConfig(params);
    callReportsAjaxRequest("/CustomReportHandler.do", params, 'create_duplicate_report'); //No I18N
}

function gotoReportInputPage() {
    var report_selected = false;
    var report_name = "";
    var reportTypeObj = document.getElementsByName('reportType');
    for (var i = 0; i < reportTypeObj.length; i++) {
        if (reportTypeObj[i].checked == true) {
            report_name = reportTypeObj[i].value;
            report_selected = true;
            break;
        }
    }
    if (!report_selected) {
        //alert(geti18nString("sdp.reports.customReport.reporttype"));
        sdpodalert({message: document.getElementById("reporttype").innerHTML});
        return;
    }
    var moduleIdObj = document.getElementsByName('moduleID');
    var module_selected = false;
    for (var i = 0; i < moduleIdObj.length; i++) {
        if (moduleIdObj[i].checked == true) {
            module_selected = true;
            break;
        }
    }
    if (!module_selected) {
        //alert(geti18nString("sdp.reports.customReport.selectmodule"));
        sdpodalert({message: document.getElementById("selectmodule").innerHTML});
        return;
    }
    var param = "";
    try {
        param = checkAndAppendReportTitle(param, true);
    } catch (e) {
        return;
    }
    if (report_name == "Matrix Reports") {
        param = "module=show_matrix_page"; //No I18N
        param += "&" + getNewReportParameters();
        displayFadeMsg();
        callReportsAjaxRequest('/CustomReportHandler.do', param, 'input_page'); //No I18N
    }
    else if (report_name == "Summary Reports" || report_name == "Audit Reports") {
        var moduleID = null;
        for (i = 0; i < moduleIdObj.length; i++) {
            if (moduleIdObj[i].checked == true) {
                moduleID = moduleIdObj[i].value;
                break;
            }
        }
        if (report_name == "Summary Reports") {
            param += "&module=show_available_subreports&moduleID=" + moduleID + "&reportType=Summary Reports";	//No I18N
            displayFadeMsg();
            callReportsAjaxRequest("/SummaryReportHandler.do", param, 'show_available_subreports'); //No I18N
        }
        else if (report_name == "Audit Reports") {
            param += "&module=auditmainpage&moduleID=" + moduleID + "&reportType=Audit Reports";//No I18N
            displayFadeMsg();
            callReportsAjaxRequest("/AuditHistoryReport.do", param, 'show_audit_main_page'); //No I18N
        }
    }
    else {
        var param = "module=show_input_page";

        param += "&" + getNewReportParameters();
        displayFadeMsg();
        callReportsAjaxRequest('/CustomReportHandler.do', param, 'input_page'); //No I18N
    }
}

//Will be invoked this method when the user clicked the previous button from summary report criteria page
function showSubReportsPage() {
    var param = "module=show_available_subreports"; //No I18N
    param = appendPreviousReportConfig(param);
    callReportsAjaxRequest("/SummaryReportHandler.do", param, 'show_available_subreports'); //No I18N
}

function getNewReportParameters() {
    var reportTypeObj = document.getElementsByName('reportType');
    var moduleIdObj = document.getElementsByName('moduleID');
    var moduleId = null;
    var param = "newReport=true";
    var length = moduleIdObj.length;
    for (var i = 0; i < length; i++) {
        if (moduleIdObj[i].checked) {
            moduleId = moduleIdObj[i].value;
            param += "&moduleID=" + moduleId;//No I18N
        }
    }
    length = reportTypeObj.length;
    for (var i = 0; i < length; i++) {
        if (reportTypeObj[i].checked) {
            param += "&reportType=" + reportTypeObj[i].value;
        }
    }
    if (document.getElementById("mf_" + moduleId)) {//No I18N
        param += "&moduleFilter=" + document.getElementById("mf_" + moduleId).value;//No I18N
    }
    try {
        param = checkAndAppendReportTitle(param, false); // this will be called for already checked case only.
    } catch (error) {
        logConsole(error);
    }
    return param;
}

function createNew() {
    parent.closeDialog();
    displayFadeMsg();
    callReportsAjaxRequest("/CustomReportHandler.do", "module=start_page", 'start_page'); //No I18N
}

function getAvailableModules(reportTypeId) {
    if (reportTypeId != -1) {
        var report_type = document.getElementById(reportTypeId).value;
        changeImage(report_type);
    }
    callReportsAjaxRequest('/CustomReportHandler.do', "module=get_available_module&report_type=" + reportTypeId, 'get_available_module'); //No I18N
}

function changeMatrixType(matrixType) {
    document.getElementById('matrixReportType').value = matrixType;

    /*var browser = navigator.appName;
     var classname = 'className';
     if(browser == "Netscape")
     {
     classname = 'class';
     }*/

    /*if( document.getElementById('subtabon').getAttribute(classname) == undefined || document.getElementById('subtabon').getAttribute(classname) == 'subtabon' )
     {
     document.getElementById('subtabon').setAttribute(classname, 'subtaboff');
     document.getElementById('subtaboff').setAttribute(classname, 'subtabon');
     }
     else
     {
     document.getElementById('subtabon').setAttribute(classname, 'subtabon');
     document.getElementById('subtaboff').setAttribute(classname, 'subtaboff');
     }*/
}

function goReportHome() {
    //document.location = "/CustomReportHandler.do";
    displayFadeMsg();
    callReportsAjaxRequest("/CustomReportHandler.do", "module=change_view&filterView=all_reports", 'input_page'); //No I18N
}

function showAllColumns(textObj) {
    if (trim(textObj.value) != '') {
        callReportsAjaxRequest('/CustomReportHandler.do', '&module=get_sql_columns&query=' + textObj.value, 'get_sql_columns'); //No I18N
    }
}

function runSQLReport() {
    if (trim(document.getElementById('query').value) == "") {
        //alert(geti18nString( "sdp.reports.queryreports.queryexcep" ));
        sdpodalert({message: document.getElementById("queryexcep").innerHTML});
        return;
    }
    if (trim(document.getElementById('reportTitle').value) == "") {
        //alert(geti18nString( "sdp.reports.queryreports.titleexcep" ));
        sdpodalert({message: document.getElementById("titleexcep").innerHTML});
        return;
    }
    document.getElementById("errorlog").value = "";

    sdpShowIndicator();

    var param = "module=run_query_editor_query" + constructParameters(document.queryreportform);
    callReportsAjaxRequest("/CustomReportHandler.do", param, 'run_query_editor'); //No I18N
}

/*function getTableSchema(obj)
 {
 var module_id = obj.value;
 if( module_id != -1 )
 {
 //alert('Under construction...');
 NewWindow("/reports/show_schema.jsp?module_id=" + module_id,'table_schema','950','600','yes','center');
 }
 else
 {
 //alert(geti18nString( "sdp.reports.queryreports.pleaseselectmod" ));
 sdpodalert({message:document.getElementById("pleaseselectmod").innerHTML});
 return;
 }
 }*/

function deleteReport(reportId) {
    parent.closeDialog();
    var param = "module=can_delete&report_id=" + reportId;
    callReportsAjaxRequest("/CustomReportHandler.do", param, 'can_delete'); //No I18N
}

function changeView(selectTag, userId) {
    parent.closeDialog();
    document.cookie = "uselectedView_" + userId + "=" + selectTag.value + ";expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/";

    var date = new Date();
    date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
    document.cookie = "uselectedView_" + userId + "=" + selectTag.value + expires + "; path=/";

    if (selectTag.value == 'all_schedule_reports') {
        showScheduledReportListView();
    }
    else {
        callReportsAjaxRequest("/CustomReportHandler.do", "module=change_view&filterView=" + selectTag.value, 'input_page'); //No I18N
    }
}

function displayFadeMsg(msg) {
    sdpShowIndicator({message: msg});
}

/** Class syntax for storing report column details */
function ColumnDetails(column_id, display_name, data_type, order_id) {
    this.column_id = column_id;
    this.display_name = display_name;
    this.data_type = data_type;
    this.order_id = order_id;
}

function deleteFolder(folder_id, event) {
    cancelBubble(event);
    sdpodconfirm({
        title: getMessageForKey('alert.title'),
        message: geti18nString("sdp.reports.customReport.folderdeletemsg"),
        okFunc: function () { //No I18N
            sdpShowIndicator();
            loadAjaxURL({
                url: "/CustomReportHandler.do",//No I18N
                data: "module=delete_folder&folder_id=" + folder_id,//No I18N
                type: "POST",//No I18N
                datatype: 'json',//No I18N
                failedCB: function (jqXHR, status) {
                    sdpShowIndicator({success: false, message: jqXHR.responseText});
                },
                success: function (data) {
                    handleJsonActionResponse(data, {
                        EXECUTE_FUNC: function (res) {
                            var temp = "tr[rel='" + folder_id + "']";//No I18N
                            var $folderrow = jQuery(temp);
                            var divId = '#report' + $folderrow.attr('id').replace('folder', '');//No I18N
                            jQuery(divId).remove();
                            $folderrow.remove();

                        }
                    });
                }
            });
        }
    });

}

function editFolderName(folderId, event) {
    cancelBubble(event);
    if (folderId != null) {
        sdpShowURLInPopup('/CustomReportHandler.do?module=edit_folder&folderId=' + folderId, 'width=auto,modal=true,title=' + jQuery('#RenameFolderTitle').html()); //No I18N
    }
    else {
        sdpShowURLInPopup('/CustomReportHandler.do?module=edit_folder', 'width=auto,modal=true,title=' + jQuery('#AddFolderTitle').html()); //No I18N
    }
}

function callReportsAjaxRequest(url, params, requestID, queryMethod, onsuccessfunc, onfailfunc) {
    try {
        if (onsuccessfunc == undefined) {
            onsuccessfunc = function (data) {
                ajaxRequestOnSuccess(data, requestID);
            };
        }
        if (onfailfunc == undefined) {
            onfailfunc = function (jqXHR, textStatus) {
                sdpodalert({
                    title: getMessageForKey('alert.title'),
                    message: jqXHR.responseText,
                    action: sdpHideIndicator
                }); //No I18N
                ajaxRequestOnFailure(jqXHR, requestID);
            };
        }
        if (queryMethod == null || queryMethod == undefined) {
            queryMethod = 'POST'; // No I18N
        }

        if (queryMethod == 'POST') { // No I18N
            params = appendCSRFParam(params);
        }
        params += "&SUBREQUEST=XMLHTTP"; //No I18N
        jQuery.ajax({
            url: url,
            data: params,
            context: document.body,
            type: queryMethod,        //No I18N
            headers: {"If-Modified-Since": 'Thu, 1 Jan 1970 00:00:00 GMT'}, //No I18N
            data: params,
            success: onsuccessfunc
        }).fail(onfailfunc);
    }
    catch (e) {
        sdpodalert({title: requestID, message: "Error while sending the request : " + e.message});	 //No I18N
    }
}


function saveFolderDetails() {
    if (!jqValidateForm(document.editFolder)) {
        return false;
    }
    var folderId = document.getElementById('folderId').value;
    var folderName = document.getElementById('folderName').value;
    folderName = encodeURIComponent(document.getElementById('folderName').value);
    var folderDesc = encodeURIComponent(document.getElementById('folderDesc').value);

    var params = "module=update_folder"; //No I18N
    var actionstr = "add_folder"; // No I18N
    if (folderId !== 'null') {
        params += "&folderId=" + folderId; //No I18N
        actionstr = "update_folder"; // No I18N
    }
    params += "&folderName=" + folderName + "&folderDesc=" + folderDesc;	//No I18N

    callReportsAjaxRequest('/CustomReportHandler.do', params, actionstr); //No I18N
    return false;
}

/*
 * Function for constructing parameter name & value(s) for the requested form
 * @param formName - request parameter(s) to be constructed for the form name
 * @return this will return the chain of param name & value pair
 */
function constructFilterParameters(formName) {
    var params = "";
    try {
        var elements_list = formName.elements;
        var length = elements_list.length;
        var element_type;
        for (var i = 0; i < length; i++) {
            element_type = elements_list[i].type;
            if (element_type == 'textarea' || element_type == 'text' || element_type == 'password') {
                var value = encodeURIComponent(elements_list[i].value);
                var name = elements_list[i].name;
                if (value == null || value == "") {
                    if (name.indexOf("criteriaValue") != -1) {
                        var id = name.substring(14);
                        if (document.getElementById("select_criteria_" + id).value != -1) {
                            return false;
                        }
                    }
                }
                params += "&" + name + "=" + value;
            }
            else if (element_type == 'checkbox' && elements_list[i].checked) {
                params += "&" + elements_list[i].name + "=" + encodeURIComponent(elements_list[i].value);
            }
            else if (element_type == 'select-one') {
                if (elements_list[i].name.indexOf("filterColumn") >= 0) {
                    params += "&" + elements_list[i].name + "=" + encodeURIComponent(elements_list[i].value);
                }
                else {
                    params += "&" + elements_list[i].name + "=" + encodeURIComponent(elements_list[i].value);
                }
            }
            else if (element_type == 'select-multiple') {
                var size = elements_list[i].options.length;
                for (var j = 0; j < size; j++) {
                    if (elements_list[i].options[j].selected) {
                        params += "&" + elements_list[i].name + "=" + encodeURIComponent(elements_list[i].options[j].value);
                    }
                }
            }
            else if (element_type == 'radio') {
                if (elements_list[i].checked) {
                    params += "&" + elements_list[i].name + "=" + encodeURIComponent(elements_list[i].value);
                }
            }
        }
    }
    catch (e) {
        sdpodalert({message: "Error while constructing request parameter : " + e.message});//No I18N
    }
    if (params == '') {
        params = 'true';
    }
    return params;
}

/**
 * Function to cenvert the long value into date format
 * @param longDate - containing date long value
 */
function getDate(longDate) {
    //var browser = navigator.appName;
    var dateObj = new Date();
    if (longDate != '' && longDate != null && longDate > 0) {
        dateObj.setTime(longDate);
    }
    var year = dateObj.getFullYear();
    /*if( browser == "Netscape" )
     {
     year = dateObj.getYear() + 1900;
     }
     else
     {
     year = dateObj.getYear();
     }*/
    var month = dateObj.getMonth() + 1;
    var date = dateObj.getDate();
    monthstr = (month < 10 ? "0" + month : month);
    datestr = (date < 10 ? "0" + date : date);
    return year + "-" + monthstr + "-" + datestr;
}

/**
 * Function used in ReportColumnDataSelector.jsp
 */
function addToValueBox(index, dataType) {
    var seletedItemList;
    //var criteriaName = document.getElementById('select_criteria_' + index);
    seletedItemList = document.getElementById('criteriaValue_' + index);

    var itemList = document.getElementById('itemList');

    if (dataType == "java.sql.Time") {
        var hours = trim(document.getElementById('TIMESPENTHH').value);
        var minutes = trim(document.getElementById('TIMESPENTMM').value);
        if (validatePickValues(hours, dataType) == "Y" || validatePickValues(minutes, dataType) == "Y") {
            //alert( "sdp.reports.errmsg.numberformatexception" );
            sdpodalert({message: document.getElementById("numberformatexception").innerHTML});
            return;
        }
        if (trim(hours) == "" && trim(minutes) == "") {
            //alert("sdp.reportcols.warnmessage.enterhhmm");
            sdpodalert({message: document.getElementById("enterhhmm").innerHTML});
            return;
        }
        else {
            seletedItemList.value = hours + " : " + minutes;
        }
    }
    else if (dataType == "javax.lang.Memory") {
        if (trim(itemList.value) == "") {
            itemList.value = 0;
        }

        if (!isInteger(itemList.value)) {
            sdpodalert({message: document.getElementById("numberformatexception").innerHTML});
            return;
        }
        if (itemList.value.indexOf("MB") == -1) {
            seletedItemList.value = itemList.value + " MB";
        }
        else {
            seletedItemList.value = itemList.value;
        }
    }
    else {
        if (trim(itemList.value) == "") {
            //alert("sdp.reports.errmsg.invalidtimedateexception");
            sdpodalert({message: document.getElementById("numberformatexception").innerHTML});
            return;
        }
        if (validatePickValues(itemList.value, dataType) != "Y") {
            seletedItemList.value = itemList.value;
        }
        else {
            //alert( "sdp.reports.errmsg.numberformatexception" );
            sdpodalert({message: document.getElementById("numberformatexception").innerHTML});
            return;
        }
    }
    parent.sdpClosePopup();
}

function validatePickValues(value, dataType) {
    if (dataType == "java.lang.Double") {
        if (!isDouble(value)) {
            value = "Y";
        }
    }
    else if (dataType == "java.lang.Long" || dataType == "java.lang.Integer" || dataType == "javax.lang.Memory" || dataType == "java.sql.Time") {
        if (!isInteger(value)) {
            value = "Y";
        }
    }
    return value;
}
function isDouble(str) {
    var objRegExp = /^\d\d*(.\d\d*)?$/;
    return objRegExp.test(str);
}

function showSummaryCriteriaPage() {
    var selectedSubReports = document.getElementById('displaySubReportList');
    var len = selectedSubReports.length;
    if (len == 0) {
        //alert("sdp.reports.customReport.selectcolserr");
        sdpodalert({message: document.getElementById("selectcolserr").innerHTML});
        return false;
    }
    if (len > 0) {
        var param = "module=show_criteria_page";
        for (var i = 0; i < len; i++) {
            param += "&displaySubReportList=" + selectedSubReports.options[i].value;
        }
        param = appendPreviousReportConfig(param);
        displayFadeMsg();
        callReportsAjaxRequest("/SummaryReportHandler.do", param, 'show_summary_filter_page'); //No I18N
    }
    return false;
}

//Function to run the summary report
function runSummaryReport(reportId) {
    parent.closeDialog();
    if (reportId != undefined) {
        var param = "module=generate_report&report_id=" + reportId;
        displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
        callReportsAjaxRequest("/SummaryReportHandler.do", param, 'show_report_page'); //No I18N
    }
    else {
        var param = "module=show_report_page&filterEnabled=true";	//No I18N
        if (document.reportfilter != undefined) {
            param += constructParameters(document.reportfilter);
        }
        var selectedSubReports = document.getElementById('displaySubReportList');
        if (selectedSubReports != undefined) {
            var len = selectedSubReports.length;
            if (len == 0) {
                sdpodalert({message: getMessageForKey("selectcolserr")});
                return false;
            }
            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    param += "&displaySubReportList=" + selectedSubReports.options[i].value;
                }
            }
        }
        try {
            param = checkAndAppendReportTitle(param, true);
        } catch (e) {
            return;
        }
        param = appendPreviousReportConfig(param);
        displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
        callReportsAjaxRequest("/SummaryReportHandler.do", param, 'show_report_page'); //No I18N
    }
}

function runAuditHistoryReport(reportId) {
    parent.closeDialog();
    if (reportId == null) {
        var historyReports = document.getElementsByName('historyReport');
        var len = historyReports.length;
        var flag = true;
        var historyreport = null;
        for (var i = 0; i < len; i++) {
            if (historyReports[i].checked) {
                historyreport = historyReports[i].value;
                flag = false;
                break;
            }
        }
        if (flag) {
            sdpodalert({message: "Please select audit history report"});//No I18N
            return false;
        }
        var param = "module=show_report_page&filterEnabled=true&historyReport=" + historyreport;//No I18N

        if (document.reportfilter != undefined) {
            param += constructParameters(document.reportfilter);
        }
        try {
            param = checkAndAppendReportTitle(param, true);
        } catch (e) {
            return;
        }

        param = appendPreviousReportConfig(param);
        displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
        callReportsAjaxRequest("/AuditHistoryReport.do", param, 'show_report_page'); //No I18N
    }
    else {
        var param = "module=generate_report&report_id=" + reportId;
        displayFadeMsg(getMessageForKey("sdp.report.common.generating.message"));
        callReportsAjaxRequest("/AuditHistoryReport.do", param, 'show_report_page'); //No I18N
    }
}

function editAuditReport(reportId, isfreshReport) {
    var param = "module=edit_report";
    if (reportId != null) {
        param += "&report_id=" + reportId;
    }
    if (isfreshReport == "true") {
        param = appendPreviousReportConfig(param);
    }
    displayFadeMsg();
    callReportsAjaxRequest("/AuditHistoryReport.do", param, 'show_audit_main_page'); //No I18N

}

function saveAuditReport(e) {
    sdpShowURLInPopup('AuditHistoryReport.do?module=save_report', 'closeButton=yes,position=absolute,width=580,resizable=true,left=' + (e.screenX - 370) + ',top=' + (e.screenY - 150));//No I18N
}

function scheduleAuditReport(reportId) {
    //document.location = "/ReportSchedule.do?mode=new&reportName=" + reportId;
    displayFadeMsg();
    params = "mode=new&report_id=" + reportId;
    callReportsAjaxRequest("/ReportSchedule.do", params, 'start_page'); //No I18N
}

function openScheduleWindow() {
    parent.closeDialog();
    displayFadeMsg();
    callReportsAjaxRequest("/ReportSchedule.do", "mode=new", 'start_page'); //No I18N
}
/*
 function scanchoice(toShow,toHide1,toHide2,toHide3,choice)
 {
 document.getElementById(toHide1).style.display ="none";
 document.getElementById(toHide2).style.display ="none";
 document.getElementById(toHide3).style.display ="none";
 //document.getElementById(toHide4).style.display ="none";
 document.getElementById(toShow).style.display ="block";

 document.getElementById("scanchoice1").className = "scanSchOFFchoice";
 document.getElementById("daily").className = "scanSchOFFchoice";
 document.getElementById("weekly").className = "scanSchOFFchoice";
 document.getElementById("monthly").className = "scanSchOFFchoice";
 document.getElementById(choice).className = "scanSchONchoice";
 }
 */
/*
 //Function to check/uncheck everyDay
 function checkEveryDay()
 {
 var selectedDays = document.getElementsByName('selectedDays');
 var toBeChecked = false;
 for(var j = 0; j < selectedDays.length; j++)
 {
 if (selectedDays[j].checked == true)
 {
 toBeChecked = true;
 }
 }
 document.getElementById('everyDay').checked = toBeChecked;
 }

 //For Every Month Groups Checkbox
 function checkMonths()
 {
 var everyMonth = document.getElementById('everyMonth');

 var selectedMonths = document.getElementsByName('selectedMonths');

 if(everyMonth.checked)
 {
 for(var j = 0; j < selectedMonths.length; j++)
 {
 selectedMonths[j].checked = true;
 }
 }
 else
 {
 for(var j = 0; j < selectedMonths.length; j++)
 {
 selectedMonths[j].checked = false;
 }
 }
 }

 //For EveryDay Groups Checkbox
 function checkDays()
 {
 var everyDay = document.getElementById("everyDay");
 var selectedDays = document.getElementsByName("selectedDays");

 if(everyDay.checked)
 {
 for(var j = 0; j < selectedDays.length; j++)
 {
 selectedDays[j].checked = true;
 }
 }
 else
 {
 for(var j = 0; j < selectedDays.length; j++)
 {
 selectedDays[j].checked = false;
 }
 }
 }

 //Function to check/uncheck every month
 function checkEveryMonth()
 {
 var selectedMonths = document.getElementsByName("selectedMonths");

 var toBeChecked = false;
 for(var j = 0; j < selectedMonths.length; j++)
 {
 if (selectedMonths[j].checked == true)
 {
 toBeChecked = true;
 }
 }
 document.getElementById('everyMonth').checked = toBeChecked;
 }
 */
function validateReportSchedule() {
    if (!validateScheduleDetails()) {
        return false;
    }
    if (!isValidEmailId(document.getElementById('toEmailSearch'))) {
        return false;
    }
    if (document.getElementById('reportName').value == "-1") {
        //alert( "sdp.reports.customreport.selectreporttoschedule" );
        sdpodalert({
            message: document.getElementById("selectreporttoschedule").innerHTML, action: function () {
                jQuery("#reportName").focus();
            }
        });
        return false;
    }
    var param = "";

    //for setting the mode dynamically..
    if (document.getElementById("mode111") != undefined && document.getElementById("mode111").value != null) {
        param += "mode=" + document.getElementById("mode111").value;
        if (document.getElementById("instance_id11") != undefined && document.getElementById("instance_id11").value != null) {
            param += "&instance_id=" + document.getElementById("instance_id11").value;
        }
    }
    else {
        param += "mode=save";
    }

    param += constructParameters(document.schedule_details);
    sdpShowIndicator();
    callReportsAjaxRequest("/ReportSchedule.do", param, 'save_schedule'); //No I18N
}

function getLongDate(dtFormat, hh, mm) {
    var tmp = dtFormat.split("-");
    var dt = new Date();
    /*
     dt.setYear(tmp[2]);
     dt.setMonth(tmp[1] - 1);
     dt.setDate(tmp[0]);
     */
    dt.setYear(tmp[0]);
    dt.setMonth(tmp[1] - 1);
    dt.setDate(tmp[2]);

    dt.setHours(hh);
    dt.setMinutes(mm);
    dt.setSeconds(0);
    return dt.getTime();
}


function showScheduledReportListView() {
    changeReportsTab('scheduleReports');//No I18N
}
function plusMinus(tdTagid) {
    var tdobj = document.getElementById(tdTagid);
    if (tdobj.style.display == 'none') {
        document.getElementById(tdTagid + "src").className = "show-plus";
        jQuery('#' + tdTagid).parents('.rep-step').removeClass('boxshadow'); //No I18N
    }
    else {
        document.getElementById(tdTagid + "src").className = "hide-minus";
        jQuery('#' + tdTagid).parents('.rep-step').addClass('boxshadow'); //No I18N

    }
}
function showTooltip(referenceId) {
    boxNo = "0";
    closeTooltip();
    //var nodeId = referenceId;
    var displayText;
    var controls;
    var contentdivid = "content_" + referenceId;

    displayText = '<div class="showtootip-div">' + document.getElementById(contentdivid).innerHTML + '&nbsp;</div>';
    controls = "position=relative,top=20,left=90,closeButton=no,srcElement=tooltip" + referenceId;	//No I18N
    controls = controls + ",closeOnBodyClick=yes,closePrevious=false";	//No I18N
    showDialog(displayText, controls);
    boxNo = "0";
}
function openFolder(folderId) {
    if (folderId != undefined && folderId != null) {
        var params = "module=change_view&filterView=all_reports&folder_id=" + folderId;
        displayFadeMsg();
        callReportsAjaxRequest("/CustomReportHandler.do", params, 'input_page'); //No I18N
    }
    else {

        callReportsAjaxRequest("/CustomReportHandler.do", "module=change_view&filterView=all_reports", 'input_page'); //No I18N
    }
}

var multipleEmailRexExp = /([A-Za-z0-9._%+\-]+@[A-Za-z0-9.\-]+\.[a-zA-Z]{2,22})+(\s?[,|;]\s?([A-Za-z0-9._%+\-]+@[A-Za-z0-9.\-]+\.[a-zA-Z]{2,22})+)*\s?[,;]?$/;

function validateEMailIDs(varEMail) {
    email = trimAll(varEMail.value);
    if (email == "" || email == null) {
        sdpodalert({message: document.getElementById('enteremail').innerHTML}); //No I18N
        varEMail.value = email;
        varEMail.focus();
        return false;
    }
    if (email.indexOf(";") > 0) {
        email = email.replace(/\;/g, ",");
    }
    email = jQuery.trim(email);
    if (!multipleEmailRexExp.test(email)) {

        sdpodalert({title: getMessageForKey('alert.title'), message: document.getElementById('enteremail').innerHTML}); //No I18N
        varEMail.value = email;
        varEMail.focus();
        return false;
    } else {
        varEMail.value = email;
        return true;
    }

}

var leading = /^\s*/g;
var trailing = /\s*$/g;

function emailCheckDuplicate(email) {
    var str = email;
    if (str == "") {
        sdpodalert({message: "Please enter proper E-Mail ID : " + email});//No I18N
        return false;
    }

    leadingremoved = str.replace(leading, "");
    str = leadingremoved.replace(trailing, "");

    if (str.length > 0) {
        var posadr1 = 0;
        var posdot = str.indexOf(".");
        var posadr = str.indexOf("@");
        posadr1 = str.lastIndexOf("@");
        if ((posdot < 0) || (posadr < 0) || (posadr1 != posadr)) {
            sdpodalert({message: "Please enter proper E-Mail ID : " + email});//No I18N
            return false;
        }
    }
    var j = str.length;
    var strobj = new String(str);
    if (strobj.charAt(j - 1) == "." || strobj.charAt(0) == "@" || strobj.charAt(j - 1) == "@" || strobj.charAt(0) == "." || strobj.charAt(0) == "-" || strobj.charAt(0) == "_" || strobj.charAt(j - 1) == "-" || strobj.charAt(j - 1) == "_" || strobj.charAt(j - 2) == "." || strobj.charAt(j - 2) == "-" || strobj.charAt(j - 2) == "_") {
        sdpodalert({message: "Please enter proper E-Mail ID : " + email});//No I18N
        return false;
    }
    return true;
}

var calDialog;
function closeCalDialog(callBackFunc) {
    if (calDialog != null && calDialog.style.visibility != "hidden") {
        calDialog.style.visibility = "hidden";
        if (document.getElementById("CalFreezeLayer") != null) document.body.removeChild(document.getElementById("CalFreezeLayer"));
        if (browser_ie && !browser_opera) {
            //document.body.removeChild(califrameIEHack);
            califrameIEHack = null;
        }
        calCloseOnBodyClick = false;

        if (typeof callBackFunc != "undefined")    callBackFunc();
    }
    if (document.getElementById('_CUSTOMALERTFRAME') != null) {
        document.getElementById("_CUSTOMALERTFRAME").src = '/framework/html/blank.html';
    }
}
function closeTooltip() {
    var divElement = document.getElementById("__DIALOG_CONTENT1");
    if (divElement != null) {
        closeDialog(null, divElement);
    }
}

function checkAndAppendReportTitle(params, showerror) {
    if (jQuery('#reportTitle').size() > 0) {
        var reportTitle = jQuery('#reportTitle').val();
        if (reportTitle === '') {
            if (showerror) {
                sdpodalert({
                    message: jQuery('#reporttitlerequest').html(), action: function () {
                        jQuery('#reportTitle').focus();
                    }
                });
            }
            throw new Error(jQuery('#reporttitlerequest').html());
        }
        params = appendParameter(params, "reportTitle", encodeURIComponent(reportTitle)); //No I18N
        /*
         if(parent.previousConfig){
         parent.previousConfig.reportTitle = reportTitle;//No I18N
         }
         */
    }
    return params;
}
function appendPreviousReportConfig(params) {
    if (parent['previousConfig']) {
        if (params) {
            params += "&previousConfig=" + encodeURIComponent(Object.toJSON ? Object.toJSON(parent['previousConfig']) : JSON.stringify(parent['previousConfig']));	//No I18N
        } else {
            params = "&previousConfig=" + encodeURIComponent(Object.toJSON ? Object.toJSON(parent['previousConfig']) : JSON.stringify(['previousConfig']));	//No I18N
        }
    }
    return params;
}
function exportReport(reportType, reportId) {
    var url = '/CustomReportHandler.do?module=export&file_type=' + reportType; //No I18N
    var repid = jQuery("#rep_id").val(); //No I18N
    if (repid && repid != "" && repid != "null" && repid != "-1") {
        url += '&report_id=' + repid;	//No I18N
    }
    window.location.href = url;
}

function handleFoldersInSaveForm(isprivate) {
    var selectid = 'folderId';  // No I18N
    var othersoptgroup = jQuery('#' + selectid + ' optgroup[key*=OTHERS_FOLDERS]'); // No I18N
    if (isprivate === true) {
        var isPrivateSelected = false;
        othersoptgroup.children().each(function (index, element) { // No I18N
            if (jQuery(element).prop('selected') === true && !isPrivateSelected) { // No I18N
                isPrivateSelected = true;
            }
            jQuery(element).attr('disabled', 'disabled').removeAttr('selected').hide(); // No I18N
        });
        othersoptgroup.attr('disabled', 'disabled').hide(); // No I18N
        if (isPrivateSelected) {
            var value = jQuery(jQuery('#' + selectid + ' option[disabled!=disabled]')[0]).val(); // No I18N
            jQuery('#' + selectid).val(value).trigger('change'); // No I18N
        }
    } else {
        othersoptgroup.show().children().removeAttr('disabled').show(); // No I18N
    }
}