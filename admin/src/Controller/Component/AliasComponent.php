<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 12/09/2016
 * Time: 20:07
 */
class AliasComponent extends Component
{

    public function generate($alias, $model, $field = 'alias')
    {

        $Model = TableRegistry::get($model);
        $alias = strtolower(Inflector::slug($alias));
        $count = $Model->find('all')->where([$field => $alias])->count();
        if ($count) $alias = $alias . '-' . ($count + 1);

        return $alias;
    }

}