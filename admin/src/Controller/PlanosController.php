<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Planos Controller
 *
 * @property \App\Model\Table\PlanosTable $Planos
 */
class PlanosController extends AppController
{

public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        
          $planos = $this->Planos->find(
            'search' , $this->Planos->filterParams($this->request->data)
            );

            
        $this->set('planos' , $this->paginate($planos ));
        $this->set('_serialize', ['planos']);
    }

    /**
     * View method
     *
     * @param string|null $id Plano id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $plano = $this->Planos->get($id, [
            'contain' => ['Clientes']
        ]);

        $this->set('plano', $plano);
        $this->set('_serialize', ['plano']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $plano = $this->Planos->newEntity();
        if ($this->request->is('post')) {
            $plano = $this->Planos->patchEntity($plano, $this->request->data);
            if ($this->Planos->save($plano)) {
                $this->Flash->success(__('The plano has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The plano could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('plano'));
        $this->set('_serialize', ['plano']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Plano id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $plano = $this->Planos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $plano = $this->Planos->patchEntity($plano, $this->request->data);
            if ($this->Planos->save($plano)) {
                $this->Flash->success(__('The plano has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The plano could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('plano'));
        $this->set('_serialize', ['plano']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Plano id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $plano = $this->Planos->get($id);
        if ($this->Planos->delete($plano)) {
            $this->Flash->success(__('The plano has been deleted.'));
        } else {
            $this->Flash->error(__('The plano could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Plano id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $plano = $this->Planos->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($dialogue->active){
                 $this->request->data('active' , 0);
            }else{
                $this->request->data('active' , 1);
            }
            $plano = $this->Planos->patchEntity($plano, $this->request->data);
            if ($this->Planos->save($plano)) {
                $this->Flash->success(__('The plano has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The plano could not be saved. Please, try again.'));
            }
        }
    }
}
