<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Inflector;

/**
 * Paginas Controller
 *
 * @property \App\Model\Table\PaginasTable $Paginas
 */
class PaginasController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);

        $this->loadComponent('Alias');
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $paginas = $this->Paginas->find(
            'search', $this->Paginas->filterParams($this->request->data)
        );


        $this->set('paginas', $this->paginate($paginas));
        $this->set('_serialize', ['paginas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pagina = $this->Paginas->get($id, [
            'contain' => []
        ]);

        $this->set('pagina', $pagina);
        $this->set('_serialize', ['pagina']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pagina = $this->Paginas->newEntity();
        if ($this->request->is('post')) {
            $this->request->data('alias', $this->Alias->generate($this->request->data('nome'), 'Paginas'));
            $pagina = $this->Paginas->patchEntity($pagina, $this->request->data);
            if ($this->Paginas->save($pagina)) {
                $this->Flash->success(__('The pagina has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pagina could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pagina'));
        $this->set('_serialize', ['pagina']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pagina = $this->Paginas->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data('alias', strtolower(Inflector::slug($this->request->data('nome'))));
            $pagina = $this->Paginas->patchEntity($pagina, $this->request->data);
            if ($this->Paginas->save($pagina)) {
                $this->Flash->success(__('The pagina has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pagina could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pagina'));
        $this->set('_serialize', ['pagina']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pagina = $this->Paginas->get($id);
        if ($this->Paginas->delete($pagina)) {
            $this->Flash->success(__('The pagina has been deleted.'));
        } else {
            $this->Flash->error(__('The pagina could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $pagina = $this->Paginas->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($dialogue->active) {
                $this->request->data('active', 0);
            } else {
                $this->request->data('active', 1);
            }
            $pagina = $this->Paginas->patchEntity($pagina, $this->request->data);
            if ($this->Paginas->save($pagina)) {
                $this->Flash->success(__('The pagina has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The pagina could not be saved. Please, try again.'));
            }
        }
    }
}
