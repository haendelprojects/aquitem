<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rodadas Controller
 *
 * @property \App\Model\Table\RodadasTable $Rodadas
 */
class RodadasController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $rodadas = $this->Rodadas->find(
            'search', $this->Rodadas->filterParams($this->request->data)
        )->contain(
            ['Campeonatos']
        );


        $this->set('rodadas', $this->paginate($rodadas));
        $this->set('_serialize', ['rodadas']);
    }

    /**
     * View method
     *
     * @param string|null $id Rodada id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rodada = $this->Rodadas->get($id, [
            'contain' => ['Campeonatos', 'Jogos']
        ]);

        $this->set('rodada', $rodada);
        $this->set('_serialize', ['rodada']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id_campeonato)
    {
        $rodada = $this->Rodadas->newEntity();
        if ($this->request->is('post')) {
            $rodada = $this->Rodadas->patchEntity($rodada, $this->request->data, [
                'associated' => ['Jogos']
            ]);
            if ($this->Rodadas->save($rodada)) {
                $this->Flash->success(__('The rodada has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rodada could not be saved. Please, try again.'));
            }

        }
        $campeonatos = $this->Rodadas->Campeonatos->find('list', ['limit' => 200]);
        $times = $this->Rodadas->Jogos->Times->find('list', ['limit' => 200]);
        $visitantes = $this->Rodadas->Jogos->Visitantes->find('list', ['limit' => 200]);
        $this->set(compact('rodada', 'campeonatos', 'id_campeonato', 'times'));
        $this->set('_serialize', ['rodada']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rodada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rodada = $this->Rodadas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rodada = $this->Rodadas->patchEntity($rodada, $this->request->data);
            if ($this->Rodadas->save($rodada)) {
                $this->Flash->success(__('The rodada has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rodada could not be saved. Please, try again.'));
            }
        }
        $campeonatos = $this->Rodadas->Campeonatos->find('list', ['limit' => 200]);
        $this->set(compact('rodada', 'campeonatos'));
        $this->set('_serialize', ['rodada']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rodada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rodada = $this->Rodadas->get($id);
        if ($this->Rodadas->delete($rodada)) {
            $this->Flash->success(__('The rodada has been deleted.'));
        } else {
            $this->Flash->error(__('The rodada could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Rodada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $rodada = $this->Rodadas->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($dialogue->active) {
                $this->request->data('active', 0);
            } else {
                $this->request->data('active', 1);
            }
            $rodada = $this->Rodadas->patchEntity($rodada, $this->request->data);
            if ($this->Rodadas->save($rodada)) {
                $this->Flash->success(__('The rodada has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The rodada could not be saved. Please, try again.'));
            }
        }
    }
}
