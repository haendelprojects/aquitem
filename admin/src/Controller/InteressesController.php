<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Interesses Controller
 *
 * @property \App\Model\Table\InteressesTable $Interesses
 */
class InteressesController extends AppController
{

public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        
          $interesses = $this->Interesses->find(
            'search' , $this->Interesses->filterParams($this->request->data)
            );

            
        $this->set('interesses' , $this->paginate($interesses ));
        $this->set('_serialize', ['interesses']);
    }

    /**
     * View method
     *
     * @param string|null $id Interess id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $interess = $this->Interesses->get($id, [
            'contain' => []
        ]);

        $this->set('interess', $interess);
        $this->set('_serialize', ['interess']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $interess = $this->Interesses->newEntity();
        if ($this->request->is('post')) {
            $interess = $this->Interesses->patchEntity($interess, $this->request->data);
            if ($this->Interesses->save($interess)) {
                $this->Flash->success(__('The interess has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The interess could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('interess'));
        $this->set('_serialize', ['interess']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Interess id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $interess = $this->Interesses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $interess = $this->Interesses->patchEntity($interess, $this->request->data);
            if ($this->Interesses->save($interess)) {
                $this->Flash->success(__('The interess has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The interess could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('interess'));
        $this->set('_serialize', ['interess']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Interess id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $interess = $this->Interesses->get($id);
        if ($this->Interesses->delete($interess)) {
            $this->Flash->success(__('The interess has been deleted.'));
        } else {
            $this->Flash->error(__('The interess could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Interess id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $interess = $this->Interesses->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($dialogue->active){
                 $this->request->data('active' , 0);
            }else{
                $this->request->data('active' , 1);
            }
            $interess = $this->Interesses->patchEntity($interess, $this->request->data);
            if ($this->Interesses->save($interess)) {
                $this->Flash->success(__('The interess has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The interess could not be saved. Please, try again.'));
            }
        }
    }
}
