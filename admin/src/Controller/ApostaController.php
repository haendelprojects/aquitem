<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Aposta Controller
 *
 * @property \App\Model\Table\ApostaTable $Aposta
 */
class ApostaController extends AppController
{

public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        
        $aposta = $this->Aposta->find(
            'search' , $this->Aposta->filterParams($this->request->data)
            )->contain(
                ['Jogos', 'Users']
            );

        
        $this->set('aposta' , $this->paginate($aposta ));
        $this->set('_serialize', ['aposta']);
    }

    /**
     * View method
     *
     * @param string|null $id Apostum id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $apostum = $this->Aposta->get($id, [
            'contain' => ['Jogos', 'Users']
        ]);

        $this->set('apostum', $apostum);
        $this->set('_serialize', ['apostum']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $apostum = $this->Aposta->newEntity();
        if ($this->request->is('post')) {
            $apostum = $this->Aposta->patchEntity($apostum, $this->request->data);
            if ($this->Aposta->save($apostum)) {
                $this->Flash->success(__('The apostum has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The apostum could not be saved. Please, try again.'));
            }
        }
        $jogos = $this->Aposta->Jogos->find('list', ['limit' => 200]);
        $users = $this->Aposta->Users->find('list', ['limit' => 200]);
        $this->set(compact('apostum', 'jogos', 'users'));
        $this->set('_serialize', ['apostum']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Apostum id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $apostum = $this->Aposta->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $apostum = $this->Aposta->patchEntity($apostum, $this->request->data);
            if ($this->Aposta->save($apostum)) {
                $this->Flash->success(__('The apostum has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The apostum could not be saved. Please, try again.'));
            }
        }
        $jogos = $this->Aposta->Jogos->find('list', ['limit' => 200]);
        $users = $this->Aposta->Users->find('list', ['limit' => 200]);
        $this->set(compact('apostum', 'jogos', 'users'));
        $this->set('_serialize', ['apostum']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Apostum id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $apostum = $this->Aposta->get($id);
        if ($this->Aposta->delete($apostum)) {
            $this->Flash->success(__('The apostum has been deleted.'));
        } else {
            $this->Flash->error(__('The apostum could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Apostum id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $apostum = $this->Aposta->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($dialogue->active){
                 $this->request->data('active' , 0);
            }else{
                $this->request->data('active' , 1);
            }
            $apostum = $this->Aposta->patchEntity($apostum, $this->request->data);
            if ($this->Aposta->save($apostum)) {
                $this->Flash->success(__('The apostum has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The apostum could not be saved. Please, try again.'));
            }
        }
    }
}
