<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Classificados Controller
 *
 * @property \App\Model\Table\ClassificadosTable $Classificados
 */
class ClassificadosController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $classificados = $this->Classificados->find(
            'search', $this->Classificados->filterParams($this->request->data)
        );


        $this->set('classificados', $this->paginate($classificados));
        $this->set('_serialize', ['classificados']);
    }

    /**
     * View method
     *
     * @param string|null $id Classificado id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classificado = $this->Classificados->get($id, [
            'contain' => ['Fotos']
        ]);

        $this->set('classificado', $classificado);
        $this->set('_serialize', ['classificado']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classificado = $this->Classificados->newEntity();
        if ($this->request->is('post')) {
            $this->request->data('criado_por', $this->Auth->user('id'));

            foreach ($this->request->data('files') as $image) {
                $this->request->data['fotos'][]['image'] = IMG_RESPOSITORY . $image;
            }

            $classificado = $this->Classificados->patchEntity($classificado, $this->request->data);

            if ($this->Classificados->save($classificado)) {
                $this->Flash->success(__('The classificado has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The classificado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('classificado'));
        $this->set('_serialize', ['classificado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Classificado id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classificado = $this->Classificados->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
              foreach ($this->request->data('files') as $image) {
                $this->request->data['fotos'][]['image'] = IMG_RESPOSITORY . $image;
            }
            $classificado = $this->Classificados->patchEntity($classificado, $this->request->data);
            if ($this->Classificados->save($classificado)) {
                $this->Flash->success(__('The classificado has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The classificado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('classificado'));
        $this->set('_serialize', ['classificado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Classificado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classificado = $this->Classificados->get($id);
        if ($this->Classificados->delete($classificado)) {
            $this->Flash->success(__('The classificado has been deleted.'));
        } else {
            $this->Flash->error(__('The classificado could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Classificado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classificado = $this->Classificados->get($id);
        if ($this->Classificados->delete($classificado)) {
            $this->Flash->success(__('The classificado has been deleted.'));
        } else {
            $this->Flash->error(__('The classificado could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
