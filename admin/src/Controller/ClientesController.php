<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Inflector;
use Cake\Utility\Text;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
            $this->loadComponent('Alias');
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $clientes = $this->Clientes->find(
            'search', $this->Clientes->filterParams($this->request->data)
        )->contain(
            ['Planos', 'Categorias']
        );


        $this->set('clientes', $this->paginate($clientes));
        $this->set('_serialize', ['clientes']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Planos', 'Categorias', 'Fotos', 'Telefones']
        ]);

        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $cliente = $this->Clientes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data('cadastrado_por', $this->Auth->user('id'));
         	$this->request->data('slug', $this->Alias->generate($this->request->data('nome') , 'Clientes' , 'slug'));
            $this->request->data('logo', IMG_RESPOSITORY . $this->request->data('logo.0'));
            $this->request->data('img_header', IMG_RESPOSITORY . $this->request->data('img_header.0'));

            foreach ($this->request->data['telefones'] as $key => $value) {
                if ($this->request->data['telefones'][$key]['numero'] == '') {
                    unset($this->request->data['telefones'][$key]);
                }
            }

            foreach ($this->request->data('files') as $image) {
                $this->request->data['fotos'][]['image'] = IMG_RESPOSITORY . $image;
            }
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('The cliente has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cliente could not be saved. Please, try again.'));
            }
        }
        $planos = $this->Clientes->Planos->find('list', ['limit' => 200]);
        $categorias = $this->Clientes->Categorias->find('list')->where(['Categorias.nome !=' => 'Indefinido'])->orderAsc('Categorias.nome');
        $this->set(compact('cliente', 'planos', 'categorias'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->request->data('logo.0') != '') {
                $this->request->data('logo', IMG_RESPOSITORY . $this->request->data('logo.0'));
            } else {
                unset($this->request->data['logo']);
            }
            if ($this->request->data('img_header.0') != '') {
                $this->request->data('img_header', IMG_RESPOSITORY . $this->request->data('img_header.0'));
            } else {
                unset($this->request->data['img_header']);
            }

            if ($this->request->data('files.0') != '') {
                foreach ($this->request->data('files') as $image) {
                    $this->request->data['fotos'][]['image'] = IMG_RESPOSITORY . $image;
                }
            } else {
                unset($this->request->data['files']);
            }


             foreach ($this->request->data['telefones'] as $key => $value) {
                if ($this->request->data['telefones'][$key]['numero'] == '') {
                    unset($this->request->data['telefones'][$key]);
                }
            }

            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);

            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('The cliente has been saved.'));
                return $this->redirect(['action' => 'view', $cliente->id]);
            } else {
                $this->Flash->error(__('The cliente could not be saved. Please, try again.'));
            }
        }
        $planos = $this->Clientes->Planos->find('list', ['limit' => 200]);
        $categorias = $this->Clientes->Categorias->find('list')->where(['Categorias.nome !=' => 'Indefinido'])->orderAsc('Categorias.nome');
        $this->set(compact('cliente', 'planos', 'categorias'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);
        if ($this->Clientes->delete($cliente)) {
            $this->Flash->success(__('The cliente has been deleted.'));
        } else {
            $this->Flash->error(__('The cliente could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $cliente = $this->Clientes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($cliente->ativo) {
                $this->request->data('ativo', 0);
            } else {
                $this->request->data('ativo', 1);
            }
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('The cliente has been saved.'));
                return $this->redirect($this->referer());
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The cliente could not be saved. Please, try again.'));
            }
        }
    }
}
