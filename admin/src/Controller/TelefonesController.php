<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Telefones Controller
 *
 * @property \App\Model\Table\TelefonesTable $Telefones
 */
class TelefonesController extends AppController
{

public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        
        $telefones = $this->Telefones->find(
            'search' , $this->Telefones->filterParams($this->request->data)
            )->contain(
                ['Clientes']
            );

        
        $this->set('telefones' , $this->paginate($telefones ));
        $this->set('_serialize', ['telefones']);
    }

    /**
     * View method
     *
     * @param string|null $id Telefone id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $telefone = $this->Telefones->get($id, [
            'contain' => ['Clientes']
        ]);

        $this->set('telefone', $telefone);
        $this->set('_serialize', ['telefone']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $telefone = $this->Telefones->newEntity();
        if ($this->request->is('post')) {
            $telefone = $this->Telefones->patchEntity($telefone, $this->request->data);
            if ($this->Telefones->save($telefone)) {
                $this->Flash->success(__('The telefone has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The telefone could not be saved. Please, try again.'));
            }
        }
        $clientes = $this->Telefones->Clientes->find('list', ['limit' => 200]);
        $this->set(compact('telefone', 'clientes'));
        $this->set('_serialize', ['telefone']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Telefone id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $telefone = $this->Telefones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $telefone = $this->Telefones->patchEntity($telefone, $this->request->data);
            if ($this->Telefones->save($telefone)) {
                $this->Flash->success(__('The telefone has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The telefone could not be saved. Please, try again.'));
            }
        }
        $clientes = $this->Telefones->Clientes->find('list', ['limit' => 200]);
        $this->set(compact('telefone', 'clientes'));
        $this->set('_serialize', ['telefone']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Telefone id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $telefone = $this->Telefones->get($id);
        if ($this->Telefones->delete($telefone)) {
            $this->Flash->success(__('The telefone has been deleted.'));
        } else {
            $this->Flash->error(__('The telefone could not be deleted. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }

    /**
     * Active method
     *
     * @param string|null $id Telefone id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $telefone = $this->Telefones->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($dialogue->active){
                 $this->request->data('active' , 0);
            }else{
                $this->request->data('active' , 1);
            }
            $telefone = $this->Telefones->patchEntity($telefone, $this->request->data);
            if ($this->Telefones->save($telefone)) {
                $this->Flash->success(__('The telefone has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The telefone could not be saved. Please, try again.'));
            }
        }
    }
}
