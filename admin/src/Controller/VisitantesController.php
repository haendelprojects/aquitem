<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Visitantes Controller
 *
 * @property \App\Model\Table\VisitantesTable $Visitantes
 */
class VisitantesController extends AppController
{

public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        
        $visitantes = $this->Visitantes->find(
            'search' , $this->Visitantes->filterParams($this->request->data)
            )->contain(
                ['Users']
            );

        
        $this->set('visitantes' , $this->paginate($visitantes ));
        $this->set('_serialize', ['visitantes']);
    }

    /**
     * View method
     *
     * @param string|null $id Visitante id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $visitante = $this->Visitantes->get($id, [
            'contain' => ['Users', 'Jogos']
        ]);

        $this->set('visitante', $visitante);
        $this->set('_serialize', ['visitante']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $visitante = $this->Visitantes->newEntity();
        if ($this->request->is('post')) {
            $visitante = $this->Visitantes->patchEntity($visitante, $this->request->data);
            if ($this->Visitantes->save($visitante)) {
                $this->Flash->success(__('The visitante has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The visitante could not be saved. Please, try again.'));
            }
        }
        $users = $this->Visitantes->Users->find('list', ['limit' => 200]);
        $this->set(compact('visitante', 'users'));
        $this->set('_serialize', ['visitante']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Visitante id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $visitante = $this->Visitantes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $visitante = $this->Visitantes->patchEntity($visitante, $this->request->data);
            if ($this->Visitantes->save($visitante)) {
                $this->Flash->success(__('The visitante has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The visitante could not be saved. Please, try again.'));
            }
        }
        $users = $this->Visitantes->Users->find('list', ['limit' => 200]);
        $this->set(compact('visitante', 'users'));
        $this->set('_serialize', ['visitante']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Visitante id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $visitante = $this->Visitantes->get($id);
        if ($this->Visitantes->delete($visitante)) {
            $this->Flash->success(__('The visitante has been deleted.'));
        } else {
            $this->Flash->error(__('The visitante could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Visitante id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $visitante = $this->Visitantes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($dialogue->active){
                 $this->request->data('active' , 0);
            }else{
                $this->request->data('active' , 1);
            }
            $visitante = $this->Visitantes->patchEntity($visitante, $this->request->data);
            if ($this->Visitantes->save($visitante)) {
                $this->Flash->success(__('The visitante has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The visitante could not be saved. Please, try again.'));
            }
        }
    }
}
