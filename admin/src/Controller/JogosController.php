<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Jogos Controller
 *
 * @property \App\Model\Table\JogosTable $Jogos
 */
class JogosController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $jogos = $this->Jogos->find(
            'search', $this->Jogos->filterParams($this->request->data)
        )->contain(
            ['Rodadas', 'Times', 'Visitantes']
        );


        $this->set('jogos', $this->paginate($jogos));
        $this->set('_serialize', ['jogos']);
    }

    /**
     * View method
     *
     * @param string|null $id Jogo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $jogo = $this->Jogos->get($id, [
            'contain' => ['Rodadas', 'Times', 'Visitantes', 'Aposta']
        ]);

        $this->set('jogo', $jogo);
        $this->set('_serialize', ['jogo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jogo = $this->Jogos->newEntity();
        if ($this->request->is('post')) {
            $jogo = $this->Jogos->patchEntity($jogo, $this->request->data);
            if ($this->Jogos->save($jogo)) {
                $this->Flash->success(__('The jogo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jogo could not be saved. Please, try again.'));
            }
        }
        $rodadas = $this->Jogos->Rodadas->find('list', ['limit' => 200]);
        $times = $this->Jogos->Times->find('list', ['limit' => 200])->orderDesc('name');

        $this->set(compact('jogo', 'rodadas', 'times'));
        $this->set('_serialize', ['jogo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jogo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $jogo = $this->Jogos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $jogo = $this->Jogos->patchEntity($jogo, $this->request->data);
            if ($this->Jogos->save($jogo)) {
                $this->Flash->success(__('The jogo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jogo could not be saved. Please, try again.'));
            }
        }
        $rodadas = $this->Jogos->Rodadas->find('list', ['limit' => 200]);
        $times = $this->Jogos->Times->find('list', ['limit' => 200]);
        $visitantes = $this->Jogos->Visitantes->find('list', ['limit' => 200]);
        $this->set(compact('jogo', 'rodadas', 'times', 'visitantes'));
        $this->set('_serialize', ['jogo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jogo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $jogo = $this->Jogos->get($id);
        if ($this->Jogos->delete($jogo)) {
            $this->Flash->success(__('The jogo has been deleted.'));
        } else {
            $this->Flash->error(__('The jogo could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Jogo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $jogo = $this->Jogos->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($dialogue->active) {
                $this->request->data('active', 0);
            } else {
                $this->request->data('active', 1);
            }
            $jogo = $this->Jogos->patchEntity($jogo, $this->request->data);
            if ($this->Jogos->save($jogo)) {
                $this->Flash->success(__('The jogo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The jogo could not be saved. Please, try again.'));
            }
        }
    }
}
