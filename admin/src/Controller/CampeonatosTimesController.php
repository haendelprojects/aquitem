<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CampeonatosTimes Controller
 *
 * @property \App\Model\Table\CampeonatosTimesTable $CampeonatosTimes
 */
class CampeonatosTimesController extends AppController
{

public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        
        $campeonatosTimes = $this->CampeonatosTimes->find(
            'search' , $this->CampeonatosTimes->filterParams($this->request->data)
            )->contain(
                ['Times', 'Campeonatos']
            );

        
        $this->set('campeonatosTimes' , $this->paginate($campeonatosTimes ));
        $this->set('_serialize', ['campeonatosTimes']);
    }

    /**
     * View method
     *
     * @param string|null $id Campeonatos Time id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $campeonatosTime = $this->CampeonatosTimes->get($id, [
            'contain' => ['Times', 'Campeonatos']
        ]);

        $this->set('campeonatosTime', $campeonatosTime);
        $this->set('_serialize', ['campeonatosTime']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $campeonatosTime = $this->CampeonatosTimes->newEntity();
        if ($this->request->is('post')) {
            $campeonatosTime = $this->CampeonatosTimes->patchEntity($campeonatosTime, $this->request->data);
            if ($this->CampeonatosTimes->save($campeonatosTime)) {
                $this->Flash->success(__('The campeonatos time has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The campeonatos time could not be saved. Please, try again.'));
            }
        }
        $times = $this->CampeonatosTimes->Times->find('list', ['limit' => 200]);
        $campeonatos = $this->CampeonatosTimes->Campeonatos->find('list', ['limit' => 200]);
        $this->set(compact('campeonatosTime', 'times', 'campeonatos'));
        $this->set('_serialize', ['campeonatosTime']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Campeonatos Time id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $campeonatosTime = $this->CampeonatosTimes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $campeonatosTime = $this->CampeonatosTimes->patchEntity($campeonatosTime, $this->request->data);
            if ($this->CampeonatosTimes->save($campeonatosTime)) {
                $this->Flash->success(__('The campeonatos time has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The campeonatos time could not be saved. Please, try again.'));
            }
        }
        $times = $this->CampeonatosTimes->Times->find('list', ['limit' => 200]);
        $campeonatos = $this->CampeonatosTimes->Campeonatos->find('list', ['limit' => 200]);
        $this->set(compact('campeonatosTime', 'times', 'campeonatos'));
        $this->set('_serialize', ['campeonatosTime']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Campeonatos Time id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $campeonatosTime = $this->CampeonatosTimes->get($id);
        if ($this->CampeonatosTimes->delete($campeonatosTime)) {
            $this->Flash->success(__('The campeonatos time has been deleted.'));
        } else {
            $this->Flash->error(__('The campeonatos time could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Active method
     *
     * @param string|null $id Campeonatos Time id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function active($id = null)
    {
        $campeonatosTime = $this->CampeonatosTimes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($dialogue->active){
                 $this->request->data('active' , 0);
            }else{
                $this->request->data('active' , 1);
            }
            $campeonatosTime = $this->CampeonatosTimes->patchEntity($campeonatosTime, $this->request->data);
            if ($this->CampeonatosTimes->save($campeonatosTime)) {
                $this->Flash->success(__('The campeonatos time has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The campeonatos time could not be saved. Please, try again.'));
            }
        }
    }
}
