<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Categorias helper
 */
class CategoriasHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    public function getList(){
        return [
            'Esportes' => 'Esportes',
            'Música' => 'Música',
            'Animais' => 'Animais',
            'Veículos' => 'Véiculos',
            'Imóveis' => 'Imóveis',
            'Moda' => 'Moda',
        ];
    }

}
