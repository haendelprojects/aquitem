<?php
namespace App\Model\Table;

use App\Model\Entity\Cliente;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Planos
 * @property \Cake\ORM\Association\BelongsTo $Categorias
 * @property \Cake\ORM\Association\HasMany $Fotos
 * @property \Cake\ORM\Association\HasMany $Telefones
 */
class ClientesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('clientes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Planos', [
            'foreignKey' => 'plano_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categorias', [
            'foreignKey' => 'categoria_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Fotos', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('Telefones', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->addBehavior('Search.Search');
        $this->_searchConfiguration();

    }

    private function _searchConfiguration()
    {

        $this->searchManager()
            ->value('id', [
                'field' => $this->aliasField('id')
            ])
            ->value('ativo', [
                'field' => $this->aliasField('ativo')
            ])
            ->like('nome', [
                'before' => true,
                'after' => true,
                'field' => [$this->aliasField('nome')]
            ])
            ->add('uf', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $args = explode(" ", $args['uf']);
                    // Modify $query as required

                    foreach ($args as $arg) {
                        $data[] =
                            ['Clientes.uf LIKE' => '%' . $arg . '%'];
                    }
                    $query->where($data);
                    return $query;
                }
            ])
            ->add('cidade', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $args = explode(" ", $args['cidade']);
                    // Modify $query as required

                    foreach ($args as $arg) {
                        $data[] =
                            ['Clientes.cidade LIKE' => '%' . $arg . '%'];
                    }
                    $query->where($data);
                    return $query;
                }
            ])
            ->add('categoria', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $args = explode(" ", $args['categoria']);
                    // Modify $query as required

                    foreach ($args as $arg) {
                        $data[] =
                            ['Categorias.nome LIKE' => '%' . $arg . '%'];
                    }
                    $query->where($data);
                    return $query;
                }
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('slogan');

        $validator
            ->allowEmpty('subdescricao');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('site');

        $validator
            ->allowEmpty('logradouro');

        $validator
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('bairro');

        $validator
            ->allowEmpty('uf');

        $validator
            ->allowEmpty('cidade');

        $validator
            ->allowEmpty('cep');

        $validator
            ->allowEmpty('cartao_desconto');

        $validator
            ->date('data_cadastro')
            ->requirePresence('data_cadastro', 'create')
            ->notEmpty('data_cadastro');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->boolean('plus')
            ->allowEmpty('plus');

        $validator
            ->allowEmpty('logo');

        $validator
            ->allowEmpty('img_header');

        $validator
            ->allowEmpty('map_embed');

        $validator
            ->allowEmpty('facebook');

        $validator
            ->allowEmpty('wix');

        $validator
            ->integer('cadastrado_por')
            ->allowEmpty('cadastrado_por');

        $validator
            ->allowEmpty('slug');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['plano_id'], 'Planos'));
        $rules->add($rules->existsIn(['categoria_id'], 'Categorias'));
        return $rules;
    }
}
