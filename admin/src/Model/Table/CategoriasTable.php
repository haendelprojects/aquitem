<?php
namespace App\Model\Table;

use App\Model\Entity\Categoria;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categorias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categorias
 * @property \Cake\ORM\Association\HasMany $Anuncios
 * @property \Cake\ORM\Association\HasMany $CategoriasPai
 * @property \Cake\ORM\Association\HasMany $Clientes
 */
class CategoriasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('categorias');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('CategoriasPai', [
            'foreignKey' => 'categoria_id'
        ]);
        $this->hasMany('Anuncios', [
            'foreignKey' => 'categoria_id'
        ]);
        $this->hasMany('Categorias', [
            'foreignKey' => 'categoria_id'
        ]);
        $this->hasMany('Clientes', [
            'foreignKey' => 'categoria_id'
        ]);

        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('id', 'Search.Value', [
                'field' => $this->aliasField('id')
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->add('nome', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message'=>'Já cadastrado'])
            ->notEmpty('nome');

        $validator
            ->boolean('tipo')
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->allowEmpty('slug');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['categoria_id'], 'Categorias'));
        $rules->add($rules->isUnique(['nome']));
        return $rules;
    }
}
