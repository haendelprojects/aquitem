<?php
namespace App\Model\Table;

use App\Model\Entity\Apostum;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Aposta Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Jogos
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class ApostaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('aposta');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Jogos', [
            'foreignKey' => 'jogo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);

  $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('id', 'Search.Value', [
                'field' => $this->aliasField('id')
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('gol_casa')
            ->allowEmpty('gol_casa');

        $validator
            ->integer('visitante_casa')
            ->allowEmpty('visitante_casa');

        $validator
            ->integer('pontuacao')
            ->allowEmpty('pontuacao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['jogo_id'], 'Jogos'));
        $rules->add($rules->existsIn(['usuario_id'], 'Users'));
        return $rules;
    }
}
