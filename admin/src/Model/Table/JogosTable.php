<?php
namespace App\Model\Table;

use App\Model\Entity\Jogo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Jogos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Rodadas
 * @property \Cake\ORM\Association\BelongsTo $Times
 * @property \Cake\ORM\Association\BelongsTo $Visitantes
 * @property \Cake\ORM\Association\HasMany $Aposta
 */
class JogosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('jogos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Rodadas', [
            'foreignKey' => 'rodada_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Times', [
            'foreignKey' => 'casa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Visitantes', [
            'tableAlias' => 'Times',
            'foreignKey' => 'visitante_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Aposta', [
            'foreignKey' => 'jogo_id'
        ]);

        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('id', 'Search.Value', [
                'field' => $this->aliasField('id')
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('gol_casa')
            ->allowEmpty('gol_casa');

        $validator
            ->integer('gol_visitante')
            ->allowEmpty('gol_visitante');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rodada_id'], 'Rodadas'));

        return $rules;
    }
}
