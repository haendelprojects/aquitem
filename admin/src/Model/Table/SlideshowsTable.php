<?php
namespace App\Model\Table;

use App\Model\Entity\Slideshow;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Slideshows Model
 *
 */
class SlideshowsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('slideshows');
        $this->displayField('id');
        $this->primaryKey('id');

  $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('id', 'Search.Value', [
                'field' => $this->aliasField('id')
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('file');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('link');

        $validator
            ->allowEmpty('ativo');

        $validator
            ->integer('posicao')
            ->allowEmpty('posicao');

        return $validator;
    }
}
