<?php
namespace App\Model\Table;

use App\Model\Entity\Campeonato;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Campeonatos Model
 *
 * @property \Cake\ORM\Association\HasMany $Rodadas
 * @property \Cake\ORM\Association\BelongsToMany $Times
 */
class CampeonatosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('campeonatos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Rodadas', [
            'foreignKey' => 'campeonato_id'
        ]);
        $this->belongsToMany('Times', [
            'foreignKey' => 'campeonato_id',
            'targetForeignKey' => 'time_id',
            'joinTable' => 'campeonatos_times'
        ]);

  $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('id', 'Search.Value', [
                'field' => $this->aliasField('id')
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('logo');

        $validator
            ->integer('ano')
            ->allowEmpty('ano');

        $validator
            ->allowEmpty('pais');

        return $validator;
    }
}
