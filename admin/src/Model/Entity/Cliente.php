<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cliente Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $slogan
 * @property string $subdescricao
 * @property string $descricao
 * @property \Cake\I18n\Time $modified
 * @property string $email
 * @property string $site
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $uf
 * @property string $cidade
 * @property string $cep
 * @property string $cartao_desconto
 * @property \Cake\I18n\Time $data_cadastro
 * @property bool $status
 * @property int $plano_id
 * @property \App\Model\Entity\Plano $plano
 * @property int $categoria_id
 * @property \App\Model\Entity\Categoria $categoria
 * @property bool $plus
 * @property string $logo
 * @property string $img_header
 * @property string $map_embed
 * @property string $facebook
 * @property string $wix
 * @property int $cadastrado_por
 * @property string $slug
 * @property \App\Model\Entity\Foto[] $fotos
 * @property \App\Model\Entity\Telefone[] $telefones
 */
class Cliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _setCep($value)
    {
        if ($value) {
            return preg_replace("/\D+/", "", $value);
        } else {
            return null;
        }

    }
}
