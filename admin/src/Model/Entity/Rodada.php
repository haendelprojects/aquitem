<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rodada Entity.
 *
 * @property int $id
 * @property int $numero
 * @property int $campeonato_id
 * @property \App\Model\Entity\Campeonato $campeonato
 * @property \Cake\I18n\Time $data_rodada
 * @property \Cake\I18n\Time $fechar_apostas_rodada
 * @property \App\Model\Entity\Jogo[] $jogos
 */
class Rodada extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
