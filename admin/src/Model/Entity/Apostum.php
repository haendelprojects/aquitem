<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Apostum Entity.
 *
 * @property int $id
 * @property int $gol_casa
 * @property int $visitante_casa
 * @property int $pontuacao
 * @property int $jogo_id
 * @property \App\Model\Entity\Jogo $jogo
 * @property int $usuario_id
 * @property \App\Model\Entity\User $user
 */
class Apostum extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
