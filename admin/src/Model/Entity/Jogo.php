<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Jogo Entity.
 *
 * @property int $id
 * @property int $rodada_id
 * @property \App\Model\Entity\Rodada $rodada
 * @property int $casa_id
 * @property \App\Model\Entity\Time $time
 * @property int $visitante_id
 * @property \App\Model\Entity\Visitante $visitante
 * @property int $gol_casa
 * @property int $gol_visitante
 * @property \App\Model\Entity\Apostum[] $aposta
 */
class Jogo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
