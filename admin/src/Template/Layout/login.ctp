<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Plano do Bem - Admin';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="FindUP Administrator">
    <meta name="author" content="KaijuThemes , FindUP">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600'
          rel='stylesheet'>


    <?= $this->Html->css('../fonts/font-awesome/css/font-awesome.min.css'); ?>
    <?= $this->Html->css('../fonts/themify-icons/themify-icons.css'); ?>
    <?= $this->Html->css('styles.css'); ?>
    <?= $this->Html->css('style.css'); ?>

    <?= $this->Html->css('../plugins/codeprettifier/prettify.css'); ?>
    <?= $this->Html->css('../plugins/iCheck/skins/minimal/blue.css'); ?>


    <?= $this->Html->css('../plugins/fullcalendar/fullcalendar.css'); ?>
    <?= $this->Html->css('../plugins/jvectormap/jquery-jvectormap-2.0.2.css'); ?>
    <?= $this->Html->css('../plugins/switchery/switchery.css'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body >

<?= $this->Flash->render() ?>

<?= $this->fetch('content') ?>


<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2016 Plano do Bem</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                class="ti ti-arrow-up"></i>
        </button>
    </div>
</footer>


<?= $this->Html->script('jquery-1.10.2.min.js'); ?>
<?= $this->Html->script('jqueryui-1.10.3.min.js'); ?>
<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('enquire.min.js'); ?>

<?= $this->Html->script('../plugins/velocityjs/velocity.min.js'); ?>
<!-- Load Velocity for Animated Content -->
<?= $this->Html->script('../plugins/velocityjs/velocity.ui.min.js'); ?>

<?= $this->Html->script('../plugins/wijets/wijets.js'); ?>
<!-- Wijet -->

<?= $this->Html->script('../plugins/codeprettifier/prettify.js'); ?>
<!-- Code Prettifier  -->
<?= $this->Html->script('../plugins/bootstrap-switch/bootstrap-switch.js'); ?>
<!-- Swith/Toggle Button -->

<?= $this->Html->script('../plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js'); ?>

<!-- End loading site level scripts -->


<!-- Load page level scripts-->


</body>
</html>
