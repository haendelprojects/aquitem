<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Aqui Tem PE - Admin';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="FindUP Administrator">
    <meta name="author" content="KaijuThemes , FindUP">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600'
          rel='stylesheet'>


    <?= $this->Html->css('../fonts/font-awesome/css/font-awesome.min.css'); ?>
    <?= $this->Html->css('../fonts/themify-icons/themify-icons.css'); ?>
    <?= $this->Html->css('styles.css'); ?>
    <?= $this->Html->css('style.css'); ?>

    <?= $this->Html->css('../plugins/codeprettifier/prettify.css'); ?>
    <?= $this->Html->css('../plugins/iCheck/skins/minimal/blue.css'); ?>


    <?= $this->Html->css('../plugins/fullcalendar/fullcalendar.css'); ?>
    <?= $this->Html->css('../plugins/jvectormap/jquery-jvectormap-2.0.2.css'); ?>
    <?= $this->Html->css('../plugins/switchery/switchery.css'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">


    <link href="/upload/css/jquery.filer.css" type="text/css" rel="stylesheet" />
    <link href="/upload/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<?= $this->element('Menu/navbar'); ?>
<div id="wrapper">
    <div id="layout-static">
        <?= $this->element('Menu/sidebar'); ?>
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    <?= $this->Flash->render() ?>

                    <?= $this->fetch('content') ?>


                    <footer role="contentinfo">
                        <div class="clearfix">
                            <ul class="list-unstyled list-inline pull-left">
                                <li><h6 style="margin: 0;">&copy; 2016 Plano do Bem</h6></li>
                            </ul>
                            <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                                    class="ti ti-arrow-up"></i>
                            </button>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->Html->script('jquery-1.10.2.min.js'); ?>
<?= $this->Html->script('jqueryui-1.10.3.min.js'); ?>
<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('enquire.min.js'); ?>

<?= $this->Html->script('../plugins/velocityjs/velocity.min.js'); ?>
<!-- Load Velocity for Animated Content -->
<?= $this->Html->script('../plugins/velocityjs/velocity.ui.min.js'); ?>

<?= $this->Html->script('../plugins/wijets/wijets.js'); ?>
<!-- Wijet -->

<?= $this->Html->script('../plugins/codeprettifier/prettify.js'); ?>
<!-- Code Prettifier  -->
<?= $this->Html->script('../plugins/bootstrap-switch/bootstrap-switch.js'); ?>
<!-- Swith/Toggle Button -->

<?= $this->Html->script('../plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js'); ?>
<!-- Bootstrap Tabdrop -->

<?= $this->Html->script('../plugins/iCheck/icheck.min.js'); ?>
<!-- iCheck -->

<?= $this->Html->script('../plugins/nanoScroller/js/jquery.nanoscroller.min.js'); ?>
<!-- nano scroller -->
<?= $this->Html->script('../plugins/form-inputmask/jquery.inputmask.bundle.min.js'); ?>



<?= $this->Html->script('../plugins/form-daterangepicker/moment.min.js'); ?>

<?= $this->Html->script('../plugins/form-colorpicker/js/bootstrap-colorpicker.min.js'); ?>

<?= $this->Html->script('../plugins/form-daterangepicker/daterangepicker.js'); ?>
<?= $this->Html->script('../plugins/bootstrap-datepicker/bootstrap-datepicker.js'); ?>
<?= $this->Html->script('../plugins/bootstrap-timepicker/bootstrap-timepicker.js'); ?>
<?= $this->Html->script('../plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'); ?>
<?= $this->Html->script('../plugins/clockface/js/clockface.js'); ?>

<?= $this->Html->script('ajax-loading.js'); ?>

<?= $this->Html->script('application.js'); ?>


<script src="/upload/js/jquery.filer.min.js"></script>


<?= $this->Html->script('../plugins/tinymce/js/tinymce/tinymce.min'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<script type="text/javascript">
    tinymce.init({
        selector: ".editor", theme: "modern", height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
        image_advtab: true,
        external_filemanager_path: "/filemanager/",
        filemanager_title: "Midias",
        filemanager_access_key: "dsflFWR9u2xQa",
        external_plugins: {"filemanager": "/filemanager/plugin.min.js"}
    });
</script>

<script>
     $(document).ready(function() {
        $('.filer_input').filer({
            changeInput: '<div class="jFiler-input-dragDrop" style="width:100%"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
            showThumbs: true,
            theme: "dragdropbox",
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item">\
                    <div class="jFiler-item-container">\
                        <div class="jFiler-item-inner">\
                            <div class="jFiler-item-thumb">\
                                <div class="jFiler-item-status"></div>\
                                <div class="jFiler-item-info">\
                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                </div>\
                                {{fi-image}}\
                            </div>\
                            <div class="jFiler-item-assets jFiler-row">\
                                <ul class="list-inline pull-left">\
                                    <li>{{fi-progressBar}}</li>\
                                </ul>\
                                <ul class="list-inline pull-right">\
                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </li>',
                itemAppend: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            },
            dragDrop: {
                dragEnter: null,
                dragLeave: null,
                drop: null,
            },
            uploadFile: {
                url: "/upload/php/upload.php",
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                beforeSend: function(){},
                success: function(data, el){
                    debugger;
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                        $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                error: function(el){
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                        $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                statusCode: null,
                onProgress: null,
                onComplete: null
            }
        });
    });
</script>

<!-- End loading site level scripts -->


<!-- Load page level scripts-->


</body>
</html>
