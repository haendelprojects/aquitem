<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Noticias'), ['controller' => 'noticias']) ?></li>
    <li><?= __('Editar'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Editar ' . ' Noticia') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($noticia) ?>
                    <fieldset>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('titulo') . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('texto', ['class' => 'editor', 'required' => false]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('data_cadastro') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('categoria', ['options' => ['Politica', 'Diversão', 'Saúde']]) . '</div>';
                        ?>
                    </fieldset>

                    <div>
                        <?= ($this->request->data('thumb') ? '<img style="width:200px" src=' . $this->request->data('thumb') . ' />' : '') ?>
                        <input type="file" name="thumb" class="filer_input" data-jfiler-limit="1">
                    </div>
                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
