<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Noticias'), ['controller' => 'noticias']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Noticia'), ['action' => 'edit', $noticia->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $noticia->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $noticia->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $noticia->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $noticia->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Noticia') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($noticia->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Titulo') ?></th>
                                <td><?= h($noticia->titulo) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Thumb') ?></th>
                                <td><?= h($noticia->thumb) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Slug') ?></th>
                                <td><?= h($noticia->slug) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($noticia->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Created By') ?></th>
                                <td><?= $this->Number->format($noticia->created_by) ?></td>
                            </tr>
                                                                                                                                            <tr>
                                <th><?= __('Data Cadastro') ?></th>
                                <td><?= h($noticia->data_cadastro) ?></td>
                            </tr>
                                                                                                                                            <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $noticia->status ? __('Yes') : __('No'); ?></td>
                            </tr>
                                                                                </table>

                    </div>
                </div>
            </div>


                                                <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Texto') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                            <?= $this->Text->autoParagraph(h($noticia->texto)); ?>
                        </div>
                </div>
            </div>

                                                                        
        </div>
    </div>
</div>
