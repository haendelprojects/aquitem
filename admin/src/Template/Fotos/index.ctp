
<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Fotos'), ['controller' => 'fotos']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Foto'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Fotos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                                                    <th>
                                        <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('image', ['label' => false, 'placeholder' => 'image', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('cliente_id', ['label' => false, 'placeholder' => 'cliente_id', 'class' => '']); ?> 
                                    </th>
                                 
                                <th> 
                                    <?php 
                                    echo  $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);  echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index' ] ); 
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>              
                            </tr>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('image') ?></th>
                                                                <th><?= $this->Paginator->sort('cliente_id') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($fotos as $foto): ?>
                            <tr>
                                                                <td><?= $this->Number->format($foto->id) ?></td>
                                                                <td><?= h($foto->image) ?></td>
                                                                <td><?= $foto->has('cliente') ? $this->Html->link($foto->cliente->id, ['controller' => 'Clientes', 'action' => 'view', $foto->cliente->id]) : '' ?></td>
                                                                <td class="actions">

                                    <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $foto->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                    <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $foto->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>

                                     <?php  $label_active =  ( $foto->ativo) ? __('Desativar') : __('Ativo'); ?>
                                      <?php  $icon_active =  ( $foto->ativo) ? 'close' : 'check'; ?>


                                    <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span><span class="sr-only">' . $label_active . '</span>', ['action' => 'active', $foto->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $foto->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>