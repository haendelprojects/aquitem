<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Fotos'), ['controller' => 'fotos']) ?></li>
    <li><?= __('Edit '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __( 'Edit ' . ' Foto') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($foto) ?>
                    <fieldset>
                        <legend><?= __('Edit Foto') ?></legend>
                        <?php
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('image') . '</div>';
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('cliente_id', ['options' => $clientes]) . '</div>';
                                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Save') , ['class'=>'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
