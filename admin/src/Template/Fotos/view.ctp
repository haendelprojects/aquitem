<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Fotos'), ['controller' => 'fotos']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Foto'), ['action' => 'edit', $foto->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $foto->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $foto->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $foto->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $foto->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Foto') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($foto->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Image') ?></th>
                                <td><?= h($foto->image) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Cliente') ?></th>
                                <td><?= $foto->has('cliente') ? $this->Html->link($foto->cliente->id, ['controller' => 'Clientes', 'action' => 'view', $foto->cliente->id]) : '' ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($foto->id) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    
        </div>
    </div>
</div>
