<div class="users form large-10 medium-10 columns content">

	<?= 'Seja Bem-vindo ' . $UserLogged['nome']; ?>




	<div class="row">
		<div class="large-6">
			<div class="callout primary">
			<table class="hover">
				<thead>
				<tr>
					<th >Tipo</th>
					<th>Quantidade</th>

				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Clientes</th>
					<td><?= $clientes_count; ?> </td>
				</tr>
				<tr>
					<th>Clientes Ativos</th>
					<td><?= $clientes_count_ativo; ?> </td>
				</tr>
				<tr>
					<th>Clientes Inativos</th>
					<td><?= $clientes_count_inativos; ?> </td>
				</tr>
				<tr>
					<th>Clientes Vencidos</th>
					<td><?= $clientes_count_vencidos; ?> </td>
				</tr>
				<tr>
					<th>Clientes Válidos</th>
					<td><?= $clientes_count_validos; ?> </td>
				</tr>
				<tr>
					<th>Categorias</th>
					<td><?= $categorias_count; ?> </td>
				</tr>
				</tbody>
			</table>
				</div>
		</div>
	</div>

</div>
