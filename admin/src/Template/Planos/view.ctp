<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Planos'), ['controller' => 'planos']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Plano'), ['action' => 'edit', $plano->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $plano->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $plano->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $plano->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $plano->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Plano') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($plano->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($plano->nome) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($plano->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Valor') ?></th>
                                <td><?= $this->Number->format($plano->valor) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Clientes') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Clientes') ?></h4>
                        <?php if (!empty($plano->clientes)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Nome') ?></th>
                                                                <th><?= __('Slogan') ?></th>
                                                                <th><?= __('Subdescricao') ?></th>
                                                                <th><?= __('Descricao') ?></th>
                                                                <th><?= __('Modified') ?></th>
                                                                <th><?= __('Email') ?></th>
                                                                <th><?= __('Site') ?></th>
                                                                <th><?= __('Logradouro') ?></th>
                                                                <th><?= __('Numero') ?></th>
                                                                <th><?= __('Complemento') ?></th>
                                                                <th><?= __('Bairro') ?></th>
                                                                <th><?= __('Uf') ?></th>
                                                                <th><?= __('Cidade') ?></th>
                                                                <th><?= __('Cep') ?></th>
                                                                <th><?= __('Cartao Desconto') ?></th>
                                                                <th><?= __('Data Cadastro') ?></th>
                                                                <th><?= __('Status') ?></th>
                                                                <th><?= __('Plano Id') ?></th>
                                                                <th><?= __('Categoria Id') ?></th>
                                                                <th><?= __('Plus') ?></th>
                                                                <th><?= __('Logo') ?></th>
                                                                <th><?= __('Img Header') ?></th>
                                                                <th><?= __('Map Embed') ?></th>
                                                                <th><?= __('Facebook') ?></th>
                                                                <th><?= __('Wix') ?></th>
                                                                <th><?= __('Cadastrado Por') ?></th>
                                                                <th><?= __('Slug') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($plano->clientes as $clientes): ?>
                            <tr>
                                <td><?= h($clientes->id) ?></td>
                                <td><?= h($clientes->nome) ?></td>
                                <td><?= h($clientes->slogan) ?></td>
                                <td><?= h($clientes->subdescricao) ?></td>
                                <td><?= h($clientes->descricao) ?></td>
                                <td><?= h($clientes->modified) ?></td>
                                <td><?= h($clientes->email) ?></td>
                                <td><?= h($clientes->site) ?></td>
                                <td><?= h($clientes->logradouro) ?></td>
                                <td><?= h($clientes->numero) ?></td>
                                <td><?= h($clientes->complemento) ?></td>
                                <td><?= h($clientes->bairro) ?></td>
                                <td><?= h($clientes->uf) ?></td>
                                <td><?= h($clientes->cidade) ?></td>
                                <td><?= h($clientes->cep) ?></td>
                                <td><?= h($clientes->cartao_desconto) ?></td>
                                <td><?= h($clientes->data_cadastro) ?></td>
                                <td><?= h($clientes->status) ?></td>
                                <td><?= h($clientes->plano_id) ?></td>
                                <td><?= h($clientes->categoria_id) ?></td>
                                <td><?= h($clientes->plus) ?></td>
                                <td><?= h($clientes->logo) ?></td>
                                <td><?= h($clientes->img_header) ?></td>
                                <td><?= h($clientes->map_embed) ?></td>
                                <td><?= h($clientes->facebook) ?></td>
                                <td><?= h($clientes->wix) ?></td>
                                <td><?= h($clientes->cadastrado_por) ?></td>
                                <td><?= h($clientes->slug) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Clientes', 'action' => 'view', $clientes->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Clientes', 'action' => 'edit', $clientes->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Clientes', 'action' => 'delete', $clientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientes->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
