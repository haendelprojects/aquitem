<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Anuncios'), ['controller' => 'anuncios']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Anuncio'), ['action' => 'edit', $anuncio->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $anuncio->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $anuncio->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $anuncio->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $anuncio->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Anuncio') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($anuncio->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Titulo') ?></th>
                                <td><?= h($anuncio->titulo) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Link') ?></th>
                                <td><?= h($anuncio->link) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Img') ?></th>
                                <td><?= h($anuncio->img) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Categoria') ?></th>
                                <td><?= $anuncio->has('categoria') ? $this->Html->link($anuncio->categoria->id, ['controller' => 'Categorias', 'action' => 'view', $anuncio->categoria->id]) : '' ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Local') ?></th>
                                <td><?= h($anuncio->local) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($anuncio->id) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    
        </div>
    </div>
</div>
