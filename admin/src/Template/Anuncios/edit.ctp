<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Anuncios'), ['controller' => 'anuncios']) ?></li>
    <li><?= __('Editar '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Editar ' . ' Anuncio') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($anuncio) ?>
                    <fieldset>
                        <?php
                        echo '<div class="col-lg-6">' . $this->Form->input('titulo') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('link') . '</div>';
                        ?>

                        <div class="col-lg-12">
                            <?= ($this->request->data('img') ? '<img style="width:200px" src=' . $this->request->data('img') . ' />' : '') ?>
                            <input type="text" name="thumb" class="filer_input">
                        </div>

                        <?php
                        echo '<div class="col-lg-6">' . $this->Form->input('categoria_id', ['options' => $categorias]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('local' ,  ['options' => ['meio-conteudo' => 'Meio do Conteudo' , 'topo-site' => 'Topo do Site' , 'lado-site' => 'Lado do Site']]) . '</div>';
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
