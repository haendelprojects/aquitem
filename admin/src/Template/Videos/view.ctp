<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Videos'), ['controller' => 'videos']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Video'), ['action' => 'edit', $video->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $video->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $video->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $video->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $video->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Video') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($video->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Url') ?></th>
                                <td><?= h($video->url) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Titulo') ?></th>
                                <td><?= h($video->titulo) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($video->id) ?></td>
                            </tr>
                                                                                                                                                                        <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $video->status ? __('Yes') : __('No'); ?></td>
                            </tr>
                                                                                </table>

                    </div>
                </div>
            </div>


                                                <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Descricao') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                            <?= $this->Text->autoParagraph(h($video->descricao)); ?>
                        </div>
                </div>
            </div>

                                                                        
        </div>
    </div>
</div>
