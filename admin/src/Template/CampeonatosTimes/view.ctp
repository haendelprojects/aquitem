<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Campeonatos Times'), ['controller' => 'campeonatosTimes']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Campeonatos Time'), ['action' => 'edit', $campeonatosTime->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $campeonatosTime->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $campeonatosTime->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $campeonatosTime->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $campeonatosTime->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Campeonatos Time') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($campeonatosTime->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Time') ?></th>
                                <td><?= $campeonatosTime->has('time') ? $this->Html->link($campeonatosTime->time->id, ['controller' => 'Times', 'action' => 'view', $campeonatosTime->time->id]) : '' ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Campeonato') ?></th>
                                <td><?= $campeonatosTime->has('campeonato') ? $this->Html->link($campeonatosTime->campeonato->id, ['controller' => 'Campeonatos', 'action' => 'view', $campeonatosTime->campeonato->id]) : '' ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($campeonatosTime->id) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    
        </div>
    </div>
</div>
