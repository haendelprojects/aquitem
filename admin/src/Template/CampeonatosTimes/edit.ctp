<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Campeonatos Times'), ['controller' => 'campeonatosTimes']) ?></li>
    <li><?= __('Edit '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __( 'Edit ' . ' Campeonatos Time') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($campeonatosTime) ?>
                    <fieldset>
                        <legend><?= __('Edit Campeonatos Time') ?></legend>
                        <?php
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('time_id', ['options' => $times]) . '</div>';
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('campeonato_id', ['options' => $campeonatos]) . '</div>';
                                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Save') , ['class'=>'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
