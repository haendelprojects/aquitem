<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Campeonatos'), ['controller' => 'campeonatos']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Editar Campeonato'), ['action' => 'edit', $campeonato->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $campeonato->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $campeonato->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $campeonato->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $campeonato->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>

                <?= $this->Html->link(__('<span class="ti ti-plus"></span> ' . 'Nova Rodada'), ['controller'=> 'rodadas', 'action' => 'add', $campeonato->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Campeonato') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($campeonato->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($campeonato->nome) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($campeonato->descricao) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Logo') ?></th>
                                <td><?= h($campeonato->logo) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Pais') ?></th>
                                <td><?= h($campeonato->pais) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($campeonato->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Ano') ?></th>
                                <td><?= $this->Number->format($campeonato->ano) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Rodadas') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Rodadas') ?></h4>
                        <?php if (!empty($campeonato->rodadas)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Numero') ?></th>
                                                                <th><?= __('Campeonato Id') ?></th>
                                                                <th><?= __('Data Rodada') ?></th>
                                                                <th><?= __('Fechar Apostas Rodada') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($campeonato->rodadas as $rodadas): ?>
                            <tr>
                                <td><?= h($rodadas->id) ?></td>
                                <td><?= h($rodadas->numero) ?></td>
                                <td><?= h($rodadas->campeonato_id) ?></td>
                                <td><?= h($rodadas->data_rodada) ?></td>
                                <td><?= h($rodadas->fechar_apostas_rodada) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Rodadas', 'action' => 'view', $rodadas->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Rodadas', 'action' => 'edit', $rodadas->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Rodadas', 'action' => 'delete', $rodadas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rodadas->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Times') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Times') ?></h4>
                        <?php if (!empty($campeonato->times)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Nome') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($campeonato->times as $times): ?>
                            <tr>
                                <td><?= h($times->id) ?></td>
                                <td><?= h($times->nome) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Times', 'action' => 'view', $times->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Times', 'action' => 'edit', $times->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Times', 'action' => 'delete', $times->id], ['confirm' => __('Are you sure you want to delete # {0}?', $times->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
