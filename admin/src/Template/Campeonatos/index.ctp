
<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Campeonatos'), ['controller' => 'campeonatos']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Campeonato'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Campeonatos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                                                    <th>
                                        <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'nome', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('descricao', ['label' => false, 'placeholder' => 'descricao', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('logo', ['label' => false, 'placeholder' => 'logo', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('ano', ['label' => false, 'placeholder' => 'ano', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('pais', ['label' => false, 'placeholder' => 'pais', 'class' => '']); ?> 
                                    </th>
                                 
                                <th> 
                                    <?php 
                                    echo  $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);  echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index' ] ); 
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>              
                            </tr>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('nome') ?></th>
                                                                <th><?= $this->Paginator->sort('descricao') ?></th>
                                                                <th><?= $this->Paginator->sort('logo') ?></th>
                                                                <th><?= $this->Paginator->sort('ano') ?></th>
                                                                <th><?= $this->Paginator->sort('pais') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($campeonatos as $campeonato): ?>
                            <tr>
                                                                <td><?= $this->Number->format($campeonato->id) ?></td>
                                                                <td><?= h($campeonato->nome) ?></td>
                                                                <td><?= h($campeonato->descricao) ?></td>
                                                                <td><?= h($campeonato->logo) ?></td>
                                                                <td><?= $this->Number->format($campeonato->ano) ?></td>
                                                                <td><?= h($campeonato->pais) ?></td>
                                                                <td class="actions">

                                    <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $campeonato->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                    <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $campeonato->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>

                                     <?php  $label_active =  ( $campeonato->ativo) ? __('Desativar') : __('Ativo'); ?>
                                      <?php  $icon_active =  ( $campeonato->ativo) ? 'close' : 'check'; ?>


                                    <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span><span class="sr-only">' . $label_active . '</span>', ['action' => 'active', $campeonato->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $campeonato->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>