<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Paginas'), ['controller' => 'paginas']) ?></li>
    <li><?= __('Adicionar'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Adicionar Pagina') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($pagina) ?>
                    <fieldset>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('nome') . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('texto',['class' => 'editor']) . '</div>';
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
