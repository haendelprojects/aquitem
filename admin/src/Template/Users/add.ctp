<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Usuários'), ['controller' => 'users']) ?></li>
    <li><?= __('Editar'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Adicionar ' . ' Usuário') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($user) ?>
                    <fieldset>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('nome') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('username' , ['label'=>'Email']) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('password' , ['label'=>'Senha']) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('ativo', ['type' => 'checkbox']) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('type', ['options' => ['ADMIN' => 'ADMIN', 'VISITANTE' => 'VISITANTE'] , 'label'=>'Tipo']) . '</div>';
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
