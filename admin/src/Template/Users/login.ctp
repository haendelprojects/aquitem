<div class="container" id="login-form">

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <br><br>
            <br><br>
            <a href="index.html" class="login-logo"><?= $this->Html->image('logo.png'); ?></a>
            <br><br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Acesso Restrito</h2>
                </div>
                <form method="POST" class="form-horizontal" id="validate-form">
                <div class="panel-body">


                        <div class="form-group mb-md">
                            <div class="col-xs-12">
                                <div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-user"></i>
										</span>
                                    <input type="text" class="form-control" name="username" placeholder="Username"
                                           data-parsley-minlength="6" placeholder="At least 6 characters" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-md">
                            <div class="col-xs-12">
                                <div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-key"></i>
										</span>
                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                           placeholder="Password">
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-n">
                            <div class="col-xs-12">
                                <a href="extras-forgotpassword.html" class="pull-left">Esqueçeu a Senha</a>

                            </div>
                        </div>

                </div>
                <div class="panel-footer">
                    <div class="clearfix">
                        <button class="btn btn-default pull-left" type="submit">Entrar</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>