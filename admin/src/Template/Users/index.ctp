<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Usuários'), ['controller' => 'users']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Usuário'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Usuários') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                <th>
                                    <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'nome', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('username', ['label' => false, 'placeholder' => 'username', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('ativo', ['label' => false, 'placeholder' => 'ativo', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('type', ['label' => false, 'placeholder' => 'type', 'class' => '']); ?>
                                </th>

                                <th>
                                    <?php
                                    echo $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);
                                    echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index']);
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>
                            </tr>
                            <tr>
                                <th><?= $this->Paginator->sort('id') ?></th>
                                <th><?= $this->Paginator->sort('nome') ?></th>
                                <th><?= $this->Paginator->sort('username') ?></th>
                                <th><?= $this->Paginator->sort('ativo') ?></th>
                                <th><?= $this->Paginator->sort('type') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user): ?>
                                <tr>
                                    <td><?= $this->Number->format($user->id) ?></td>
                                    <td><?= h($user->nome) ?></td>
                                    <td><?= h($user->username) ?></td>
                                    <td><?= h($user->active) ?></td>
                                    <td><?= h($user->type) ?></td>
                                    <td class="actions">

                                        <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $user->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                        <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $user->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>
                                        <?= $this->Form->postLink('<span class="ti ti-trash"></span><span class="sr-only">Deletar</span>', ['action' => 'delete', $user->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $user->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>