<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Users'), ['controller' => 'users']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit User'), ['action' => 'edit', $user->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $user->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $user->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $user->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $user->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('User') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($user->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($user->nome) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Username') ?></th>
                                <td><?= h($user->username) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Password') ?></th>
                                <td><?= h($user->password) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Ativo') ?></th>
                                <td><?= h($user->ativo) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Type') ?></th>
                                <td><?= h($user->type) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($user->id) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Visitantes') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Visitantes') ?></h4>
                        <?php if (!empty($user->visitantes)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Nome') ?></th>
                                                                <th><?= __('Email') ?></th>
                                                                <th><?= __('Telefone') ?></th>
                                                                <th><?= __('User Id') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($user->visitantes as $visitantes): ?>
                            <tr>
                                <td><?= h($visitantes->id) ?></td>
                                <td><?= h($visitantes->nome) ?></td>
                                <td><?= h($visitantes->email) ?></td>
                                <td><?= h($visitantes->telefone) ?></td>
                                <td><?= h($visitantes->user_id) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Visitantes', 'action' => 'view', $visitantes->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Visitantes', 'action' => 'edit', $visitantes->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Visitantes', 'action' => 'delete', $visitantes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitantes->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
