<div class="users form large-10 medium-10 columns content">
	<?= $this->Form->create( $user ) ?>
	<fieldset>
		<legend><?= __( 'Meus Dados' ) ?></legend>
		<?php
		echo $this->Form->input( 'nome' );
		echo $this->Form->input( 'username' );
		echo $this->Form->input( 'password' );
		?>
	</fieldset>
	<?= $this->Form->button( __( 'Salvar' ), [ 'class' => 'button success' ] ) ?>
	<?= $this->Form->end() ?>
</div>
