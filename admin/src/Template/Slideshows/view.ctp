<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Slideshows'), ['controller' => 'slideshows']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Slideshow'), ['action' => 'edit', $slideshow->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $slideshow->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $slideshow->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $slideshow->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $slideshow->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Slideshow') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($slideshow->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('File') ?></th>
                                <td><?= h($slideshow->file) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Description') ?></th>
                                <td><?= h($slideshow->description) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Link') ?></th>
                                <td><?= h($slideshow->link) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Ativo') ?></th>
                                <td><?= h($slideshow->ativo) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($slideshow->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Posicao') ?></th>
                                <td><?= $this->Number->format($slideshow->posicao) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    
        </div>
    </div>
</div>
