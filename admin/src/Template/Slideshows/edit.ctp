<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Slideshows'), ['controller' => 'slideshows']) ?></li>
    <li><?= __('Editar '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Editar ' . ' Slideshow') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($slideshow) ?>
                    <fieldset>
                        <div class="col-lg-12">
                            <?= ($this->request->data('file') ? '<img style="width:200px" src=' . $this->request->data('file') . ' />' : '') ?>
                            <input type="file" name="files[]" class="filer_input">
                        </div>
                        <?php

                        echo '<div class="col-lg-6">' . $this->Form->input('description') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('link') . '</div>';
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
