<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Slideshows'), ['controller' => 'slideshows']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Slideshow'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Slideshows') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                <th>
                                    <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('file', ['label' => false, 'placeholder' => 'file', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('description', ['label' => false, 'placeholder' => 'description', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('link', ['label' => false, 'placeholder' => 'link', 'class' => '']); ?>
                                </th>


                                <th>
                                    <?php
                                    echo $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);
                                    echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index']);
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>
                            </tr>
                            <tr>
                                <th><?= $this->Paginator->sort('id') ?></th>
                                <th><?= $this->Paginator->sort('file') ?></th>
                                <th><?= $this->Paginator->sort('description') ?></th>
                                <th><?= $this->Paginator->sort('link') ?></th>

                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($slideshows as $slideshow): ?>
                                <tr>
                                    <td><?= $this->Number->format($slideshow->id) ?></td>
                                    <td><?= h($slideshow->file) ?></td>
                                    <td><?= h($slideshow->description) ?></td>
                                    <td><?= h($slideshow->link) ?></td>
                                    <td class="actions">

                                        <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $slideshow->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                        <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $slideshow->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>

                                        <?php $label_active = ($slideshow->ativo) ? __('Desativar') : __('Ativo'); ?>
                                        <?php $icon_active = ($slideshow->ativo) ? 'close' : 'check'; ?>


                                        <?= $this->Form->postLink('<span class="ti ti-' . $icon_active . '"></span><span class="sr-only">' . $label_active . '</span>', ['action' => 'active', $slideshow->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $slideshow->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>