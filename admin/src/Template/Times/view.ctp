<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Times'), ['controller' => 'times']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Time'), ['action' => 'edit', $time->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $time->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $time->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $time->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $time->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Time') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($time->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($time->nome) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($time->id) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Campeonatos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Campeonatos') ?></h4>
                        <?php if (!empty($time->campeonatos)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Nome') ?></th>
                                                                <th><?= __('Descricao') ?></th>
                                                                <th><?= __('Logo') ?></th>
                                                                <th><?= __('Ano') ?></th>
                                                                <th><?= __('Pais') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($time->campeonatos as $campeonatos): ?>
                            <tr>
                                <td><?= h($campeonatos->id) ?></td>
                                <td><?= h($campeonatos->nome) ?></td>
                                <td><?= h($campeonatos->descricao) ?></td>
                                <td><?= h($campeonatos->logo) ?></td>
                                <td><?= h($campeonatos->ano) ?></td>
                                <td><?= h($campeonatos->pais) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Campeonatos', 'action' => 'view', $campeonatos->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Campeonatos', 'action' => 'edit', $campeonatos->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Campeonatos', 'action' => 'delete', $campeonatos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $campeonatos->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
