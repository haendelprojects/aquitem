<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Telefones'), ['controller' => 'telefones']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Telefone'), ['action' => 'edit', $telefone->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $telefone->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $telefone->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $telefone->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $telefone->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Telefone') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($telefone->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= h($telefone->numero) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Cliente') ?></th>
                                <td><?= $telefone->has('cliente') ? $this->Html->link($telefone->cliente->id, ['controller' => 'Clientes', 'action' => 'view', $telefone->cliente->id]) : '' ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($telefone->id) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    
        </div>
    </div>
</div>
