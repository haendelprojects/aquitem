<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Jogos'), ['controller' => 'jogos']) ?></li>
    <li><?= __('Add '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Add ' . ' Jogo') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($jogo) ?>
                    <fieldset>
                        <legend><?= __('Add Jogo') ?></legend>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('rodada_id', ['options' => $rodadas]) . '</div>';
                        echo '<div class="col-lg-5">' . $this->Form->input('casa_id', ['options' => $times]) . '</div>';
                        ?>

                        <div class="col-lg-2 text-center"> <span class="btn btn-primary">X</span></div>
                        <?php
                        echo '<div class="col-lg-5">' . $this->Form->input('visitante_id', ['options' => $visitantes]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('gol_casa') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('gol_visitante') . '</div>';
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
