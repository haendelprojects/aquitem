<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Jogos'), ['controller' => 'jogos']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Jogo'), ['action' => 'edit', $jogo->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $jogo->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $jogo->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $jogo->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $jogo->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Jogo') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($jogo->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Rodada') ?></th>
                                <td><?= $jogo->has('rodada') ? $this->Html->link($jogo->rodada->id, ['controller' => 'Rodadas', 'action' => 'view', $jogo->rodada->id]) : '' ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Time') ?></th>
                                <td><?= $jogo->has('time') ? $this->Html->link($jogo->time->id, ['controller' => 'Times', 'action' => 'view', $jogo->time->id]) : '' ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Visitante') ?></th>
                                <td><?= $jogo->has('visitante') ? $this->Html->link($jogo->visitante->id, ['controller' => 'Visitantes', 'action' => 'view', $jogo->visitante->id]) : '' ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($jogo->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Gol Casa') ?></th>
                                <td><?= $this->Number->format($jogo->gol_casa) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Gol Visitante') ?></th>
                                <td><?= $this->Number->format($jogo->gol_visitante) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Aposta') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Aposta') ?></h4>
                        <?php if (!empty($jogo->aposta)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Gol Casa') ?></th>
                                                                <th><?= __('Visitante Casa') ?></th>
                                                                <th><?= __('Pontuacao') ?></th>
                                                                <th><?= __('Jogo Id') ?></th>
                                                                <th><?= __('Usuario Id') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($jogo->aposta as $aposta): ?>
                            <tr>
                                <td><?= h($aposta->id) ?></td>
                                <td><?= h($aposta->gol_casa) ?></td>
                                <td><?= h($aposta->visitante_casa) ?></td>
                                <td><?= h($aposta->pontuacao) ?></td>
                                <td><?= h($aposta->jogo_id) ?></td>
                                <td><?= h($aposta->usuario_id) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Aposta', 'action' => 'view', $aposta->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Aposta', 'action' => 'edit', $aposta->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Aposta', 'action' => 'delete', $aposta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $aposta->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
