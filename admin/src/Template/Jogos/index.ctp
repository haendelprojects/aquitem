
<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Jogos'), ['controller' => 'jogos']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Jogo'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Jogos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                                                    <th>
                                        <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('rodada_id', ['label' => false, 'placeholder' => 'rodada_id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('casa_id', ['label' => false, 'placeholder' => 'casa_id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('visitante_id', ['label' => false, 'placeholder' => 'visitante_id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('gol_casa', ['label' => false, 'placeholder' => 'gol_casa', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('gol_visitante', ['label' => false, 'placeholder' => 'gol_visitante', 'class' => '']); ?> 
                                    </th>
                                 
                                <th> 
                                    <?php 
                                    echo  $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);  echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index' ] ); 
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>              
                            </tr>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('rodada_id') ?></th>
                                                                <th><?= $this->Paginator->sort('casa_id') ?></th>
                                                                <th><?= $this->Paginator->sort('visitante_id') ?></th>
                                                                <th><?= $this->Paginator->sort('gol_casa') ?></th>
                                                                <th><?= $this->Paginator->sort('gol_visitante') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($jogos as $jogo): ?>
                            <tr>
                                                                <td><?= $this->Number->format($jogo->id) ?></td>
                                                                <td><?= $jogo->has('rodada') ? $this->Html->link($jogo->rodada->id, ['controller' => 'Rodadas', 'action' => 'view', $jogo->rodada->id]) : '' ?></td>
                                                                <td><?= $jogo->has('time') ? $this->Html->link($jogo->time->id, ['controller' => 'Times', 'action' => 'view', $jogo->time->id]) : '' ?></td>
                                                                <td><?= $jogo->has('visitante') ? $this->Html->link($jogo->visitante->id, ['controller' => 'Visitantes', 'action' => 'view', $jogo->visitante->id]) : '' ?></td>
                                                                <td><?= $this->Number->format($jogo->gol_casa) ?></td>
                                                                <td><?= $this->Number->format($jogo->gol_visitante) ?></td>
                                                                <td class="actions">

                                    <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $jogo->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                    <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $jogo->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>

                                     <?php  $label_active =  ( $jogo->ativo) ? __('Desativar') : __('Ativo'); ?>
                                      <?php  $icon_active =  ( $jogo->ativo) ? 'close' : 'check'; ?>


                                    <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span><span class="sr-only">' . $label_active . '</span>', ['action' => 'active', $jogo->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $jogo->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>