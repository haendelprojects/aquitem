<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Rodadas'), ['controller' => 'rodadas']) ?></li>
    <li><?= __('Add '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Add ' . ' Rodada') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($rodada) ?>
                    <fieldset>
                        <legend><?= __('Add Rodada') ?></legend>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('campeonato_id', ['type'=>'hidden', 'value' => $id_campeonato]) . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('campeonato_id', ['options' => $campeonatos, 'disabled', 'empty' => $id_campeonato]) . '</div>';
                        $rodadas = [];
                        for ($i = 1; $i < 39; $i++) {
                            $rodadas[] = $i;;
                        }
                        echo '<div class="col-lg-6">' . $this->Form->input('numero' , ['options' => $rodadas]) . '</div>';

                        echo '<div class="col-lg-6">' . $this->Form->input('data_rodada', ['empty' => true]) . '</div>' ;
                        echo '<div class="col-lg-6">' . $this->Form->input('fechar_apostas_rodada', ['empty' => true]) .'</div>';
                        ?>
                    </fieldset>

                    <h2>Jogos</h2>

                    <?php for($i = 1 ; $i <= 20/2 ; $i++): ?>

                        <fieldset>
                            <legend><?= __('Jogo ' . $i) ?></legend>
                            <?php
                            echo '<div class="col-lg-5">' . $this->Form->input('jogos.'.$i.'.casa_id', ['options' => $times]) . '</div>';
                            ?>

                            <div class="col-lg-2 text-center"> <span class="btn btn-primary">X</span></div>
                            <?php
                            echo '<div class="col-lg-5">' . $this->Form->input('jogos.'.$i.'.visitante_id', ['options' => $times]) . '</div>';
                            echo '<div class="col-lg-6">' . $this->Form->input('jogos.'.$i.'.gol_casa') . '</div>';
                            echo '<div class="col-lg-6">' . $this->Form->input('jogos.'.$i.'.gol_visitante') . '</div>';
                            ?>
                        </fieldset>

                    <?php endfor ;?>

                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
