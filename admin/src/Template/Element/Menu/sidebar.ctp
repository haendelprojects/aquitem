<div class="static-sidebar-wrapper sidebar-default ">
    <div class="static-sidebar">
        <div class="sidebar">
            <div class="widget">
                <div class="widget-body">
                    <div class="userinfo">
                        <div class="avatar">
                            <?= $this->Html->image('avatar.png', ['class' => 'img-circle', 'style' => 'width:100%']); ?>
                        </div>
                        <div class="info">
                            <span class="username"><?= $UserLogged['nome'] ?></span>
                            <span class="useremail"><?= $UserLogged['username'] ?></span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="widget stay-on-collapse" id="widget-sidebar">
                <nav role="navigation" class="widget-body">
                    <ul class="acc-menu">
                        <li class="nav-separator"><span>Explore</span></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-files"></i> Paginas'), ['controller' => 'paginas', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-briefcase"></i> Clientes'), ['controller' => 'clientes', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-menu-alt"></i> Categorias'), ['controller' => 'Categorias', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-user"></i> Usuários'), ['controller' => 'Users', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-info"></i> Notícias'), ['controller' => 'Noticias', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-desktop"></i> Videos'), ['controller' => 'Videos', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-announcement"></i> Anúncios'), ['controller' => 'Anuncios', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-layers"></i> Slideshow'), ['controller' => 'Slideshows', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-briefcase"></i> Classificados'), ['controller' => 'Classificados', 'action' => 'index'], ['escape' => false]); ?></li>
                        <li><?= $this->Html->link(__('<i class="ti ti-briefcase"></i> Interesses'), ['controller' => 'Interesses', 'action' => 'index'], ['escape' => false]); ?></li>

                    </ul>
                </nav>
            </div>

        </div>
    </div>
</div>
