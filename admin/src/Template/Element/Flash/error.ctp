<div class="alert alert-dismissable alert-danger">
    <i class="ti ti-close"></i>&nbsp; <strong>Oh no!</strong> <?= h($message) ?>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
</div>