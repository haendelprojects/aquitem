<div class="alert alert-dismissable alert-success">
    <i class="ti ti-check"></i>&nbsp; <strong> <?= __('Well done!') ?></strong> <?= h($message) ?>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
</div>