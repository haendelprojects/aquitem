
<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Aposta'), ['controller' => 'aposta']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Apostum'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Aposta') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                                                    <th>
                                        <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('gol_casa', ['label' => false, 'placeholder' => 'gol_casa', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('visitante_casa', ['label' => false, 'placeholder' => 'visitante_casa', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('pontuacao', ['label' => false, 'placeholder' => 'pontuacao', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('jogo_id', ['label' => false, 'placeholder' => 'jogo_id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('usuario_id', ['label' => false, 'placeholder' => 'usuario_id', 'class' => '']); ?> 
                                    </th>
                                 
                                <th> 
                                    <?php 
                                    echo  $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);  echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index' ] ); 
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>              
                            </tr>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('gol_casa') ?></th>
                                                                <th><?= $this->Paginator->sort('visitante_casa') ?></th>
                                                                <th><?= $this->Paginator->sort('pontuacao') ?></th>
                                                                <th><?= $this->Paginator->sort('jogo_id') ?></th>
                                                                <th><?= $this->Paginator->sort('usuario_id') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($aposta as $apostum): ?>
                            <tr>
                                                                <td><?= $this->Number->format($apostum->id) ?></td>
                                                                <td><?= $this->Number->format($apostum->gol_casa) ?></td>
                                                                <td><?= $this->Number->format($apostum->visitante_casa) ?></td>
                                                                <td><?= $this->Number->format($apostum->pontuacao) ?></td>
                                                                <td><?= $apostum->has('jogo') ? $this->Html->link($apostum->jogo->id, ['controller' => 'Jogos', 'action' => 'view', $apostum->jogo->id]) : '' ?></td>
                                                                <td><?= $apostum->has('user') ? $this->Html->link($apostum->user->id, ['controller' => 'Users', 'action' => 'view', $apostum->user->id]) : '' ?></td>
                                                                <td class="actions">

                                    <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $apostum->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                    <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $apostum->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>

                                     <?php  $label_active =  ( $apostum->ativo) ? __('Desativar') : __('Ativo'); ?>
                                      <?php  $icon_active =  ( $apostum->ativo) ? 'close' : 'check'; ?>


                                    <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span><span class="sr-only">' . $label_active . '</span>', ['action' => 'active', $apostum->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $apostum->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>