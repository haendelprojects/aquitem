<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Aposta'), ['controller' => 'aposta']) ?></li>
    <li><?= __('Add '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __( 'Add ' . ' Apostum') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($apostum) ?>
                    <fieldset>
                        <legend><?= __('Add Apostum') ?></legend>
                        <?php
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('gol_casa') . '</div>';
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('visitante_casa') . '</div>';
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('pontuacao') . '</div>';
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('jogo_id', ['options' => $jogos]) . '</div>';
                                                    echo  '<div class="col-lg-6">' .  $this->Form->input('usuario_id', ['options' => $users]) . '</div>';
                                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Save') , ['class'=>'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
