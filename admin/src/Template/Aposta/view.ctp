<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Aposta'), ['controller' => 'aposta']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Apostum'), ['action' => 'edit', $apostum->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $apostum->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $apostum->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $apostum->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $apostum->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Apostum') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($apostum->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Jogo') ?></th>
                                <td><?= $apostum->has('jogo') ? $this->Html->link($apostum->jogo->id, ['controller' => 'Jogos', 'action' => 'view', $apostum->jogo->id]) : '' ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('User') ?></th>
                                <td><?= $apostum->has('user') ? $this->Html->link($apostum->user->id, ['controller' => 'Users', 'action' => 'view', $apostum->user->id]) : '' ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($apostum->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Gol Casa') ?></th>
                                <td><?= $this->Number->format($apostum->gol_casa) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Visitante Casa') ?></th>
                                <td><?= $this->Number->format($apostum->visitante_casa) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Pontuacao') ?></th>
                                <td><?= $this->Number->format($apostum->pontuacao) ?></td>
                            </tr>
                                                                                                                                        </table>

                    </div>
                </div>
            </div>


                                    
        </div>
    </div>
</div>
