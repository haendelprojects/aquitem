<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Rodadas'), ['controller' => 'rodadas']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Rodada'), ['action' => 'edit', $rodada->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $rodada->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $rodada->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $rodada->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $rodada->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Rodada') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($rodada->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Campeonato') ?></th>
                                <td><?= $rodada->has('campeonato') ? $this->Html->link($rodada->campeonato->id, ['controller' => 'Campeonatos', 'action' => 'view', $rodada->campeonato->id]) : '' ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($rodada->id) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= $this->Number->format($rodada->numero) ?></td>
                            </tr>
                                                                                                                                            <tr>
                                <th><?= __('Data Rodada') ?></th>
                                <td><?= h($rodada->data_rodada) ?></td>
                            </tr>
                                                        <tr>
                                <th><?= __('Fechar Apostas Rodada') ?></th>
                                <td><?= h($rodada->fechar_apostas_rodada) ?></td>
                            </tr>
                                                                                                            </table>

                    </div>
                </div>
            </div>


                                    

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Jogos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Jogos') ?></h4>
                        <?php if (!empty($rodada->jogos)): ?>
                        <table class="table">
                            <tr>
                                                                <th><?= __('Id') ?></th>
                                                                <th><?= __('Rodada Id') ?></th>
                                                                <th><?= __('Casa Id') ?></th>
                                                                <th><?= __('Visitante Id') ?></th>
                                                                <th><?= __('Gol Casa') ?></th>
                                                                <th><?= __('Gol Visitante') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($rodada->jogos as $jogos): ?>
                            <tr>
                                <td><?= h($jogos->id) ?></td>
                                <td><?= h($jogos->rodada_id) ?></td>
                                <td><?= h($jogos->casa_id) ?></td>
                                <td><?= h($jogos->visitante_id) ?></td>
                                <td><?= h($jogos->gol_casa) ?></td>
                                <td><?= h($jogos->gol_visitante) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Jogos', 'action' => 'view', $jogos->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['controller' => 'Jogos', 'action' => 'edit', $jogos->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Jogos', 'action' => 'delete', $jogos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jogos->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
