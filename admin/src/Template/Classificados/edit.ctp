<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Classificados'), ['controller' => 'classificados']) ?></li>
    <li><?= __('Editar '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Editar ' . ' Classificado') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($classificado) ?>
                    <fieldset>
                        <legend><?= __('Editar Classificado') ?></legend>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('titulo') . '</div>';

                        echo '<div class="col-lg-12">' . $this->Form->input('descricao') . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('preco') . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('categoria', ['options' => $this->Categorias->getList()]) . '</div>';
                        ?>
                    </fieldset>

                    <fieldset>
                        <legend><?= __('Galeria') ?></legend>
                        <div>
                            <?php if ($this->request->data('fotos')) : ?>
                                <?php foreach ($this->request->data('fotos') as $foto) : ?>
                                    <img src="<?= $foto['image'] ?>" alt="" height="100">
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                        <input type="file" name="files[]" class="filer_input">
                    </fieldset>


                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
