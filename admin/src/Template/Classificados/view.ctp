<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Classificados'), ['controller' => 'classificados']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Classificado'), ['action' => 'edit', $classificado->id], ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php $icon_active = ($classificado->ativo) ? 'close' : 'check'; ?>
                <?php $label_active = ($classificado->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-' . $icon_active . '"></span> ' . $label_active, ['action' => 'active', $classificado->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $classificado->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Classificado') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($classificado->id) ?></h4>
                        <table class="table">
                            <tr>
                                <th><?= __('Categoria') ?></th>
                                <td><?= h($classificado->categoria) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Titulo') ?></th>
                                <td><?= h($classificado->titulo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($classificado->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Criado Por') ?></th>
                                <td><?= $this->Number->format($classificado->criado_por) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Preco') ?></th>
                                <td><?= $this->Number->format($classificado->preco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Created') ?></th>
                                <td><?= h($classificado->created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Modified') ?></th>
                                <td><?= h($classificado->modified) ?></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>


            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Descricao') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <?= $this->Text->autoParagraph(h($classificado->descricao)); ?>
                    </div>
                </div>
            </div>


            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Fotos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= __('Fotos') ?></h4>
                        <?php if (!empty($classificado->fotos)): ?>
                            <table class="table">
                                <tr>

                                    <th></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                                <?php foreach ($classificado->fotos as $fotos): ?>
                                    <tr>
                                        <td><img height="100" src="<?= h($fotos->image) ?>"/></td>
                                        <td class="actions">
                                            <?= $this->Form->postLink(__('Apagar'), ['controller' => 'Fotos', 'action' => 'delete', $fotos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fotos->id)]) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
