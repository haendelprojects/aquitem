<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Categorias'), ['controller' => 'categorias']) ?></li>
    <li><?= __('Editar '); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Editar ' . ' Categoria') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->Form->create($categoria) ?>
                    <fieldset>

                        <?php
                        echo '<div class="col-lg-6">' . $this->Form->input('nome') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('categoria_id' , ['empty'=>true,'label'=>'Categoria Pai']) . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('palavras_chaves' , ['maxlength'=>150]) . '</div>';

                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-success']) ?>
                    <?= $this->Form->end() ?>
                </div>

            </div>

        </div>

    </div>

</div>
