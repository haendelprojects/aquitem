<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Categorias'), ['controller' => 'categorias']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Categoria'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Categorias') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                <th>
                                    <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?>
                                </th>
                                <th>
                                    <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'nome', 'class' => '']); ?>
                                </th>


                                <th>
                                    <?php
                                    echo $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);
                                    echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index']);
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>
                            </tr>
                            <tr>
                                <th><?= $this->Paginator->sort('id') ?></th>
                                <th><?= $this->Paginator->sort('nome') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($categorias as $categoria): ?>
                                <tr>
                                    <td><?= $this->Number->format($categoria->id) ?></td>
                                    <td><?= h($categoria->nome) ?></td>
                                    <td class="actions">

                                        <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $categoria->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                        <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $categoria->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>
                                        <?= $this->Form->postLink('<span class="ti ti-trash"></span><span class="sr-only">' . __('Deletar') . '</span>', ['action' => 'delete', $categoria->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $categoria->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>