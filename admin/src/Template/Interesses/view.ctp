<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Interesses'), ['controller' => 'interesses']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Interess'), ['action' => 'edit', $interess->id ] , ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php  $icon_active =  ( $interess->ativo) ? 'close' : 'check'; ?>
                <?php $label_active =  ( $interess->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span> ' . $label_active , ['action' => 'active', $interess->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $interess->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Interess') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($interess->id) ?></h4>
                        <table class="table">
                                                                                                                <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($interess->nome) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Email') ?></th>
                                <td><?= h($interess->email) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Telefone') ?></th>
                                <td><?= h($interess->telefone) ?></td>
                            </tr>
                                                                                                                <tr>
                                <th><?= __('Celular') ?></th>
                                <td><?= h($interess->celular) ?></td>
                            </tr>
                                                                                                                                                                                                    <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($interess->id) ?></td>
                            </tr>
                                                                                                                                            <tr>
                                <th><?= __('Created') ?></th>
                                <td><?= h($interess->created) ?></td>
                            </tr>
                                                                                                            </table>

                    </div>
                </div>
            </div>


                                                <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Mensagem') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                            <?= $this->Text->autoParagraph(h($interess->mensagem)); ?>
                        </div>
                </div>
            </div>

                                                                        
        </div>
    </div>
</div>
