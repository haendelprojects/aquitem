
<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Interesses'), ['controller' => 'interesses']) ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mb-xl">
            <?= $this->Html->link(__('Adicionar Interess'), ['action' => 'add'], ['class' => 'btn btn-success-alt']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Interesses') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?= $this->Form->create(); ?>
                                                                    <th>
                                        <?= $this->Form->input('id', ['label' => false, 'placeholder' => 'id', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('created', ['label' => false, 'placeholder' => 'created', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'nome', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('email', ['label' => false, 'placeholder' => 'email', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('telefone', ['label' => false, 'placeholder' => 'telefone', 'class' => '']); ?> 
                                    </th>
                                                                    <th>
                                        <?= $this->Form->input('celular', ['label' => false, 'placeholder' => 'celular', 'class' => '']); ?> 
                                    </th>
                                 
                                <th> 
                                    <?php 
                                    echo  $this->Form->button(__('Filtrar'), ['type' => 'submit', 'class' => 'button button-success']);  echo '&nbsp';
                                    echo $this->Html->link(__('Redefinir'), ['action' => 'index' ] ); 
                                    ?>

                                </th>
                                <?= $this->Form->end(); ?>              
                            </tr>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('created') ?></th>
                                                                <th><?= $this->Paginator->sort('nome') ?></th>
                                                                <th><?= $this->Paginator->sort('email') ?></th>
                                                                <th><?= $this->Paginator->sort('telefone') ?></th>
                                                                <th><?= $this->Paginator->sort('celular') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($interesses as $interess): ?>
                            <tr>
                                                                <td><?= $this->Number->format($interess->id) ?></td>
                                                                <td><?= h($interess->created) ?></td>
                                                                <td><?= h($interess->nome) ?></td>
                                                                <td><?= h($interess->email) ?></td>
                                                                <td><?= h($interess->telefone) ?></td>
                                                                <td><?= h($interess->celular) ?></td>
                                                                <td class="actions">

                                    <?= $this->Html->link('<span class="ti ti-search"></span><span class="sr-only">' . __('Detalhes') . '</span>', ['action' => 'view', $interess->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                                    <?= $this->Html->link('<span class="ti ti-pencil"></span><span class="sr-only">' . __('Editar') . '</span>', ['action' => 'edit', $interess->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>

                                     <?php  $label_active =  ( $interess->ativo) ? __('Desativar') : __('Ativo'); ?>
                                      <?php  $icon_active =  ( $interess->ativo) ? 'close' : 'check'; ?>


                                    <?= $this->Form->postLink('<span class="ti ti-'.$icon_active.'"></span><span class="sr-only">' . $label_active . '</span>', ['action' => 'active', $interess->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $interess->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>