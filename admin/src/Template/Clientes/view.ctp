<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Clientes'), ['controller' => 'clientes']) ?></li>
    <li><?= __('Detalhes'); ?></li>
</ol>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">

        </div>
        <div class="col-sm-3">
            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                <a href="#" class="list-group-item">
                    <?php

                    if (strpos($cliente->logo, 'http://img.') === false) {
                        echo $this->Html->image( $cliente->logo, ['style' => 'width:70%; margin-left: 10px']);
                    } else {
                        echo $this->Html->image($cliente->logo, ['style' => 'width:70%; margin-left: 10px']);
                    }

                    ?>
                </a>
                <?= $this->Html->link(__('<span class="ti ti-pencil"></span> ' . 'Edit Cliente'), ['action' => 'edit', $cliente->id], ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php $icon_active = ($cliente->ativo) ? 'close' : 'check'; ?>
                <?php $label_active = ($cliente->ativo) ? __('Desativar') : __('Ativar'); ?>

                <?= $this->Form->postLink('<span class="ti ti-' . $icon_active . '"></span> ' . $label_active, ['action' => 'active', $cliente->id], ['confirm' => __('Deseja (des)Ativar # {0}?', $cliente->id), 'escape' => false, 'class' => 'list-group-item', 'title' => __('Delete')]) ?>
            </div>
        </div><!-- col-sm-3 -->

        <div class="col-sm-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= h('Cliente') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <h4><?= h($cliente->id) ?></h4>
                        <table class="table">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($cliente->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Email') ?></th>
                                <td><?= h($cliente->email) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Site') ?></th>
                                <td><?= h($cliente->site) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Logradouro') ?></th>
                                <td><?= h($cliente->logradouro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= h($cliente->numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Complemento') ?></th>
                                <td><?= h($cliente->complemento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td><?= h($cliente->bairro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Uf') ?></th>
                                <td><?= h($cliente->uf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cidade') ?></th>
                                <td><?= h($cliente->cidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($cliente->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cartao Desconto') ?></th>
                                <td><?= h($cliente->cartao_desconto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Plano') ?></th>
                                <td><?= $cliente->has('plano') ? $this->Html->link($cliente->plano->id, ['controller' => 'Planos', 'action' => 'view', $cliente->plano->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Categoria') ?></th>
                                <td><?= $cliente->has('categoria') ? $this->Html->link($cliente->categoria->id, ['controller' => 'Categorias', 'action' => 'view', $cliente->categoria->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Logo') ?></th>
                                <td><?= h($cliente->logo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Img Header') ?></th>
                                <td><?= h($cliente->img_header) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Facebook') ?></th>
                                <td><?= h($cliente->facebook) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Wix') ?></th>
                                <td><?= h($cliente->wix) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Slug') ?></th>
                                <td><?= h($cliente->slug) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($cliente->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cadastrado Por') ?></th>
                                <td><?= $this->Number->format($cliente->cadastrado_por) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Modified') ?></th>
                                <td><?= h($cliente->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Data Cadastro') ?></th>
                                <td><?= h($cliente->data_cadastro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $cliente->status ? __('Yes') : __('No'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Plus') ?></th>
                                <td><?= $cliente->plus ? __('Yes') : __('No'); ?></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>


            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Slogan') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <?= $this->Text->autoParagraph(h($cliente->slogan)); ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Subdescricao') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <?= $this->Text->autoParagraph(h($cliente->subdescricao)); ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Descricao') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <?= $this->Text->autoParagraph(h($cliente->descricao)); ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Map Embed') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">
                        <?= $this->Text->autoParagraph(h($cliente->map_embed)); ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Telefones') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Telefones') ?></h4>
                        <?php if (!empty($cliente->telefones)): ?>
                            <table class="table">
                                <tr>

                                    <th><?= __('Numero') ?></th>
                                    <th><?= __('Apagar') ?></th>

                                </tr>
                                <?php foreach ($cliente->telefones as $telefones): ?>
                                    <tr>

                                        <td><?= h($telefones->numero) ?></td>

                                        <td class="actions">
                                            <?= $this->Form->postLink(__('Apagar'), ['controller' => 'Telefones', 'action' => 'delete', $telefones->id], ['confirm' => __('Are you sure you want to delete # {0}?', $telefones->id)]) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>


            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Fotos') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <div class="about-area">

                        <h4><?= __('Fotos') ?></h4>
                        <?php if (!empty($cliente->fotos)): ?>
                            <table class="table">
                                <tr>

                                    <th></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                                <?php foreach ($cliente->fotos as $fotos): ?>
                                    <tr>
                                        <td>
                                            <?php

                                            if (strpos($cliente->logo, 'http://img.') === false) {
                                                echo $this->Html->image( $fotos->image, ['style' => 'width:70%; margin-left: 10px']);
                                            } else {
                                                echo $this->Html->image($fotos->image, ['style' => 'width:70%; margin-left: 10px']);
                                            }

                                            ?>
                                        </td>
                                        <td class="actions">
                                            <?= $this->Form->postLink(__('Apagar'), ['controller' => 'Fotos', 'action' => 'delete', $fotos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fotos->id)]) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
