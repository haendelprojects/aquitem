<ol class="breadcrumb">
    <li><?= $this->Html->link(__('Home'), ['controller' => 'dashboard']) ?></li>
    <li><?= $this->Html->link(__('Clientes'), ['controller' => 'clientes']) ?></li>
    <li><?= __('Editar '); ?></li>
</ol>
<div class="container-fluid">
    <?= $this->Form->create($cliente) ?>
    <div class="row">
        <div class="col-xs-9">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><?= __('Editar ' . ' Cliente') ?></h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">

                    <fieldset>
                        <legend><?= __('Detalhes') ?></legend>
                        <?php
                        echo '<div class="col-lg-12">' . $this->Form->input('nome') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('slogan') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('subdescricao') . '</div>';
                        echo '<div class="col-lg-12">' . $this->Form->input('descricao', ['class' => 'editor']) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('email') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('site') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('facebook') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('wix') . '</div>'; ?>


                    </fieldset>

                    <fieldset>
                        <legend><?= __('Endereço') ?></legend>
                        <?php
                        echo '<div class="col-lg-3">' . $this->Form->input('cep', ['class' => 'zipcode mask', 'maxlength' => 9, 'data-inputmask' => "'mask':'99999-999'"]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('logradouro', ['class' => 'street']) . '</div>';
                        echo '<div class="col-lg-3">' . $this->Form->input('numero', ['class' => '']) . '</div>';
                        echo '<div class="col-lg-3">' . $this->Form->input('complemento', ['class' => '']) . '</div>';
                        echo '<div class="col-lg-3">' . $this->Form->input('bairro', ['class' => 'neighborhood']) . '</div>';
                        echo '<div class="col-lg-3">' . $this->Form->input('uf', ['class' => 'state']) . '</div>';
                        echo '<div class="col-lg-3">' . $this->Form->input('cidade', ['class' => 'city']) . '</div>';


                        ?>
                    </fieldset>
                    <fieldset>
                        <legend><?= __('Detalhes') ?></legend>
                        <?php

                        echo '<div class="col-lg-6">' . $this->Form->input('cartao_desconto') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('data_cadastro') . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('status', ['options' => [1 => 'Ativo', 0 => 'Desativado']]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('plano_id', ['options' => $planos]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('categoria_id', ['options' => $categorias]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('plus', ['options' => ['Não', 'Sim']]) . '</div>';
                        ?>
                    </fieldset>

                    <fieldset>
                        <legend>Novos Telefones</legend>
                        <?php
                        echo '<div class="col-lg-6">' . $this->Form->input('telefones.0.numero', ['class' => 'mask', 'maxlength' => 15, 'data-inputmask' => "'mask':'(99) 99999-9999'"]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('telefones.1.numero', ['class' => 'mask', 'maxlength' => 15, 'data-inputmask' => "'mask':'(99) 99999-9999'"]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('telefones.2.numero', ['class' => 'mask', 'maxlength' => 15, 'data-inputmask' => "'mask':'(99) 99999-9999'"]) . '</div>';
                        echo '<div class="col-lg-6">' . $this->Form->input('telefones.3.numero', ['class' => 'mask', 'maxlength' => 15, 'data-inputmask' => "'mask':'(99) 99999-9999'"]) . '</div>';
                        ?>
                    </fieldset>

                    <fieldset>
                        <legend><?= __('Adicionar Fotos a Galeria') ?></legend>
                        <div>
                            <?php if ($this->request->data('fotos')) : ?>
                                <?php foreach ($this->request->data('fotos') as $foto) : ?>
                                    <img src="<?= $foto['image'] ?>" alt="" height="100">
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                        <input type="file" name="files[]" class="filer_input">
                    </fieldset>

                </div>

            </div>

        </div>


        <div class="col-xs-3">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><i class="ti ti-image"></i> Alterar Logo</h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">
                    <?= ($this->request->data('logo') ? '<img style="width:100%" src=' . $this->request->data('logo') . ' />' : '') ?>
                    <input type="file" name="logo" class="filer_input">
                </div>
            </div>


            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2><i class="ti ti-image"></i> Alterar Imagem do Cabeçalho</h2>
                    <div class="panel-ctrls" data-actions-container=""
                         data-action-collapse='{"target": ".panel-body"}'></div>
                    <div class="options">

                    </div>
                </div>
                <div class="panel-body">

                    <?= ($this->request->data('img_header') ? '<img style="width:100%" src=' . $this->request->data('img_header') . ' />' : '') ?>
                    <input type="file" name="img_header" class="filer_input">
                </div>
            </div>
        </div>
    </div>

    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-success']) ?>
    <?= $this->Form->end() ?>

</div>
