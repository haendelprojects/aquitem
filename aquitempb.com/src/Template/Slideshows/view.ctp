<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Slideshow'), ['action' => 'edit', $slideshow->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Slideshow'), ['action' => 'delete', $slideshow->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slideshow->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Slideshows'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Slideshow'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="slideshows view large-9 medium-8 columns content">
    <h3><?= h($slideshow->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('File') ?></th>
            <td><?= h($slideshow->file) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($slideshow->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Link') ?></th>
            <td><?= h($slideshow->link) ?></td>
        </tr>
        <tr>
            <th><?= __('Ativo') ?></th>
            <td><?= h($slideshow->ativo) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($slideshow->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Posicao') ?></th>
            <td><?= $this->Number->format($slideshow->posicao) ?></td>
        </tr>
    </table>
</div>
