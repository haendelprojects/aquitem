<nav class="large-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo Slide'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="slideshows index large-8 medium-8 columns content">
    <h3><?= __('Slides') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('file') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('link') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th><?= $this->Paginator->sort('posicao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($slideshows as $slideshow):?>
            <tr>
                <td><?= $this->Number->format($slideshow->id) ?></td>
                <td><?= $this->Html->image( $slideshow->slide  , ['style'=> 'width:200px; height: 100px' ]); ?></td>
                <td><?= h($slideshow->description) ?></td>
                <td><?= h($slideshow->link) ?></td>
                <td><?= h($slideshow->ativo) ?></td>
                <td><?= $this->Number->format($slideshow->posicao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $slideshow->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $slideshow->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $slideshow->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slideshow->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
