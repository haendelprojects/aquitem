<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $slideshow->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $slideshow->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Slideshows'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="slideshows form large-9 medium-8 columns content">
    <?= $this->Form->create($slideshow) ?>
    <fieldset>
        <legend><?= __('Edit Slideshow') ?></legend>
        <?php
            echo $this->Form->input('file');
            echo $this->Form->input('description');
            echo $this->Form->input('link');
            echo $this->Form->input('ativo');
            echo $this->Form->input('posicao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
