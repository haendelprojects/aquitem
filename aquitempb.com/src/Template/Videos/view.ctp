<div class="videos view large-12 medium-12 columns content-site">
    <h3><?= h($video->titulo) ?></h3>


    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= h($video->url) ?>" frameborder="0" allowfullscreen></iframe>

        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($video->descricao)); ?>

</div>
