<div class="row content-site">
    <div class="noticias view large-12 medium-12 columns" style="background: #fff">
        <br>
        <h3><?= h($noticia->titulo) ?></h3>
        <span>Publicado por <?= $noticia->user->nome; ?> em <?= $noticia->data_cadastro; ?></span>
        <br>
        <div>
            <?= $this->Text->autoParagraph($noticia->texto); ?>
        </div>


    </div>
</div>
<br></br>

<div class="row content-site">
    <div class="large-12">
        <h2 class="titulo-sessao">Aqui tem Notícias</h2>
    </div>
    <?php foreach ($noticias as $noticia) : ?>
        <a href="<?= URL; ?>/noticias/<?= $noticia->slug; ?>"
           class="large-4 column noticia">
            <div class="noticia-image">
                <?= $this->Html->image(REPOSITORY_IMG . $noticia->thumb); ?>
            </div>
            <span class="noticia-data"><?= $noticia->data_cadastro; ?></span>
            <h3 class="noticia-titulo">
                <?= $noticia->titulo; ?>
            </h3>
        </a>
    <?php endforeach; ?>

    <p class="noticia-ler-mais"><?= $this->Html->link('+ Ler mais', ['controller' => 'noticias']); ?></p>
</div>