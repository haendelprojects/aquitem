<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 300px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 300px; overflow: hidden;">
            <?php foreach($slideshows as $slideshow): ?>
            <div data-p="112.50" style="display: none;">
                <a href="<?= $slideshow->link; ?>">
                <?= $this->Html->image( REPOSITORY_IMG . $slideshow->file, ['data-u' => 'image']); ?>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora12l" style="top:0px;left:0px;width:30px;height:46px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora12r" style="top:0px;right:0px;width:30px;height:46px;" data-autocenter="2"></span>
        <a href="http://www.jssor.com" style="display:none">Slideshow Maker</a>
    </div>

<div class="noticias row">
    <div class="large-12 column">

        <div class="row content-site">
            <div class="large-12">
                <h2 class="titulo-sessao section-notice-color">Aqui tem Notícias</h2>
            </div>
            <?php foreach ($noticias as $noticia) : ?>
                <a href="<?= URL ?>/noticias/<?= $noticia->slug; ?>"
                   class="large-4 column noticia">
                    <div class="noticia-image">
                        <?= $this->Html->image( REPOSITORY_IMG . $noticia->thumb); ?>
                    </div>
                    <span class="noticia-data section-notice-color"><?= $noticia->data_cadastro; ?></span>
                    <h3 class="noticia-titulo section-notice-color">
                        <?= $noticia->titulo; ?>
                    </h3>
                </a>
            <?php endforeach; ?>

            <p class="noticia-ler-mais large-12 column text-right"><?= $this->Html->link('+ Ler mais', ['controller' => 'noticias'] , ['class' => 'section-notice-bg']); ?></p>
        </div>
    </div>
</div>

<div class="noticias row">
    <div class="large-12 column">

        <div class="row content-site">
            <div class="large-12">
                <h2 class="titulo-sessao section-ads-color">Anúncios</h2>
            </div>
            <?php foreach ($anuncios as $anuncio) : ?>
                <a href="<?= $anuncio->link; ?>"
                   class="large-4 column noticia">
                    <div class="noticia-image">
                        <?= $this->Html->image(REPOSITORY_IMG . $anuncio->img); ?>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<div class="noticias row">
    <div class="large-12 column">

        <div class="row content-site">
            <div class="large-12">
                <h2 class="titulo-sessao section-fun-color">Vídeos</h2>
            </div>
            <?php foreach ($videos as $video) : ?>
                <a href="<?= URL . '/videos/' . $video->id; ?>"
                   class="large-4 column noticia">
                    <div class="noticia-image">
                        <?= $this->Html->image('http://i1.ytimg.com/vi/' . $video->url . '/hqdefault.jpg'); ?>
                    </div>

                    <h3 class="noticia-titulo section-fun-color">
                        <?= $video->titulo; ?>
                    </h3>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<!--classificados-->

<div class="noticias row">
    <div class="large-12 column">

        <div class="row content-site">
            <div class="large-12">
                <h2 class="titulo-sessao section-class-color">Classificados</h2>
            </div>
             <?php foreach ($classificados as $classificado) : ?>
                <a href="<?= \Cake\Routing\Router::url('/' , true) ?>/classificados/view/<?= $classificado->id; ?>"
                   class="large-4 column noticia">
                    <div class="noticia-image">
                        <?= $this->Html->image( $classificado->fotos[0]->image); ?>
                    </div>
                    <span class="noticia-data section-notice-color"><?= $classificado->data_cadastro; ?></span>
                    <h3 class="noticia-titulo section-notice-color">
                        <?= $classificado->titulo; ?>
                    </h3>
                </a>
            <?php endforeach; ?>

            <p class="noticia-ler-mais large-12 column text-right "><?= $this->Html->link('+ Ler mais', ['controller'=>'classificados'], ['class' => 'section-notice-bg']); ?></p>


        </div>
    </div>
</div>

