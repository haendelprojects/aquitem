<style type="text/css">
    .classificados-itens {
        padding : 10px;
        margin-bottom: 15px;
    }

    .classificados-itens h2 {
        font-size: 24px;
        font-weight: 600;
    }

    .classificados-itens h3 {
        font-size: 22px;
        color: #de2700;
    }


</style>


<div class="noticias index large-12 medium-12 columns ">
        
    <div class="row ">
        <div class="large-12">
            <h2 class="titulo-sessao">Aqui Tem Classificados</h2> 
        </div>

            <?php foreach ($classificados as $classificado):?>
            <div class="content-site classificados-itens">
                <h2><?= h($classificado->titulo) ?></h2>
                <h3>R$ <?= $classificado->preco ?></h3>
                <div><?= $classificado->descricao ?></div>

                 <div class="content-site galeria">
                    <?php foreach ($classificado->fotos as $foto): ?>
                        <a class="fancybox" rel="gallery1" href="<?= (strpos($foto->image , 'http://img.') === false) ? REPOSITORY_IMG  : '' ?><?= $foto->image; ?>" title="Galeria da empresa">
                            <img src="<?= (strpos($foto->image , 'http://img.') === false) ? REPOSITORY_IMG  : '' ?><?= $foto->image; ?>" alt=""/>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endforeach; ?>
        
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
