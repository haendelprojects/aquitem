<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Aqui Tem PE';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?> | Lista telefônica de Pernambuco
    </title>

    <meta name="description"
          content="Ache no AquiTemPE telefones e endereços de empresas, serviços, produtos, profissionais, classificados , noticias, fotos de empresas.">

    <meta name="keywords"
          content="<?= $keywords ?>"/>

    <meta name="robots" content="index,follow"/>
    <meta http-equiv="Cache-Control" content="no-cache, no-store"/>
    <meta http-equiv="Pragma" content="no-cache, no-store"/>
    <meta name="rating" content="General"/>
    <meta name="distribution" content="Global"/>
    <meta name="Author" content=" Erick Haendel - www.aquitempe.com"/>
    <meta name="language" content="pt-br"/>
    <meta name="resource-type" content="document"/>
    <meta name="DC.title" content=" "/>

    <?= $this->Html->meta('icon') ?>


    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->Html->css('helpers/jquery.fancybox-thumbs'); ?>
    <?= $this->Html->css('helpers/jquery.fancybox-buttons'); ?>
    <?= $this->Html->css('jquery.fancybox'); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28058458-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4586879461902086",
    enable_page_level_ads: true
  });
</script>
</head>
<body>


<section class="container clearfix">
    <div class="row">

        <!--		master header ad-->
        <div class="large-10 large-offset-1 master-header-ad columns box-content">
            <div class="row">
                <div class="large-7 master-header-ad-banner columns">
                    <?php foreach ($anuncio_topo as $anuncio): ?>
                    <!-- <a href="<?= $anuncio->link; ?>">
                        <?= $this->Html->image( REPOSITORY_IMG .  $anuncio->img); ?>
                        </a> -->

                    <?php endforeach; ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Topo -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7736371220220774"
     data-ad-slot="4334159245"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                </div>
                <div class="large-5 master-header-ad-contact columns text-center">
                    <p class="master-header-ad-contact"> contato@aquitempe.com | 81 98449.1600 - 99308.0331 <img width="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABF1BMVEX///9Vxzwtbh9Dny/+/v7c3NxCnC5NxTFSxjji3+NIxCpPxjREojBLxS5GxCfi4OIqbRtLsTU0mxklahQrah4XZgA9nSc8nSVUxTtPujjj4+Pj9OAAYADv+e0haQ5GpjGR2IRiy0xozFOs4aOA03DL7MWc3JG3zLTU2dP4/Pc1gCXX8NOJ1nt30WZVpUWl35vI08bB6Lqiw53d7Nrs8us4dCs6jCmz46onlgCLqYaewZjR7sysx6horFuRvIo3hSZXhU9NgUPR3c98m3eyv69AeDV1sWqLpYeatJZRpEBPgEZqk2OEvHqtwqljqlWdsZne5txpk2G6xbiizJq82rdzlW1CiDQ7rxvQ5MyHuH+z1K2ayJJ8s3JJ9pJxAAASY0lEQVR4nO2dDVfaSBfHgYAJIEkaGuKWl4ICkkUU3XVprbq6vrW11dZud9d9/P6f45kkQGYmM5OZJBC6x/85e7aniuXHvXPf5oKZzLOe9axnPetZz3pWMrKbw97+ZGuv3wbq721N9nvDetNO+2klonpva2czW1HVUqUiy2VXslyplEqqqnRG7a39YTPt5xhZ9f32JiCTy4qSJUtRyjKAV7p7vR8Osz7pZtVKmYaGg8qlUqe9/8NQ2r02P52PWa6onf4w7ScfruZkVCqJ0s0pZVXe6eXTZmDI3h+plYh0M5VLcvtd2iAUDXdi400h1ezW6p3J/KSjlpPAcwXctbtaR7LZT8h8vspqZz9trLnqO6qcLJ4rpZSdpI3mCvAl554YY6WSPuMC+VxVsun6arO9WL6s46udFJPH1kLOX4BRHdXT4XvXqSyBz1FZ3UuBz+6qCecHlirZpbtqT170AUSlqDtLrVeXa0BPsrxEM75TlmtAT4raXhZgf/kG9CRvLCWo2pvLCqFBKerW4gGH5TQ8dC61u+iAM0nLQ2eSO4ttHftqunxZpxxfZOc4Su8I+lLUhRXj9uYyylAOLaqIa2ZTjTGwFpMZm9mUYwysUjd5wLq8QoCgFB8lDhh1yrsoyQkjrhxgNvv3dZKAzdUD/GnNOEkO0F6lIOPppzVJMhKzYr6zMmliJgcQID4mRDhaUUCA+C0RwJ1kSzVFcS66wX/Uq+FQzQAl6c2fCQBuJVRsK4pzo10qb47a7X6/394ZdeSSWhK+SoUBgRV/iQ3YSwLQueDNjvr7Q2z/wq73JjsbghfGMKBUk+JudNRL8fGc6+vZJX0elft3zf12VuUumRBASbLi5oxOzDwB8EYeXZ4q95Xc6/AN0DFA4Kf/iwXYjdcvldXNSZNJB1HW+0op9PUMAIJo808MwEmsQyir7ToX3gwyv98JGZIQAIEVox/FehxAWd6z+fFmkL1NFiMRUKrdRSaMcQjLal+Yb8rYofoqGTBG4m9HPoSKutOMwucx7pfJ/zANEBzFaFnxXWQfLXWGUflcRrtNclU6oFT7EAXQjlpVKeqEwjf70XnbngcH8jfWg3eTDMCIKWMnoo9WOnXS03ZftOFkrz3a7GSdmrSz2e1v9epkykxmDzMjEzCSnw4j+qgzBiPhuduKpYqzi+k+dfA/d/ey3J0QUwp2fRACGCWebkbyUWdaG8TLs7YVnZLV3UkMnEboCigMEPipaJcxiVSPKuWAh7q1isourRVZ3dgKBF8QzGd+FA4IClSxvG9HAix3moFnORxxFdUAsh1knNZUPICSJRZs+lHCTHnTzuB8zBIFe7iTRDHEfZUXEASbXwUAI5Vr5c08Blgfid3FuYUQ+iPeqQonoFQTGUx1I0xmcMBMvi++MCWXe6irZoZ/cwIK9ftRMkW5g778mZ4SxdMVtYv8nEz+2OAlrPE3wyPxTKFk0SeWIRZePCorwwwEWCjyI3Kn/QgmVEpIkMg0Y2yEOesIPmChUDzgReQ2YgQTqkMEcBjerDN/Wtc70i4gQLyorfMhcp7ECIEU1NpojI853ZHdxDoFLBS0szU+RIsvnO6IR8A2ApjAhNUpjuaAQLnf+BDf8CxqNIWfn9JBguheIhPW0hAC1B4afIhchc2ecJBX4WI0GUCA+Pe4CCHemL/zIXIsFAnfpJXgQxhzPOfrxZo11iDEc/1nHkSOFqMnWnMrmzBgIpcALqBUWx9AhGPz5c8chLXdUELhVAH7aLz5IwroPNsC7KetnM5jxLCEIRxnKn04jm4EXh+nlxfOHS+8WtS6ho5i4VDP6eE1amis2RKsRRQFDqOBS4ByaWM06mQVsZ/6YgZiHPmI2qWZy+U4yvAQQtEhMDy18Lo55Ktdb+U1H/gKFyBAhKINCDYOYli8MdjXGKLnCA4zQQ/3x4oiOQgCBEcRMuKDY8SXYYnR+sR2UsFkqPYgQvy+H7Yv/1jkBeKIiJ+e6VyISTqp0mH4aGkPTiO8teAL7KQZA8yIuZchuZ/ppqJOWoJnh9g8rTxCilXOOwIcUKpB8VS70l1Edu5nRtOJWMxTspAJ97DHquhYia9WCgAiwcYLp6GIrGjaFXNSed6q5jM2doIre9hMiSfWEABBWwslRdeGQMzcz2owBCs2yEwBgLKdRwk5TgAJEDXiRcsjfMnK/ca/VEDB8UW5C5kJe2i5j5oQIIbeE5ABESOOzdxM9MTIGGYINk4lP1UETjA61vBibcghpwA6RsRjTRgilVCw6q74jhhIM6XgbZLNJqQCStbj3IhO/T0TPTFSq29bbHwBOWnAv5VRkJCdEumAkrTuh5oH303piNSDKJgNK34yzLSxJ19uEwhZKZEFKBnHfqw51CFESu6v0Qq3sIOCCY6k+COhPAIh0u/YmIBw1te+V3MQIiUx0q71cUOwBVVsmXd4miETUiMZGxCcRKyHghCJ3/+Gcpcodu0LOWKmj782ZMLAC8EJKBnbczcdIISU3E8rTaMfw0DBLgfSYZ6eL0IBkWgK5QsPkfBoSqgRDDT+fIYw+ijvkAjJThIOKEm7GvkgUhKjRV4AF5yy+dmQ8Ei4rZp/G3k3gAcQ6qG0+xZOGESkbGaINRZQX0G6E1ftACDZR7gAoYOIhRpaYiQSBsIFmxDK94RaqPQOv5In71jxAUqW3+rjoYaYGMkLmWI1G1RaZwgPxFN+xiaODzgBpdqTX30TCAOJkVy3iU0w/HyQaRLcG5kyOtokeQgvIDyR0giAgcRITBe2YNk9TxbkA4YMOEBZRzrk3IBAfjA904mMSGIkpgvBabd/0MiJHLnOCM7hRAFrfjANpotZSPW/nTirEU2H8waQksiRDpF0YyMCCKcLKiGUNYhDU1pJRSP0Ez45zaANVDCOCQFCXfB8kkE4jPOsQdweEkz4ECFljAxPi4OHVQwQJryhEvqIxJQv2jv5hJSWQdlAjNhHfr4gICehnxhJ/ZPgQD+cMFtBGgwkG4kC8hLOEyNptC84huIgxC74IT99LQrITThDNAiEgguX4ecwMNifX/GLA/JFGk9u7n9DIBTr8OFsQS/ZsU2i6XJ8BEA4W1xQs8VUTseYACGU8RkxClt3c0u3KIBcGd/XWhKElR67pvGEVTZOfxEJUJL8Sc1ZOGFuLQnCCbsunaqExtN6JSLgLn2MQVQChLJ/t0TqLebCtha/rNUIz98wLDYgPE8MCTSeGvEJoUkM82pQUaDV2syrwmA3CFPbHj9aTEbrlNkB8xGKtfjwCWP3zlDKAIBAd/hCrDHWtOLglGVHy596P0QnFFxSKNPvDlHNd4o8wELxEUU0jl37FAundDtC6fA+MqHwGsY8EYQV7dOdjCkgYNmWIBRjPgstDh5pdoSG3uySZiaTQChYeUO3h6HNsxtt5oDgWRYejVnAQTa7ioMngxSJoEtSrmSR0w8TIISCadh9gFKqZyBAB2V856FYJ1oB/cIJgREKNIUcT7KonhEIBTtguMMNrdoVpYkAAlMUtx1Ga7eAy/lC4Bj69xZjrmNY/U4gFF6cVP0kELoAoLzYLmIkLuPdoBCQVjwKnEZ/DsUXaFpvCYTCe5dQLmfcDHp6LVljHBGgDAJ/55nxBHVUaFrKU5UCme8JhIJ33OhBDEk1TqlmBKxIVfERNSLkpAWeUwgItwmEwouX8BUp28W9WnSa93gIr7Fg4zspX77PmX+QCOPsPzOj6azYNk45EYu7CCEUSUPb36kaxPciCpZtqJvu0yPxa9/bTigHDxfmpNB64iGfl7aINzOCa3tODoBqauoyN9wu1SweT9W2kXQBxxlOJ9WvSIDCCRG4qX+FRn19sH7QuA43Y/EJcVJ4JYovkuaqt0RC8bcDIYtt5BYq0PBateOixsADGqAmhMs6vlOYa5GSBZD4WwbhlRriSSR19MbuNpMRi6TwKeRL9yCUjsmE4u87hFcuSNmGPLKoGXfHRaqvFo8QE0JrGNxxJtf4QiaM8BZ11W/gCaUbdSZTMz4cFCj1DP6OUf9LpCt8ig3JgOJvecKMuIc9njl0MmqP46CzFrUnYnvsEZ5zmrD6lUIoHmrQgSjm5mFTNcu4OxggkEXteA1NhXD3yG9CWqDJZILvWgoVvJGQGcJG5Bgb1izr5LhQdCg1rVgcHH0I9E0QP/cppAaaKO+PRUs32Alek5AIkIZxcrQ9Ho+PT++CIwzkTUG8gRQQUj8ERLTNx4wI1wycgJ4lDUdWsLWH3zBTGHDmQlDRnNMAIx1EKNb4PrDB+e5ytixoiaag3XIT0o9hpI8vg/pge/YCJQR4BwNyhxla6+RJPCPC620zJ08GsIaOcLgGUFNCxrudxYtvuIOaJouNJPik2geYT/vKV3I7opTdnkLeMEAQvGVaShDQQgH54yhtgjGTaL6AJxle/5QQ4B0CyNkWTgmZnzUkWrgFnDQZQOMarecEDiF5GBzdTSEndSNpQoCPaFX+kf8QhjmpqJvCTupE0kSiaA0byglkQpcw5FPNxD4OEn4DInBSBJB0w8Ij6wM6O9YuRA4heZ6PSOhjMdBICgFaxofrXe5PP0IMeKppMQAZVfdMIqtR8D3wfmUOCPBOx6BXOJZCbueDMnax4b8ooF4NAxSqTVEn9Wzg4rkNUUFj3eoS+dbwYaMoILMmnUlgWgN1Tra67uLtOtabP8GQ23lENaN2VMCa/lffBQEpw25U/LEGiaQGwLs7GuMTpmIBtLUcQadm7B7gj9UKZ6KAzIptLu5OvwJ96pjkjiRIw6Witn1Nav4QPOspOGHUBldCacI1IaOt8MXdB0OR9MvRgD4D1YqDgxOLTFmzwMF9Oi4EH61dtgQqGU+M3heWzftr7eeRNPMKP0BByML26Ymz+2RZNU/gT4ZR2308AAeX8GjtRtRDczypwhPnZ9TMnTTzKvgESZBFbbB9cPp0fX1ycnL99Hh0sD32xlCEbx58jABIuZAJKs9HOHNSPsDpMwecc2k0w2ugWxIpRecmDClJBY04c1IKYIjbMl+GcRQDCpiQ8yROnZQIqGnjy6iM2uC7KRxiPBNynkJHPJelnpMSAAHezblpVu9fRWDUBhemcI6YmpAvkE4VnhO9dI8Dapr2cHNounHe1O8HYoxaDD7eXDhT+AfLuTUpCujgXUzx3Be11boYUMMJge/yNjof/TqGotDq1HFSGBCQPFzoJpalq+bZPQ+k5rh2LlIAnZuQcmdIU9gOmBNJfUDAcPldJz5B3fQg6ZTO1x5urvAXR1DEPS+mQobDwElngOD0XN62GK+/3jKvLi4dSozT/ZvB5c2ZGRPP+TeEfxVLnv1ZgMBJX03x7s9YeHNIUz+7uL98GHgmBxqML+9vbnPx6RzxJ3tfzGADIumrKZ7JfXpA5DGB9MOrq8PDqvNHs1VNgC4XOkKkiPWx5fJW0Ut7UV5/HSgRLl/sKTBNNsOI8iB+bEhQ5ucogEw/Xf9rdfCi+qgj2i9i2ViX1l6mjeVLb0XyUVfk4akzNlz/OW0uXw2BihsXcX/bm4uujhFN4VwPi/Dh49PLl5UxYlWopQgq8Lm589ulFTFinEPoKo8tL/jXZ+urEUvFeiaSmsjvwYHvB39bBSM2ImZCWHC0QS5AV8GI8aLMTP6n526gq2rpG7El2PXSNPtdAPgV9nragNUrjt8TwKV2iQQoradsRF2P+5tyfTm/1pm0hJAuYDVmnkDUrZAAUzWiHq1jomqHfCmfJqDg5ClU1yTE9d//O4CZzCeiFVMCrOrJuqinRwJiSkZcDGAm8+1NEHEtDcDWeXJpAtWfQcQ0migzoUqGpH+CaxXLb6IaidSiNDUDa07LNqKeRDfBkn2Cx5vlGrHait0PhgqPN0s1ovlxMUEU1T/YItfyjKgv9gj6wjx1aZ1wtRVjbCiob2/gmLqk+rvxdVFZkKRf4PXYpRixGvVuIrJgMy7eiHrj6zJCDKqmfxoXbsSWHuECNAH9KU0ZF9wJVxtvk5rHCOvbNHEsciYFHDT5VpBf9ie3Ul1cE6Wb54svYtj65ZMbchbFl0vnAKJqfjKshRhRNw+XnSFoan56YyUea6rm4SrYb6bmt78irYRS1Wp8XV6Jxqn3/Is1Yaqa5ts04ydV27dJQOpm42yV3BOV/VlgRYpivY+fl1+fCcl2LBlp10ZvAeutOt5Uf7w/F6Sstkzz6u14me1RXP06fvuxwbOZpwO4hnn2fvxjGA9V/o/Pb88OTQc0SArIgN0a5tXX99srGTf5ZX8Zf35/cXtebcCqnt9evP08/vIj+SWHbPtXV/8xrGc961nPetazVl//B3HDgB4Pr/wbAAAAAElFTkSuQmCC" /> </p>
                    <a href="/interesses" class="master-header-ad-button">Anuncie conosco, clique
                        aqui</a>
                </div>
            </div>
        </div>
        <!--		end master header ad-->

        <!--		header-->

        <div class="large-10 large-offset-1 header-site columns box-content">
            <div class="row header-site-top">
                <div class="large-4 columns">
                <a href="/" class="display:block">
                    <?= $this->Html->image('logo_header.png', ['class' => 'header-site-logo']); ?>
                    </a>
                </div>
                <div class="large-8 columns">
                    <div class="row form-search">
                        <?= $this->Form->create(null, [
                            'url' => [
                                'controller' => 'Clientes',
                                'action' => 'index'
                            ]
                        ]); ?>
                        <div class="large-12 columns">
                            <div class="row collapse">
                                <div class="small-9 columns">
                                    <?= $this->Form->input('q', [
                                        'placeholder' => 'Busque : O que você procura ? Academia ? ',
                                        'label' => false,
                                        'div' => false,
                                        'templates' => ['inputContainer' => '{{content}}']
                                    ]); ?>
                                </div>
                                <div class="small-3 columns">
                                    <?= $this->Form->button('Buscar', [
                                        'type' => 'submit',
                                        'class' => 'button postfix button-search',
                                        'templates' => ['inputContainer' => '{{content}}'],
                                        ['escape' => false]
                                    ]); ?>
                                </div>
                            </div>

                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns ">
                    <nav class="top-bar expanded" data-topbar role="navigation">
                        <section class="top-bar-section">
                            <ul>

                                <!-- <li><?= $this->Html->link(' Página Inicial', '/'                                    ) ?></li> -->

                                <li><?= $this->Html->link(__('Noticias'), ['controller' => 'noticias'], ['class' => 'section-notice-color']) ?></li>
                                <li><?= $this->Html->link(__('Anuncie'), ['controller' => 'pages', 'action' => 'anuncie'] , ['class' => 'section-ads-color']) ?></li>
                                <li><?= $this->Html->link(__('Classificados'), ['controller' => 'classificados', 'action' => 'index'], ['class' => 'section-class-color']) ?></li>
                                
                                <li><?= $this->Html->link(__('Blog'), 'http://blog.aquitempe.com' , ['class' => 'section-fun-color']) ?></li>
                                <!-- <li><?= $this->Html->link(__('Vídeos'), ['controller' => 'videos'] , ['class' => 'section-fun-color']) ?></li> -->
                                      <li class="pull-right"><?= $this->Html->link(__('Fale Conosco'), ['controller' => 'pages', 'action' => 'contato' ] , ['class' => 'menu-clean']) ?></li>
                                <li class="pull-right"><?= $this->Html->link(__('Serviços'), ['controller' => 'pages', 'action' => 'servicos' ] , ['class' => 'menu-clean']) ?></li>
                                <li class="pull-right"><?= $this->Html->link(__('Planos'), ['controller' => 'pages', 'action' => 'planos' ] , ['class' => 'menu-clean']) ?></li>
                                <li class="pull-right"><?= $this->Html->link(__('Descontos'), ['controller' => 'pages', 'action' => 'descontos' ] , ['class' => 'menu-clean']) ?></li>
                          
                            </ul>
                        </section>
                    </nav>
                </div>


            </div>
            <div class="row header-site-menu">

            </div>
        </div>
        <!--		end header-->

    </div>
    <div class="row">
        <div class="large-10 large-offset-1 columns">
            <div class="row">
                <div class="large-3 columns" style="padding-left:0px;">


                    <div class="">

                        <div class="row content-site">
                            <div class="large-12">
                                <h2 class="titulo-sessao">Canais</h2>
                            </div>


                            <ul class="lista-canais">
                                <li><span>Categorias</span>
                                    <div class="sidebar">
                                        <ul class="sidebar-list">
                                            <?php foreach ($lista_categorias as $categoria) : ?>
                                                <li class="sidebar-list-item">
                                                    <?php if (count($categoria['children']) == 0) : ?>
                                                        <?= $this->Html->link($categoria['nome'], '/cat/' . $categoria['slug']); ?>
                                                    <?php else : ?>
                                                        <span><?= $categoria['nome'] ?></span>
                                                        <ul class="sub-sidebar-list">
                                                            <?php foreach ($categoria['children'] as $cat) : ?>
                                                                <li class="sub-sidebar-list-item"><?= $this->Html->link($cat['nome'], '/cat/' . $cat['slug']); ?> </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>

                                </li>
                                <li><?= $this->Html->link('Classificados', ['controller'=>'classificados']); ?></li>
                                <li><?= $this->Html->link('Descontos', '#'); ?></li>
                                <li><?= $this->Html->link('Diversão', '#'); ?></li>
                                <li><?= $this->Html->link('Videos', ['controller' => 'videos']); ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="">

                        <div class="row content-site">
                            <div class="large-12">
                                <h2 class="titulo-sessao">Onde nos encontrar</h2>
                            </div>

                            <ul class="lista-como-nos-encontrar">
                                <li><?= $this->Html->image('ico_fb.png'); ?><a href="https://www.facebook.com/AquiTemPecom/?ref=bookmarks">facebook</a>
                                </li>
                                <li><?= $this->Html->image('ico_insta.png'); ?>@aquitempe</li>
                                <li><?= $this->Html->image('ico_whats.png'); ?>99279 9039</li>
                                <li><?= $this->Html->image('ico_fone.png'); ?>3318 0946</li>
                                <li><?= $this->Html->image('ico_fone.png'); ?>98847 3797</li>
                                <li><?= $this->Html->image('ico_mail.png'); ?>contato@aquitempe.com</li>
                            </ul>
                        </div>
                    </div>

                    <div class="">
                        <div class="row content-site">
                            <div class="large-12">
                                <h2 class="titulo-sessao">Mapas</h2>
                            </div>
                            <div class="text-center">
                                <?= $this->Html->image('mapa.png', ['style' => 'width:80%; margin: 20px']); ?>
                            </div>
                        </div>


<div class="row content-site">
                                <div class="large-12">
                                </div>
                                <div class="text-center">

<!-- Sidebar -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7736371220220774"
     data-ad-slot="1621148849"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>





</div></div>
                        <?php foreach ($anuncio_lado as $anuncio): ?>



                       <!--  <div class="row content-site">
                                <div class="large-12">
                                </div>
                                <div class="text-center">
                                 <a href="<?= $anuncio->link; ?>">
                                    <?= $this->Html->image( REPOSITORY_IMG .  $anuncio->img, ['style' => 'width:80%; margin: 20px']); ?>
                                    </a>
                                </div>
                            </div>  -->
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="large-9 columns">
                    <div class="row">

                        <?= $this->Flash->render() ?>
                        <?= $this->fetch('content') ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>

</section>
<footer class="footer-site text-center">
    <div u="slides"></div>
    <p>
        Av. Getúlio Vargas, nº 808 sala 810 - Olinda - Pernambuco | 81 98449.1600 - 99308.0331 <img width="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABF1BMVEX///9Vxzwtbh9Dny/+/v7c3NxCnC5NxTFSxjji3+NIxCpPxjREojBLxS5GxCfi4OIqbRtLsTU0mxklahQrah4XZgA9nSc8nSVUxTtPujjj4+Pj9OAAYADv+e0haQ5GpjGR2IRiy0xozFOs4aOA03DL7MWc3JG3zLTU2dP4/Pc1gCXX8NOJ1nt30WZVpUWl35vI08bB6Lqiw53d7Nrs8us4dCs6jCmz46onlgCLqYaewZjR7sysx6horFuRvIo3hSZXhU9NgUPR3c98m3eyv69AeDV1sWqLpYeatJZRpEBPgEZqk2OEvHqtwqljqlWdsZne5txpk2G6xbiizJq82rdzlW1CiDQ7rxvQ5MyHuH+z1K2ayJJ8s3JJ9pJxAAASY0lEQVR4nO2dDVfaSBfHgYAJIEkaGuKWl4ICkkUU3XVprbq6vrW11dZud9d9/P6f45kkQGYmM5OZJBC6x/85e7aniuXHvXPf5oKZzLOe9axnPetZz3pWMrKbw97+ZGuv3wbq721N9nvDetNO+2klonpva2czW1HVUqUiy2VXslyplEqqqnRG7a39YTPt5xhZ9f32JiCTy4qSJUtRyjKAV7p7vR8Osz7pZtVKmYaGg8qlUqe9/8NQ2r02P52PWa6onf4w7ScfruZkVCqJ0s0pZVXe6eXTZmDI3h+plYh0M5VLcvtd2iAUDXdi400h1ezW6p3J/KSjlpPAcwXctbtaR7LZT8h8vspqZz9trLnqO6qcLJ4rpZSdpI3mCvAl554YY6WSPuMC+VxVsun6arO9WL6s46udFJPH1kLOX4BRHdXT4XvXqSyBz1FZ3UuBz+6qCecHlirZpbtqT170AUSlqDtLrVeXa0BPsrxEM75TlmtAT4raXhZgf/kG9CRvLCWo2pvLCqFBKerW4gGH5TQ8dC61u+iAM0nLQ2eSO4ttHftqunxZpxxfZOc4Su8I+lLUhRXj9uYyylAOLaqIa2ZTjTGwFpMZm9mUYwysUjd5wLq8QoCgFB8lDhh1yrsoyQkjrhxgNvv3dZKAzdUD/GnNOEkO0F6lIOPppzVJMhKzYr6zMmliJgcQID4mRDhaUUCA+C0RwJ1kSzVFcS66wX/Uq+FQzQAl6c2fCQBuJVRsK4pzo10qb47a7X6/394ZdeSSWhK+SoUBgRV/iQ3YSwLQueDNjvr7Q2z/wq73JjsbghfGMKBUk+JudNRL8fGc6+vZJX0elft3zf12VuUumRBASbLi5oxOzDwB8EYeXZ4q95Xc6/AN0DFA4Kf/iwXYjdcvldXNSZNJB1HW+0op9PUMAIJo808MwEmsQyir7ToX3gwyv98JGZIQAIEVox/FehxAWd6z+fFmkL1NFiMRUKrdRSaMcQjLal+Yb8rYofoqGTBG4m9HPoSKutOMwucx7pfJ/zANEBzFaFnxXWQfLXWGUflcRrtNclU6oFT7EAXQjlpVKeqEwjf70XnbngcH8jfWg3eTDMCIKWMnoo9WOnXS03ZftOFkrz3a7GSdmrSz2e1v9epkykxmDzMjEzCSnw4j+qgzBiPhuduKpYqzi+k+dfA/d/ey3J0QUwp2fRACGCWebkbyUWdaG8TLs7YVnZLV3UkMnEboCigMEPipaJcxiVSPKuWAh7q1isourRVZ3dgKBF8QzGd+FA4IClSxvG9HAix3moFnORxxFdUAsh1knNZUPICSJRZs+lHCTHnTzuB8zBIFe7iTRDHEfZUXEASbXwUAI5Vr5c08Blgfid3FuYUQ+iPeqQonoFQTGUx1I0xmcMBMvi++MCWXe6irZoZ/cwIK9ftRMkW5g778mZ4SxdMVtYv8nEz+2OAlrPE3wyPxTKFk0SeWIRZePCorwwwEWCjyI3Kn/QgmVEpIkMg0Y2yEOesIPmChUDzgReQ2YgQTqkMEcBjerDN/Wtc70i4gQLyorfMhcp7ECIEU1NpojI853ZHdxDoFLBS0szU+RIsvnO6IR8A2ApjAhNUpjuaAQLnf+BDf8CxqNIWfn9JBguheIhPW0hAC1B4afIhchc2ecJBX4WI0GUCA+Pe4CCHemL/zIXIsFAnfpJXgQxhzPOfrxZo11iDEc/1nHkSOFqMnWnMrmzBgIpcALqBUWx9AhGPz5c8chLXdUELhVAH7aLz5IwroPNsC7KetnM5jxLCEIRxnKn04jm4EXh+nlxfOHS+8WtS6ho5i4VDP6eE1amis2RKsRRQFDqOBS4ByaWM06mQVsZ/6YgZiHPmI2qWZy+U4yvAQQtEhMDy18Lo55Ktdb+U1H/gKFyBAhKINCDYOYli8MdjXGKLnCA4zQQ/3x4oiOQgCBEcRMuKDY8SXYYnR+sR2UsFkqPYgQvy+H7Yv/1jkBeKIiJ+e6VyISTqp0mH4aGkPTiO8teAL7KQZA8yIuZchuZ/ppqJOWoJnh9g8rTxCilXOOwIcUKpB8VS70l1Edu5nRtOJWMxTspAJ97DHquhYia9WCgAiwcYLp6GIrGjaFXNSed6q5jM2doIre9hMiSfWEABBWwslRdeGQMzcz2owBCs2yEwBgLKdRwk5TgAJEDXiRcsjfMnK/ca/VEDB8UW5C5kJe2i5j5oQIIbeE5ABESOOzdxM9MTIGGYINk4lP1UETjA61vBibcghpwA6RsRjTRgilVCw6q74jhhIM6XgbZLNJqQCStbj3IhO/T0TPTFSq29bbHwBOWnAv5VRkJCdEumAkrTuh5oH303piNSDKJgNK34yzLSxJ19uEwhZKZEFKBnHfqw51CFESu6v0Qq3sIOCCY6k+COhPAIh0u/YmIBw1te+V3MQIiUx0q71cUOwBVVsmXd4miETUiMZGxCcRKyHghCJ3/+Gcpcodu0LOWKmj782ZMLAC8EJKBnbczcdIISU3E8rTaMfw0DBLgfSYZ6eL0IBkWgK5QsPkfBoSqgRDDT+fIYw+ijvkAjJThIOKEm7GvkgUhKjRV4AF5yy+dmQ8Ei4rZp/G3k3gAcQ6qG0+xZOGESkbGaINRZQX0G6E1ftACDZR7gAoYOIhRpaYiQSBsIFmxDK94RaqPQOv5In71jxAUqW3+rjoYaYGMkLmWI1G1RaZwgPxFN+xiaODzgBpdqTX30TCAOJkVy3iU0w/HyQaRLcG5kyOtokeQgvIDyR0giAgcRITBe2YNk9TxbkA4YMOEBZRzrk3IBAfjA904mMSGIkpgvBabd/0MiJHLnOCM7hRAFrfjANpotZSPW/nTirEU2H8waQksiRDpF0YyMCCKcLKiGUNYhDU1pJRSP0Ez45zaANVDCOCQFCXfB8kkE4jPOsQdweEkz4ECFljAxPi4OHVQwQJryhEvqIxJQv2jv5hJSWQdlAjNhHfr4gICehnxhJ/ZPgQD+cMFtBGgwkG4kC8hLOEyNptC84huIgxC74IT99LQrITThDNAiEgguX4ecwMNifX/GLA/JFGk9u7n9DIBTr8OFsQS/ZsU2i6XJ8BEA4W1xQs8VUTseYACGU8RkxClt3c0u3KIBcGd/XWhKElR67pvGEVTZOfxEJUJL8Sc1ZOGFuLQnCCbsunaqExtN6JSLgLn2MQVQChLJ/t0TqLebCtha/rNUIz98wLDYgPE8MCTSeGvEJoUkM82pQUaDV2syrwmA3CFPbHj9aTEbrlNkB8xGKtfjwCWP3zlDKAIBAd/hCrDHWtOLglGVHy596P0QnFFxSKNPvDlHNd4o8wELxEUU0jl37FAundDtC6fA+MqHwGsY8EYQV7dOdjCkgYNmWIBRjPgstDh5pdoSG3uySZiaTQChYeUO3h6HNsxtt5oDgWRYejVnAQTa7ioMngxSJoEtSrmSR0w8TIISCadh9gFKqZyBAB2V856FYJ1oB/cIJgREKNIUcT7KonhEIBTtguMMNrdoVpYkAAlMUtx1Ga7eAy/lC4Bj69xZjrmNY/U4gFF6cVP0kELoAoLzYLmIkLuPdoBCQVjwKnEZ/DsUXaFpvCYTCe5dQLmfcDHp6LVljHBGgDAJ/55nxBHVUaFrKU5UCme8JhIJ33OhBDEk1TqlmBKxIVfERNSLkpAWeUwgItwmEwouX8BUp28W9WnSa93gIr7Fg4zspX77PmX+QCOPsPzOj6azYNk45EYu7CCEUSUPb36kaxPciCpZtqJvu0yPxa9/bTigHDxfmpNB64iGfl7aINzOCa3tODoBqauoyN9wu1SweT9W2kXQBxxlOJ9WvSIDCCRG4qX+FRn19sH7QuA43Y/EJcVJ4JYovkuaqt0RC8bcDIYtt5BYq0PBateOixsADGqAmhMs6vlOYa5GSBZD4WwbhlRriSSR19MbuNpMRi6TwKeRL9yCUjsmE4u87hFcuSNmGPLKoGXfHRaqvFo8QE0JrGNxxJtf4QiaM8BZ11W/gCaUbdSZTMz4cFCj1DP6OUf9LpCt8ig3JgOJvecKMuIc9njl0MmqP46CzFrUnYnvsEZ5zmrD6lUIoHmrQgSjm5mFTNcu4OxggkEXteA1NhXD3yG9CWqDJZILvWgoVvJGQGcJG5Bgb1izr5LhQdCg1rVgcHH0I9E0QP/cppAaaKO+PRUs32Alek5AIkIZxcrQ9Ho+PT++CIwzkTUG8gRQQUj8ERLTNx4wI1wycgJ4lDUdWsLWH3zBTGHDmQlDRnNMAIx1EKNb4PrDB+e5ytixoiaag3XIT0o9hpI8vg/pge/YCJQR4BwNyhxla6+RJPCPC620zJ08GsIaOcLgGUFNCxrudxYtvuIOaJouNJPik2geYT/vKV3I7opTdnkLeMEAQvGVaShDQQgH54yhtgjGTaL6AJxle/5QQ4B0CyNkWTgmZnzUkWrgFnDQZQOMarecEDiF5GBzdTSEndSNpQoCPaFX+kf8QhjmpqJvCTupE0kSiaA0byglkQpcw5FPNxD4OEn4DInBSBJB0w8Ij6wM6O9YuRA4heZ6PSOhjMdBICgFaxofrXe5PP0IMeKppMQAZVfdMIqtR8D3wfmUOCPBOx6BXOJZCbueDMnax4b8ooF4NAxSqTVEn9Wzg4rkNUUFj3eoS+dbwYaMoILMmnUlgWgN1Tra67uLtOtabP8GQ23lENaN2VMCa/lffBQEpw25U/LEGiaQGwLs7GuMTpmIBtLUcQadm7B7gj9UKZ6KAzIptLu5OvwJ96pjkjiRIw6Witn1Nav4QPOspOGHUBldCacI1IaOt8MXdB0OR9MvRgD4D1YqDgxOLTFmzwMF9Oi4EH61dtgQqGU+M3heWzftr7eeRNPMKP0BByML26Ymz+2RZNU/gT4ZR2308AAeX8GjtRtRDczypwhPnZ9TMnTTzKvgESZBFbbB9cPp0fX1ycnL99Hh0sD32xlCEbx58jABIuZAJKs9HOHNSPsDpMwecc2k0w2ugWxIpRecmDClJBY04c1IKYIjbMl+GcRQDCpiQ8yROnZQIqGnjy6iM2uC7KRxiPBNynkJHPJelnpMSAAHezblpVu9fRWDUBhemcI6YmpAvkE4VnhO9dI8Dapr2cHNounHe1O8HYoxaDD7eXDhT+AfLuTUpCujgXUzx3Be11boYUMMJge/yNjof/TqGotDq1HFSGBCQPFzoJpalq+bZPQ+k5rh2LlIAnZuQcmdIU9gOmBNJfUDAcPldJz5B3fQg6ZTO1x5urvAXR1DEPS+mQobDwElngOD0XN62GK+/3jKvLi4dSozT/ZvB5c2ZGRPP+TeEfxVLnv1ZgMBJX03x7s9YeHNIUz+7uL98GHgmBxqML+9vbnPx6RzxJ3tfzGADIumrKZ7JfXpA5DGB9MOrq8PDqvNHs1VNgC4XOkKkiPWx5fJW0Ut7UV5/HSgRLl/sKTBNNsOI8iB+bEhQ5ucogEw/Xf9rdfCi+qgj2i9i2ViX1l6mjeVLb0XyUVfk4akzNlz/OW0uXw2BihsXcX/bm4uujhFN4VwPi/Dh49PLl5UxYlWopQgq8Lm589ulFTFinEPoKo8tL/jXZ+urEUvFeiaSmsjvwYHvB39bBSM2ImZCWHC0QS5AV8GI8aLMTP6n526gq2rpG7El2PXSNPtdAPgV9nragNUrjt8TwKV2iQQoradsRF2P+5tyfTm/1pm0hJAuYDVmnkDUrZAAUzWiHq1jomqHfCmfJqDg5ClU1yTE9d//O4CZzCeiFVMCrOrJuqinRwJiSkZcDGAm8+1NEHEtDcDWeXJpAtWfQcQ0migzoUqGpH+CaxXLb6IaidSiNDUDa07LNqKeRDfBkn2Cx5vlGrHait0PhgqPN0s1ovlxMUEU1T/YItfyjKgv9gj6wjx1aZ1wtRVjbCiob2/gmLqk+rvxdVFZkKRf4PXYpRixGvVuIrJgMy7eiHrj6zJCDKqmfxoXbsSWHuECNAH9KU0ZF9wJVxtvk5rHCOvbNHEsciYFHDT5VpBf9ie3Ul1cE6Wb54svYtj65ZMbchbFl0vnAKJqfjKshRhRNw+XnSFoan56YyUea6rm4SrYb6bmt78irYRS1Wp8XV6Jxqn3/Is1Yaqa5ts04ydV27dJQOpm42yV3BOV/VlgRYpivY+fl1+fCcl2LBlp10ZvAeutOt5Uf7w/F6Sstkzz6u14me1RXP06fvuxwbOZpwO4hnn2fvxjGA9V/o/Pb88OTQc0SArIgN0a5tXX99srGTf5ZX8Zf35/cXtebcCqnt9evP08/vIj+SWHbPtXV/8xrGc961nPetazVl//B3HDgB4Pr/wbAAAAAElFTkSuQmCC" />  | contato@aquitempe.com</p>
</footer>





<?= $this->Html->script('jquery-1.10.1.min'); ?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<?= $this->Html->script('g-maps'); ?>
<?= $this->Html->script('jquery.mousewheel-3.0.6.pack'); ?>
<?= $this->Html->script('jquery.fancybox.pack'); ?>
<?= $this->Html->script('helpers/jquery.fancybox-buttons'); ?>
<?= $this->Html->script('helpers/jquery.fancybox-media'); ?>
<?= $this->Html->script('helpers/jquery.fancybox-thumbs'); ?>
<?= $this->Html->script('jssor.slider.min'); ?>
<?= $this->Html->script('application'); ?>


</body>
</html>
