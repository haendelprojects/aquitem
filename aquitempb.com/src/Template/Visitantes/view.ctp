<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Visitante'), ['action' => 'edit', $visitante->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Visitante'), ['action' => 'delete', $visitante->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitante->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Visitantes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitante'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="visitantes view large-9 medium-8 columns content">
    <h3><?= h($visitante->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($visitante->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($visitante->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Telefone') ?></th>
            <td><?= h($visitante->telefone) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $visitante->has('user') ? $this->Html->link($visitante->user->id, ['controller' => 'Users', 'action' => 'view', $visitante->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($visitante->id) ?></td>
        </tr>
    </table>
</div>
