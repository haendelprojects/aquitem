<nav class="large-2 medium-2 columns" id="actions-sidebar">
	<ul class="side-nav">
		<li class="heading"><?= __( 'Ações' ) ?></li>
		<li><?= $this->Html->link( __( 'Novo Usuário' ), [ 'action' => 'add' ] ) ?></li>
	</ul>
</nav>
<div class="users index large-8 medium-8 columns content">
	<h3><?= __( 'Usuários' ) ?></h3>
	<table cellpadding="0" cellspacing="0">
		<thead>
		<tr>
			<th><?= $this->Paginator->sort( 'id' ) ?></th>
			<th><?= $this->Paginator->sort( 'nome' ) ?></th>
			<th><?= $this->Paginator->sort( 'username' ) ?></th>
			<th><?= $this->Paginator->sort( 'ativo' ) ?></th>
			<th class="actions"><?= __( 'Ações' ) ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ( $users as $user ): ?>
			<tr>
				<td><?= $this->Number->format( $user->id ) ?></td>
				<td><?= h( $user->nome ) ?></td>
				<td><?= h( $user->username ) ?></td>
				<td><?= h( $user->ativo ) ?></td>
				<td class="actions">
					<?= $this->Html->link( __( 'Detalhes' ), [ 'action' => 'view', $user->id ] ) ?>
					<?= $this->Html->link( __( 'Editar' ), [ 'action' => 'edit', $user->id ] ) ?>
					<?= $this->Form->postLink( __( 'Delete' ), [
						'action' => 'delete',
						$user->id
					], [ 'confirm' => __( 'Are you sure you want to delete # {0}?', $user->id ) ] ) ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paginator">
		<ul class="pagination">
			<?= $this->Paginator->prev( '< ' . __( 'previous' ) ) ?>
			<?= $this->Paginator->numbers() ?>
			<?= $this->Paginator->next( __( 'next' ) . ' >' ) ?>
		</ul>
		<p><?= $this->Paginator->counter() ?></p>
	</div>
</div>
