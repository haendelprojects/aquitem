<nav class="large-2 medium-2 columns" id="actions-sidebar">
	<ul class="side-nav">
		<li class="heading"><?= __( 'Ações' ) ?></li>
		<li><?= $this->Html->link( __( 'Lista de Usuários' ), [ 'action' => 'index' ] ) ?></li>
		<li><?= $this->Html->link( __( 'Voltar' ), 'javascript:history.back()' ) ?></li>
	</ul>
</nav>
<div class="users form large-8 medium-8 columns content">
	<?= $this->Form->create( $user ) ?>
	<fieldset>
		<legend><?= __( 'Dados' ) ?></legend>
		<?php
		echo $this->Form->input( 'nome' );
		echo $this->Form->input( 'username' );
		echo $this->Form->input( 'password' );
		echo $this->Form->input( 'ativo' , ['type'=> 'checkbox']);
		echo $this->Form->input( 'type' , [ 'options' => ['ADMIN' => 'ADMIN' , 'VISITANTE' => 'VISITANTE']]);
		?>
	</fieldset>
	<?= $this->Form->button( __( 'Submit' ) ) ?>
	<?= $this->Form->end() ?>
</div>
