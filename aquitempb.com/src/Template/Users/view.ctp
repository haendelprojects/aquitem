<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Visitantes'), ['controller' => 'Visitantes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitante'), ['controller' => 'Visitantes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($user->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Ativo') ?></th>
            <td><?= h($user->ativo) ?></td>
        </tr>
        <tr>
            <th><?= __('Type') ?></th>
            <td><?= h($user->type) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Visitantes') ?></h4>
        <?php if (!empty($user->visitantes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Telefone') ?></th>
                <th><?= __('User Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->visitantes as $visitantes): ?>
            <tr>
                <td><?= h($visitantes->id) ?></td>
                <td><?= h($visitantes->nome) ?></td>
                <td><?= h($visitantes->email) ?></td>
                <td><?= h($visitantes->telefone) ?></td>
                <td><?= h($visitantes->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Visitantes', 'action' => 'view', $visitantes->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Visitantes', 'action' => 'edit', $visitantes->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Visitantes', 'action' => 'delete', $visitantes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitantes->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
