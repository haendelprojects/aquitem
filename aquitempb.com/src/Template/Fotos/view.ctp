<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Foto'), ['action' => 'edit', $foto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Foto'), ['action' => 'delete', $foto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $foto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fotos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Foto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fotos view large-9 medium-8 columns content">
    <h3><?= h($foto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('File') ?></th>
            <td><?= h($foto->file) ?></td>
        </tr>
        <tr>
            <th><?= __('Cliente') ?></th>
            <td><?= $foto->has('cliente') ? $this->Html->link($foto->cliente->id, ['controller' => 'Clientes', 'action' => 'view', $foto->cliente->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($foto->id) ?></td>
        </tr>
    </table>
</div>
