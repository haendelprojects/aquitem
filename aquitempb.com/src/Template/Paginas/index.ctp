<nav class="large-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova Pagina'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="paginas index large-8 medium-8 columns content">
    <h3><?= __('Paginas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('alias') ?></th>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th class="actions"><?= __('Ações') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($paginas as $pagina): ?>
            <tr>
                <td><?= h($pagina->nome) ?></td>
                <td><?= h($pagina->alias) ?></td>
                <td><?= $this->Number->format($pagina->id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $pagina->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $pagina->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pagina->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pagina->id)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
