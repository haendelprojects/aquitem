<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Interesses Controller
 *
 * @property \App\Model\Table\InteressesTable $Interesses
 */
class InteressesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
         $interess = $this->Interesses->newEntity();
        if ($this->request->is('post')) {
            $interess = $this->Interesses->patchEntity($interess, $this->request->data);
            if ($this->Interesses->save($interess)) {
                $this->Flash->success(__('Entraremos em contato embreve.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente.'));
            }
        }
        $this->set(compact('interess'));
        $this->set('_serialize', ['interess']);
    }

   
}
