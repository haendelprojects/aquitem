<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Classificados Controller
 *
 * @property \App\Model\Table\ClassificadosTable $Classificados
 */
class ClassificadosController extends AppController
{

    public $paginate = [
        'order' => [
            'id' => 'desc'
        ],
        'contain' => [
            'Users' , 'Fotos'
        ]
    ];
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('classificados', $this->paginate($this->Classificados));
        $this->set('_serialize', ['classificados']);
    }

    /**
     * View method
     *
     * @param string|null $id Classificado id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classificado = $this->Classificados->get($id, [
            'contain' => ['Fotos']
        ]);
        $this->set('classificado', $classificado);
        $this->set('_serialize', ['classificado']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classificado = $this->Classificados->newEntity();
        if ($this->request->is('post')) {
            $classificado = $this->Classificados->patchEntity($classificado, $this->request->data);
            if ($this->Classificados->save($classificado)) {
                $this->Flash->success(__('The classificado has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The classificado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('classificado'));
        $this->set('_serialize', ['classificado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Classificado id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classificado = $this->Classificados->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $classificado = $this->Classificados->patchEntity($classificado, $this->request->data);
            if ($this->Classificados->save($classificado)) {
                $this->Flash->success(__('The classificado has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The classificado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('classificado'));
        $this->set('_serialize', ['classificado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Classificado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classificado = $this->Classificados->get($id);
        if ($this->Classificados->delete($classificado)) {
            $this->Flash->success(__('The classificado has been deleted.'));
        } else {
            $this->Flash->error(__('The classificado could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
