<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Inflector;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);

        $this->Auth->allow(['*']);
    }

    private function alter_img()
    {
        set_time_limit(1500);
        $clientes = $this->Clientes->find('all')
            ->select(['id', 'logo'])
            ->where(['id >' => '8348']);
        //  ->limit(600)->toArray();


        foreach ($clientes as $client) {
            $client = $client->toArray();

            $client['logo'] = 'http://img.aquitempe.com/clientes/' . $client['id'] . '/marca.jpg';
            $file_headers = @get_headers($client['logo']);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $client['logo'] = 'http://img.aquitempe.com/clientes/' . $client['id'] . '/marca.png';
                $file_headers = @get_headers($client['logo']);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $client['logo'] = 'http://img.aquitempe.com/clientes/' . $client['id'] . '/marca.gif';
                    $file_headers = @get_headers($client['logo']);
                    if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $client['logo'] = '';
                    }

                }

            }
            $cat = $this->Clientes->get($client['id'], [
                'fields' => ['id', 'logo']
            ]);
            $cat = $this->Clientes->patchEntity($cat, $client);
            debug($cat);
            $this->Clientes->save($cat);
        }

        exit();
    }

    private function alter_slug()
    {
        set_time_limit(1500);
        $clientes = $this->Clientes->find('all')
            ->select(['id', 'nome', 'slug'])->toArray();
        //  ->limit(600)->toArray();


        foreach ($clientes as $client) {
            $client = $client->toArray();

            $client['slug'] = strtolower(Inflector::slug(strip_tags($client['nome'])));

            $cat = $this->Clientes->get($client['id'], [
                'fields' => ['id', 'slug']
            ]);
            $cat = $this->Clientes->patchEntity($cat, $client);
            debug($cat);
            $this->Clientes->save($cat);
        }

        exit();
    }

    private function alter_telefones()
    {
        set_time_limit(1500);


        for ($i = 1; $i <= 9296; $i++) {
            $client = $this->Clientes->find('all')->where(['id' => $i])->first();

            if ($client) {

            } else {
                $this->request->data('id', $i);
                $this->request->data('nome', 'Cliente' . $i);
                $this->request->data('ativo', 0);
                $this->request->data('data_cadastro', date("Y-m-d"));
                $this->request->data('plano_id', 1);
                $this->request->data('categoria_id', 1);


                // debug($this->request->data);
                $cli = $this->Clientes->newEntity($this->request->data,
                    [
                        'accessibleFields' => ['id' => true],
                    ]);
                //$cli->dirty('id', true);
                //$cli = $this->Clientes->patchEntity($cli, $this->request->data);

                // debug($cli->errors());
                $this->Clientes->save($cli);

                debug($cli);
            }
        }
        exit();
    }

    public function categoria($slug = null)
    {
        $query = $this->Clientes
            ->find('all')
            ->where(['Categorias.slug' => $slug , 'Clientes.ativo' => 1])
            ->order([ 'Planos.id' => 'DESC' , 'Clientes.nome' => 'DESC' ,])
            ->contain(['Planos','Categorias', 'Telefones']);

        $cat = $this->Clientes->Categorias->find('all')->where(['Categorias.slug' => $slug])->first();
        $this->set('keywords' , $cat->palavras_chaves);
        $this->set('clientes', $this->paginate($query));
        $this->set('_serialize', ['clientes']);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {

        $query = $this->Clientes
            ->find('search', $this->Clientes->filterParams($this->request->query))
            ->where([ 'Clientes.ativo' => 1])
            ->contain(['Planos', 'Categorias', 'Telefones'])
            ->order([ 'Planos.id' => 'DESC' , 'Clientes.nome' => 'DESC']);
        $this->set('clientes', $this->paginate($query));
        $this->set('_serialize', ['clientes']);


    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     *
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($slug = null)
    {
        $cliente = $this->Clientes->find('all')
            ->where(['Clientes.slug' => $slug ,  'Clientes.ativo' => 1])
            ->contain([
                'Planos', 'Categorias', 'Fotos', 'Telefones'
            ])->first();
        $this->set('cliente', $cliente);
        $this->set('keywords' , $cliente->categoria->palavras_chaves);
        $this->set('_serialize', ['cliente']);
    }


}
