<?php
/**
 * Created by Citrus Tecnologia.
 * User: Erick
 * Date: 07/01/2016
 * Time: 13:45
 */

namespace App\Controller;

use App\Controller\AppController;


/**
 * Dashboard Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class DashboardController extends AppController {
	public function initialize() {
		parent::initialize();

		$this->loadModel( 'Clientes' );
		$this->loadModel( 'Videos' );
		$this->loadModel( 'Categorias' );

	}

	public function index() {
		$clientes_count         = $this->Clientes->find( 'all' )->count();
		$clientes_count_ativo   = $this->Clientes->find( 'all' )->where( [ 'Clientes.status' => 1 ] )->count();
		$clientes_count_inativos = $this->Clientes->find( 'all' )->where( [ 'Clientes.status' => 0 ] )->count();
		$clientes_count_vencidos = $this->Clientes->find( 'all' )->where( [ 'Clientes.data_cadastro < ' => date( 'Y-m-d', strtotime( '-1 year' ) ) ] )->count();
		$clientes_count_validos = $this->Clientes->find( 'all' )->where( [ 'Clientes.data_cadastro > ' => date( 'Y-m-d', strtotime( '-1 year' ) ) ] )->count();

		$categorias_count = $this->Categorias->find( 'all' )->count();

		$this->set( compact( 'clientes_count', 'categorias_count', 'clientes_count_ativo', 'clientes_count_inativos' , 'clientes_count_vencidos' ,'clientes_count_validos') );
	}
}