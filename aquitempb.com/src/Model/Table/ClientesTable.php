<?php
namespace App\Model\Table;

use App\Model\Entity\Cliente;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Planos
 * @property \Cake\ORM\Association\BelongsTo $Categorias
 * @property \Cake\ORM\Association\HasMany $Fotos
 */
class ClientesTable extends Table {

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 *
	 * @return void
	 */
	public function initialize( array $config ) {
		parent::initialize( $config );

		$this->table( 'clientes' );
		$this->displayField( 'id' );
		$this->primaryKey( 'id' );

		$this->belongsTo( 'Planos', [
			'foreignKey' => 'plano_id',
			'joinType'   => 'INNER'
		] );
		$this->belongsTo( 'Categorias', [
			'foreignKey' => 'categoria_id',
			'joinType'   => 'INNER'
		] );

		$this->belongsTo( 'Users', [
			'foreignKey' => 'cadastrado_por',
			'joinType'   => 'INNER'
		] );
		$this->hasMany( 'Fotos', [
			'foreignKey' => 'cliente_id'
		] );

		$this->hasMany( 'Telefones', [
			'foreignKey' => 'cliente_id'
		] );


		$this->addBehavior( 'Xety/Cake3Upload.Upload', [
				'fields' => [
					'logo'       => [
						'path'        => 'upload/cliente/logo/:md5',
						'defaultFile' => 'default_logo.png',
						'prefix'      => '../'
					],
					'img_header' => [
						'path'        => 'upload/cliente/img_header/:md5',
						'defaultFile' => 'default_img_header.png',
						'prefix'      => '../'
					]
				]
			]
		);

		$this->addBehavior( 'Search.Search' );
		$this->_searchConfiguration();

	}

	private function _searchConfiguration() {

		$this->searchManager()
		     ->add( 'q', 'Search.Callback', [
			     'callback' => function ( $query, $args, $manager ) {
				     //$args = explode( " ", $args['q'] );
				     $args = preg_split( '/\s|em|\.|,|\(|\)|na|no|-|_/',
					     $args['q'],
					     0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE );
				     // Modify $query as required

				     foreach ( $args as $arg ) {
					     $data[] = [
						     'OR' => [
							     [ 'Clientes.nome LIKE' => '%' . $arg . '%' ],
								 [ 'Clientes.slogan LIKE' => '%' . $arg . '%' ],
								 [ 'Clientes.descricao LIKE' => '%' . $arg . '%' ],
							     [ 'Clientes.cidade LIKE' => '%' . $arg . '%' ],
							     [ 'Clientes.uf LIKE' => '%' . $arg . '%' ],
							     [ 'Clientes.bairro LIKE' => '%' . $arg . '%' ],
								 [ 'Clientes.logradouro LIKE' => '%' . $arg . '%' ],
								 [ 'Clientes.complemento LIKE' => '%' . $arg . '%' ],
							     [ 'Categorias.nome LIKE' => '%' . $arg . '%' ],

						     ]
					     ];
				     }
				     $query->where( $data )->contain( [ 'Categorias' ] );

				     return $query;
			     }
		     ] );
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 *
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault( Validator $validator ) {
		$validator
			->add( 'id', 'valid', [ 'rule' => 'numeric' ] )
			->allowEmpty( 'id', 'create' );

		$validator
			->requirePresence( 'nome', 'create' )
			->notEmpty( 'nome' );


		$validator
			->add( 'data_cadastro', 'valid', [ 'rule' => 'date' ] )
			->requirePresence( 'data_cadastro', 'create' )
			->notEmpty( 'data_cadastro' );

		$validator
			->add( 'status', 'valid', [ 'rule' => 'boolean' ] )
			->requirePresence( 'status', 'create' )
			->notEmpty( 'status' );

		$validator
			->add( 'plus', 'valid', [ 'rule' => 'boolean' ] )
			->allowEmpty( 'plus' );

		$validator
			->allowEmpty( 'logo' );

		$validator
			->allowEmpty( 'img_header' );

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 *
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules( RulesChecker $rules ) {
		$rules->add( $rules->isUnique( [ 'email' ] ) );
		$rules->add( $rules->existsIn( [ 'plano_id' ], 'Planos' ) );
		$rules->add( $rules->existsIn( [ 'categoria_id' ], 'Categorias' ) );

		return $rules;
	}
}

