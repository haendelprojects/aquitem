<?php
namespace App\Model\Table;

use App\Model\Entity\Classificado;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Classificados Model
 *
 * @property \Cake\ORM\Association\HasMany $Fotos
 */
class ClassificadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('classificados');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Fotos', [
            'foreignKey' => 'classificado_id'
        ]);

         $this->belongsTo('Users', [
            'foreignKey' => 'criado_por'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('categoria');

        $validator
            ->allowEmpty('titulo');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->add('criado_por', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('criado_por');

        $validator
            ->add('preco', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('preco');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        return $rules;
    }
}
