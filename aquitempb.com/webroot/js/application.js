$(document).ready(function () {
    $(".fancybox").fancybox();
});


/**
 * Mapa
 * @type {{zoom: number, center: google.maps.LatLng}}
 */
var div_map = document.getElementById("map");
if (div_map != null) {
    var myOptions = {zoom: 14, center: new google.maps.LatLng(40.805478, -73.96522499999998)};
    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    var addressElement = $('#address');

    if (addressElement) {
        GMaps.geocode({
            address: addressElement.text(),
            callback: function (results, status) {
                if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng);

                    marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(latlng.lat(), latlng.lng())
                    });

                    infowindow = new google.maps.InfoWindow({content: "<b>" + $('#clientName').text() + "</b><br/>" + $('#address').text()});

                    google.maps.event.addListener(marker, "click", function () {
                        infowindow.open(map, marker);
                    });

                    infowindow.open(map, marker);
                }
            }
        });
    }
}


jQuery(document).ready(function ($) {

    var jssor_1_SlideshowTransitions = [
        {$Duration: 1200, $Opacity: 2}
    ];

    var jssor_1_options = {
        $AutoPlay: true,
        $SlideshowOptions: {
            $Class: $JssorSlideshowRunner$,
            $Transitions: jssor_1_SlideshowTransitions,
            $TransitionsOrder: 1
        },
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$
        }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

//responsive code begin
//you can remove responsive code if you don't want the slider scales while window resizing
    function ScaleSlider() {
        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
        if (refSize) {
            refSize = Math.min(refSize, 800);

            jssor_1_slider.$ScaleWidth(refSize);
            refSize = Math.min(refSize, 300);
            jssor_1_slider.$ScaleHeight(refSize);
        }
        else {
            window.setTimeout(ScaleSlider, 30);
        }
    }

    ScaleSlider();
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    //responsive code end
});



