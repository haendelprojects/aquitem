<div class="form large-4 medium-8 large-offset-4 medium-offset-2 columns content box-login">
	<div class="text-center">
		<?= $this->Html->image('logo.gif', ['class'=> 'logo-login']); ?>
		<h3>Login</h3>
	</div>

	<?= $this->Form->create() ?>
	<?= $this->Form->input( 'username' ) ?>
	<?= $this->Form->input( 'password' ) ?>
	<?= $this->Form->button( 'Acessar' , ['class'=>'button success']) ?>
	<?= $this->Form->end() ?>
</div>
