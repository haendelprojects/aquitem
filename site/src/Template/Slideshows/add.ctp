<nav class="large-2 medium-2 columns" id="actions-sidebar">
	<ul class="side-nav">
		<li class="heading"><?= __( 'Ações' ) ?></li>
		<li><?= $this->Html->link( __( 'Slides' ), [ 'action' => 'index' ] ) ?></li>
	</ul>
</nav>
<div class="slideshows form large-8 medium-8 columns content">
	<?= $this->Form->create( $slideshow , ['type'=>'file']) ?>
	<fieldset>
		<legend><?= __( 'Add Slideshow' ) ?></legend>
		<?php
		echo $this->Form->input( 'slide_file' ,  ['type' => 'file'] );
		echo $this->Form->input( 'description' );
		echo $this->Form->input( 'link' );
		echo $this->Form->input( 'ativo' );
		echo $this->Form->input( 'posicao' );
		?>
	</fieldset>
	<?= $this->Form->button( __( 'Submit' ) ) ?>
	<?= $this->Form->end() ?>
</div>
