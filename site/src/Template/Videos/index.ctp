
<div class="videos index large-12 medium-12 columns content-site">
    <h3><?= __('Videos') ?></h3>
    <?php foreach ($videos as $video) : ?>
        <a href="<?= URL . '/videos/' . $video->id; ?>"
           class="large-4 column noticia">
            <div class="noticia-image">
                <?= $this->Html->image('http://i1.ytimg.com/vi/' . $video->url . '/hqdefault.jpg'); ?>
            </div>

            <h3 class="noticia-titulo">
                <?= $video->titulo; ?>
            </h3>
        </a>
    <?php endforeach; ?>
</div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
