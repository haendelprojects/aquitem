<nav class="large-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Lista de Categorias'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova Categoria'), ['controller' => 'Categorias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Lista de Anuncios'), ['controller' => 'Anuncios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Anuncio'), ['controller' => 'Anuncios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Lista de Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categorias form large-8 medium-8 columns content">
    <?= $this->Form->create($categoria) ?>
    <fieldset>
        <legend><?= __('Adicionar Categoria') ?></legend>
        <?php
        echo $this->Form->input('nome');
        echo $this->Form->input('tipo');
        echo $this->Form->input('categoria_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar'), ['class' => 'success']) ?>
    <?= $this->Form->end() ?>
</div>
