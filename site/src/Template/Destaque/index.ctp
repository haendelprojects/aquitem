<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Destaque'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="destaque index large-9 medium-8 columns content">
    <h3><?= __('Destaque') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($destaque as $destaque): ?>
            <tr>
                <td><?= $this->Number->format($destaque->id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $destaque->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $destaque->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $destaque->id], ['confirm' => __('Are you sure you want to delete # {0}?', $destaque->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
