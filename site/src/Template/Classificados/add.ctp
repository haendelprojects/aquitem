<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Classificados'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Fotos'), ['controller' => 'Fotos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Foto'), ['controller' => 'Fotos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classificados form large-9 medium-8 columns content">
    <?= $this->Form->create($classificado) ?>
    <fieldset>
        <legend><?= __('Add Classificado') ?></legend>
        <?php
            echo $this->Form->input('categoria');
            echo $this->Form->input('titulo');
            echo $this->Form->input('descricao');
            echo $this->Form->input('criado_por');
            echo $this->Form->input('preco');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
