<div class="noticias index large-12 medium-12 columns ">
    
<div class="row content-site">
    <div class="large-12">
        <h2 class="titulo-sessao">Aqui tem Noticias</h2> 
    </div>
    <?php foreach ($noticias as $noticia) : ?>
        <a href="<?= URL; ?>/noticias/<?= $noticia->slug; ?>"
           class="large-4 column noticia" style="height:300px">
            <div class="noticia-image">
                <?php
		  				if (strpos( $noticia->thumb, 'http://img.') === false) {
		                    echo $this->Html->image(REPOSITORY_IMG .  $noticia->thumb );
		                } else {
		                    echo $this->Html->image( $noticia->thumb);
		                } ?>
            </div>
            <span class="noticia-data"><?= $noticia->data_cadastro; ?></span>
            <h3 class="noticia-titulo">
                <?= $noticia->titulo; ?>
            </h3>
        </a>
    <?php endforeach; ?>
</div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
