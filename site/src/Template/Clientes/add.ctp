<nav class="large-2 medium-2 columns actions-sidebar sub-sidebar" id="actions-sidebar">
	<ul class="side-nav">
		<li class="heading"><?= __( 'Ações' ) ?></li>
		<li><?= $this->Html->link( __( 'Voltar' ), 'javascript:history.back()' ) ?></li>
	</ul>
</nav>
<div class="clientes index large-8 medium-8 columns content">
	<h3>Novo Cliente</h3>
	<?= $this->Form->create( $cliente, [ 'type' => 'file' ] ) ?>
	<fieldset>
		<legend><?= __( 'Dados' ) ?></legend>
		<?php
		echo $this->Form->input( 'nome' );
		echo $this->Form->input( 'slogan' );
		echo $this->Form->input( 'subdescricao' );
		echo $this->Form->input( 'descricao' );
		echo $this->Form->input( 'data_cadastro' );
		echo $this->Form->input( 'status', [ 'label' => 'Ativo' ] );
		echo $this->Form->input( 'plano_id', [ 'options' => $planos ] );
		echo $this->Form->input( 'categoria_id', [ 'options' => $categorias ] );
		echo $this->Form->input( 'plus' );
		echo $this->Form->input( 'logo_file', [ 'type' => 'file' ] );
		echo $this->Form->input( 'img_header_file', [ 'type' => 'file' ] );
		?>

		<legend><?= __( 'Contato' ) ?></legend>
		<br>
		<?php
		echo $this->Form->input( 'email' );
		echo $this->Form->input( 'site' );
		?>
		<legend><?= __( 'Endereço' ) ?></legend>
		<br>
		<?php
		echo $this->Form->input( 'logradouro' );
		echo $this->Form->input( 'numero' );
		echo $this->Form->input( 'complemento' );
		echo $this->Form->input( 'bairro' );
		echo $this->Form->input( 'cidade' );
		echo $this->Form->input( 'uf' );
		echo $this->Form->input( 'cep' );
		echo $this->Form->input( 'cartao_desconto' );
		?>
	</fieldset>
	<?= $this->Form->button( __( 'Cadastrar' ) ) ?>
	<?= $this->Form->end() ?>
</div>
