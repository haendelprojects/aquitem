<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cliente->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cliente->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Voltar'), 'javascript:history.back()') ?></li>
    </ul>
</nav>
<div class="clientes form large-8 medium-8 columns content">
    <?= $this->Form->create($cliente) ?>
    <fieldset>
        <legend><?= __('Edit Cliente') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('slogan');
            echo $this->Form->input('subdescricao');
            echo $this->Form->input('descricao');
            echo $this->Form->input('email');
            echo $this->Form->input('site');
            echo $this->Form->input('logradouro');
            echo $this->Form->input('numero');
            echo $this->Form->input('complemento');
            echo $this->Form->input('bairro');
            echo $this->Form->input('cidade');
            echo $this->Form->input('uf');
            echo $this->Form->input('cep');
            echo $this->Form->input('cartao_desconto');
            echo $this->Form->input('data_cadastro');
            echo $this->Form->input('status');
            echo $this->Form->input('plano_id', ['options' => $planos]);
            echo $this->Form->input('categoria_id', ['options' => $categorias]);
            echo $this->Form->input('plus');
            echo $this->Form->input('logo');
            echo $this->Form->input('img_header');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
