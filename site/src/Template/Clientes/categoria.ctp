<?php

function mascara_string($mascara , $string){
    $s =   str_replace(" ", "", $string);
    for($i = 0; $i < strlen($string) ; $i++){
        $mascara[strpos($mascara, "#")] = $string[$i];
    }
    return $mascara;
}

?>

<div class="large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0" class="table-list-clientes">
        <tbody>
        <?php foreach ($clientes as $cliente): ?>
            <td style="width:30%"><?php
                if (strpos($cliente->logo, 'http://img.') === false) {
                    echo $this->Html->image(REPOSITORY_IMG . $cliente->logo, ['style' => 'width:70%; margin-left: 10px']);
                } else {
                    echo $this->Html->image($cliente->logo, ['style' => 'width:70%; margin-left: 10px']);
                }

                ?>
            <td>
                <?php

                $text = '<h4 class="title">' . strip_tags($cliente->nome) . '</h4>';
                $text .= '<p>'.strip_tags($cliente->slogan) .'</p>';
                $text .= '<p><strong>Telefone: </strong>';

            

                foreach ($cliente->telefones as $telefone) {

                    if(strlen($telefone['numero']) == 11){
                         $text .= mascara_string('(##) #####-####' , $telefone['numero']) . ',';
                    }elseif(strlen($telefone['numero']) == 10){
                         $text .= mascara_string('(##) ####-####' , $telefone['numero']) . ',';
                    }else{
                         $text .= $telefone['numero'] . ', ';
                    }
                }
                $text .= '</p>';
                $text .= '  <p><strong>Categoria:</strong> '.$cliente->categoria->nome. '</p>';
                $text .= '<p><strong > Localização: </strong > ' . h($cliente->logradouro) . ', ' . h($cliente->numero) . ', ' . h($cliente->bairro) . ' - ' . h($cliente->cidade) . ' / ' . h($cliente->uf) . '</p >';
                $text .= '  <p> + Detalhes</p>';
                echo $this->Html->link($text, '/clientes/' . $cliente->slug, ['escape' => false]); ?>

            </td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
</div>
