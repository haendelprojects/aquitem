<?php

function mascara_string($mascara , $string){
    $s =   str_replace(" ", "", $string);
    for($i = 0; $i < strlen($string) ; $i++){
        $mascara[strpos($mascara, "#")] = $string[$i];
    }
    return $mascara;
}

?>


<div class="large-12 medium-12 columns detalhe-cliente">
    <?php

  if (strpos($cliente->img_header , 'http://img.') === false) {
                    echo $this->Html->image(REPOSITORY_IMG . $cliente->img_header , ['class' => 'page-detail-img-header']);
                } else {
                    echo $this->Html->image($cliente->img_header , ['class' => 'page-detail-img-header']);
                }
    ?>


    <div class="content-site row descricao-cliente">
        <div class="large-3 column text-center">
            <?php
            if ($cliente->logo != '' && $cliente->logo != null) {
                if (strpos($cliente->logo, 'http://img.') === false) {
                    echo $this->Html->image(REPOSITORY_IMG . $cliente->logo, ['style' => 'width: 90%']);
                } else {
                    echo $this->Html->image($cliente->logo, ['style' => 'width:90%']);
                }

            } ?>


        </div>
        <div class="large-9 column">
            <h1 style="font-size:20px; font-weight: 600" id="clientName"><?= strip_tags($cliente->nome) ?> </h1>
            <div>
                <b><?= $this->Text->autoParagraph(h($cliente->slogan)); ?></b>
            </div>
            <div>
                <?= $this->Text->autoParagraph($cliente->descricao); ?>
            </div>
        </div>

    </div>


    <table class="vertical-table">
        <tr>
            <th><?= __('Telefones') ?></th>
            <td><?php

                foreach ($cliente->telefones as $telefone) {

                    if(strlen($telefone['numero']) == 11){
                        echo mascara_string('(##) #####-####' , $telefone['numero']) . ',';
                    }elseif(strlen($telefone['numero']) == 10){
                        echo mascara_string('(##) ####-####' , $telefone['numero']) . ',';
                    }else{
                        echo $telefone['numero'] . ', ';
                    }
                } ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= (strpos($cliente->email, '<a') !== false) ? '' : h($cliente->email); ?></td>
        </tr>
        <tr>
            <th><?= __('Site') ?></th>
            <td><?= $this->Html->link('+ Detalhes', $cliente->site, ['target' => '_blank']); ?></td>
        </tr>

        <tr>
            <th><?= __('Página Web') ?></th>
            <td><?php

                if (strpos($cliente->wix, '<a') !== false) {
                    echo $cliente->wix;
                } else {
                    echo $this->Html->link('+ Detalhes', h($cliente->wix), ['target' => '_blank']);
                }; ?></td>
        </tr>

        <tr>
            <th><?= __('Endereço') ?></th>
            <td id="address">
                <?= h($cliente->logradouro) ?> , <?= h($cliente->numero) ?> , <?= h($cliente->complemento) ?>
                <?= h($cliente->bairro) ?> - <?= h($cliente->cidade) ?>/<?= h($cliente->uf) ?>
            </td>
        </tr>
    </table>

    <div>
        <div id="map"></div>
    </div>

</br>
    <div class="content-site galeria">
        <h4>Galeria de Imagens</h4>

        <?php foreach ($cliente->fotos as $foto): ?>
            <a class="fancybox" rel="gallery1" href="<?= (strpos($foto->image , 'http://img.') === false) ? REPOSITORY_IMG  : '' ?><?= $foto->image; ?>"
               title="Galeria da empresa">
                <img src="<?= (strpos($foto->image , 'http://img.') === false) ? REPOSITORY_IMG  : '' ?><?= $foto->image; ?>" alt=""/>
            </a>
        <?php endforeach; ?>
    </div>

</div>

