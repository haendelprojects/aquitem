<div class="large-12 medium-12 columns detalhe-cliente">
    <?= $this->Form->create($interess) ?>
    <fieldset>

        <h2 class="titulo-sessao"><?= __('Contato para Anúncios') ?></h2>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('email');
            echo $this->Form->input('telefone');
            echo $this->Form->input('celular');
            echo $this->Form->input('mensagem');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
