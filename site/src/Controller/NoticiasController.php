<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Noticias Controller
 *
 * @property \App\Model\Table\NoticiasTable $Noticias
 */
class NoticiasController extends AppController
{

    public function initialize()
    {
        parent:: initialize();

        $this->Auth->allow(['view']);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $query = $this->Noticias->find('all')->where(['status' => 1])->order(['data_cadastro' => 'desc']);
        $this->set('noticias', $this->paginate($query));
        $this->set('_serialize', ['noticias']);
    }

    /**
     * View method
     *
     * @param string|null $id Noticia id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($slug = null)
    {
        $noticia = $this->Noticias->find('all')->where(['slug' => $slug])->contain(['Users'])->first();

        $noticias = $this->Noticias
            ->find('all')
            ->where(['status' => 1, 'id !=' => $noticia->id])
            ->order(['data_cadastro' => 'desc'])
            ->limit(3);
        $this->set('noticias', $noticias );

        $this->set('noticia', $noticia);
        $this->set('_serialize', ['noticia', 'noticias']);
    }


}
