<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Inflector;

/**
 * Categorias Controller
 *
 * @property \App\Model\Table\CategoriasTable $Categorias
 */
class CategoriasController extends AppController {

	public function initialize() {
		parent::initialize();

		$this->Auth->allow();
	}

	public function createSlug() {
		$categorias = $this->Categorias->find( 'all' )->toArray();

		foreach ( $categorias as $categoria ) {
			$categoria = $categoria->toArray();

			if ( $categoria['nome'] == '' ) {
				$categoria['nome'] = 'Indefinido';
			}

			$categoria['slug'] = strtolower( Inflector::slug( $categoria['nome'] ) );

			debug( $categoria );
			$cat = $this->Categorias->get( $categoria['id'] );

			debug( $cat );
			$cat = $this->Categorias->patchEntity( $cat, $categoria );

			debug( $cat->errors() );
			debug( $cat );

			$this->Categorias->save( $cat );
		}

		exit();
	}


	public function alterType() {
		$categorias = $this->Categorias->find( 'all' )->toArray();

		foreach ( $categorias as $categoria ) {
			$categoria = $categoria->toArray();

			if ( $categoria['categoria_id'] != '' ) {
				$categoria['tipo'] = 0;
			}

			$cat = $this->Categorias->get( $categoria['id'] );

;
			$cat = $this->Categorias->patchEntity( $cat, $categoria );



			$this->Categorias->save( $cat );
		}

		exit();
	}


	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index() {
		$this->set( 'categorias', $this->paginate( $this->Categorias ) );
		$this->set( '_serialize', [ 'categorias' ] );
	}

	public function listAll() {
		$this->set( 'categorias',  $this->Categorias->find('all')->where(['nome !=' => "Indefinido", "categoria_id IS NOT" => null])->orderAsc('nome')  );
		$this->set( '_serialize', [ 'categorias' ] );
	}

	/**
	 * View method
	 *
	 * @param string|null $id Categoria id.
	 *
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view( $id = null ) {
		$categoria = $this->Categorias->get( $id, [
			'contain' => [ 'Categorias', 'Anuncios', 'Clientes' ]
		] );

$this->redirect(['controller'=>'clientes', 'action'=>'categoria', $categoria->slug]);
		$this->set( 'categoria', $categoria );
		$this->set( '_serialize', [ 'categoria' ] );
	}



}
