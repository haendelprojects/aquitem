<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Destaque Controller
 *
 * @property \App\Model\Table\DestaqueTable $Destaque
 */
class DestaqueController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('destaque', $this->paginate($this->Destaque));
        $this->set('_serialize', ['destaque']);
    }

    /**
     * View method
     *
     * @param string|null $id Destaque id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $destaque = $this->Destaque->get($id, [
            'contain' => []
        ]);
        $this->set('destaque', $destaque);
        $this->set('_serialize', ['destaque']);
    }


}
