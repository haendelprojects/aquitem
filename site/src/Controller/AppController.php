<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form'
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
        ]);

        $this->Auth->allow();

        $keywords = 'endereços, lista online, lista telefonica, pernambuco listas, guia online, guia de endereços, profissionais liberais, grandes empresas, auxilio a lista, paginas amarelas, catalogo de endereços, classificados, noticias, serviços, telefones, guia, listas telefonicas, lista telefônica, 102, guia local, portal de endereços, listas online, busca local.';

$this->set(compact('keywords'));
    }

    public function beforeFilter(Event $event)
    {
        $Categorias = TableRegistry::get('Categorias');
        $Anuncios = TableRegistry::get('Anuncios');
        $lista_categorias = $Categorias
            ->find('threaded', [
                'keyField' => 'id',
                'parentField' => 'categoria_id'
            ])
            ->where(['Categorias.nome !=' => 'Indefinido'])
            ->order(['Categorias.nome ' => 'ASC'])
            ->toArray();

        $anuncio_topo = $Anuncios->find('all', [
                'conditions' => ['local' => 'topo-site'],
                'limit' => 1,
                'order' => 'rand()']
        );


        $anuncio_lado = $Anuncios->find('all', [
                'conditions' => ['local' => 'lado-site'],
                'limit' => 2,
                'order' => 'rand()']
        );



        $this->set(compact('lista_categorias', 'anuncio_topo', 'anuncio_lado'));
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     *
     * @return void
     */
    public function beforeRender(Event $event)
    {
        //Setando na view o usu�rio logado
        $this->set('UserLogged', $this->Auth->user());

        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
