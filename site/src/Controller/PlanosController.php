<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Planos Controller
 *
 * @property \App\Model\Table\PlanosTable $Planos
 */
class PlanosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('planos', $this->paginate($this->Planos));
        $this->set('_serialize', ['planos']);
    }

    /**
     * View method
     *
     * @param string|null $id Plano id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $plano = $this->Planos->get($id, [
            'contain' => ['Clientes']
        ]);
        $this->set('plano', $plano);
        $this->set('_serialize', ['plano']);
    }


}
