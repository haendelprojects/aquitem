<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Anuncios Controller
 *
 * @property \App\Model\Table\AnunciosTable $Anuncios
 */
class AnunciosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categorias']
        ];
        $this->set('anuncios', $this->paginate($this->Anuncios));
        $this->set('_serialize', ['anuncios']);
    }

    /**
     * View method
     *
     * @param string|null $id Anuncio id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $anuncio = $this->Anuncios->get($id, [
            'contain' => ['Categorias']
        ]);
        $this->set('anuncio', $anuncio);
        $this->set('_serialize', ['anuncio']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $anuncio = $this->Anuncios->newEntity();
        if ($this->request->is('post')) {
            $anuncio = $this->Anuncios->patchEntity($anuncio, $this->request->data);
            if ($this->Anuncios->save($anuncio)) {
                $this->Flash->success(__('The anuncio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The anuncio could not be saved. Please, try again.'));
            }
        }
        $categorias = $this->Anuncios->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('anuncio', 'categorias'));
        $this->set('_serialize', ['anuncio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Anuncio id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $anuncio = $this->Anuncios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anuncio = $this->Anuncios->patchEntity($anuncio, $this->request->data);
            if ($this->Anuncios->save($anuncio)) {
                $this->Flash->success(__('The anuncio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The anuncio could not be saved. Please, try again.'));
            }
        }
        $categorias = $this->Anuncios->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('anuncio', 'categorias'));
        $this->set('_serialize', ['anuncio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Anuncio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $anuncio = $this->Anuncios->get($id);
        if ($this->Anuncios->delete($anuncio)) {
            $this->Flash->success(__('The anuncio has been deleted.'));
        } else {
            $this->Flash->error(__('The anuncio could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
